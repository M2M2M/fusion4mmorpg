package common

const (
	ServerOpenStatus_Normal = iota
	ServerOpenStatus_Maintain
	ServerOpenStatus_Hide
)

const (
	ServerSpecialFlag_Hot = 1 << iota
	ServerSpecialFlag_Full
)
const (
	ServerSpecialFlag_New = 1 << (8 + iota)
	ServerSpecialFlag_Recommand
)

type GameServerInfo struct {
	ID                uint32 `db:"Id"`
	ExternalIP        string `db:"externalIP"`
	ExternalPort      uint16 `db:"externalPort"`
	InternalIP        string `db:"internalIP"`
	InternalName      string `db:"internalName"`
	LogicID           uint32 `db:"logicId"`
	LogicName         string `db:"logicName"`
	LogicOpenStatus   uint8  `db:"logicOpenStatus"`
	LogicSpecialFlags uint32 `db:"logicSpecialFlags"`
}

func (*GameServerInfo) GetTableName() string {
	return "t_game_servers"
}
