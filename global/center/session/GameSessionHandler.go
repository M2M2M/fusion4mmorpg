package session

import (
	"test.com/fusion"
	"test.com/protocol"
)

var GameSessionHandlers = [protocol.CENTER_PROTOCOL_COUNT]func(*GameSession, *fusion.NetPacket) int{
	protocol.CCT_REGISTER: (*GameSession).handleRegister,
}

var GameSessionRPCHandlers = [protocol.CENTER_PROTOCOL_COUNT]func(*GameSession, *fusion.NetPacket, *fusion.RPCReqMetaInfo) int{
	protocol.CCT_VERIFY_ACCOUNT_VALIDITY: (*GameSession).handleVerifyAccountValidity,
	protocol.CCT_CHARACTER_CREATE:        (*GameSession).handleCharacterCreate,
	protocol.CCT_CHARACTER_DELETE:        (*GameSession).handleCharacterDelete,
	protocol.CCT_GUILD_CREATE:            (*GameSession).handleGuildCreate,
	protocol.CCT_GUILD_DELETE:            (*GameSession).handleGuildDelete,
}
