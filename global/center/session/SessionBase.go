package session

import (
	"test.com/fusion"
)

func finishResp(opcode int, errCode int32, errMsg string) *fusion.NetPacket {
	var resp = fusion.NetPacket{Opcode: opcode}
	resp.Write(errCode, errMsg)
	return &resp
}
