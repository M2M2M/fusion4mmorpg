package worlddb

// ItemClass
const (
	ItemClass_None       = 0
	ItemClass_Equip      = 1 // 装备
	ItemClass_Quest      = 2 // 任务
	ItemClass_Material   = 3 // 材料
	ItemClass_Gemstone   = 4 // 宝石
	ItemClass_Consumable = 5 // 消耗品
	ItemClass_Score      = 6 // 积分
	ItemClass_Scrap      = 7 // 废品
	ItemClass_Other      = 8 // 其它
)

// ItemSubClass
const (
	ItemSubClass_Equip_Weapon = 1 // 武器
	ItemSubClass_Equip_Belt   = 2 // 腰带
	ItemSubClass_Equip_Gloves = 3 // 手套
	ItemSubClass_Equip_Shoes  = 4 // 鞋子
)

// ItemQuality
const (
	ItemQuality_White = 0
	ItemQuality_Red   = 1
	ItemQuality_Count = 2
)

type ItemPrototype_Flags struct {
	CanUse            bool `json:"canUse,omitempty"`
	CanSell           bool `json:"canSell,omitempty"`
	CanDestroy        bool `json:"canDestroy,omitempty"`
	IsDestroyAfterUse bool `json:"isDestroyAfterUse,omitempty"`
}

type ItemPrototype struct {
	ItemTypeID   uint32              `json:"itemTypeID,omitempty" rule:"required"`
	ItemFlags    ItemPrototype_Flags `json:"itemFlags,omitempty" rule:"required"`
	ItemClass    uint32              `json:"itemClass,omitempty" rule:"required"`
	ItemSubClass uint32              `json:"itemSubClass,omitempty" rule:"required"`
	ItemQuality  uint32              `json:"itemQuality,omitempty" rule:"required"`
	ItemLevel    uint32              `json:"itemLevel,omitempty" rule:"required"`
	ItemStack    uint32              `json:"itemStack,omitempty" rule:"required"`

	ItemSellPrice uint32 `json:"itemSellPrice,omitempty" rule:"required"`

	ItemLootId     uint32   `json:"itemLootId,omitempty" rule:"required"`
	ItemSpellId    uint32   `json:"itemSpellId,omitempty" rule:"required"`
	ItemSpellLevel uint32   `json:"itemSpellLevel,omitempty" rule:"required"`
	ItemScriptId   uint32   `json:"itemScriptId,omitempty" rule:"required"`
	ItemScriptArgs string   `json:"itemScriptArgs,omitempty" rule:"required"`
	ItemParams     []uint32 `json:"itemParams,omitempty" rule:"required"`
}

func (*ItemPrototype) GetTableName() string {
	return "item_prototype"
}
func (*ItemPrototype) GetTableKeyName() string {
	return "itemTypeID"
}
func (obj *ItemPrototype) GetTableKeyValue() uint {
	return uint(obj.ItemTypeID)
}

type ItemEquipPrototype_Attr struct {
	Type  uint32  `json:"type,omitempty"`
	Value float64 `json:"value,omitempty"`
}

type ItemEquipPrototype_Spell struct {
	Id    uint32 `json:"id,omitempty"`
	Level uint32 `json:"level,omitempty"`
}

type ItemEquipPrototype struct {
	ItemTypeID uint32 `json:"itemTypeID,omitempty" rule:"required"`

	ItemEquipAttrs []ItemEquipPrototype_Attr `json:"itemEquipAttrs,omitempty" rule:"required"`

	ItemEquipSpells []ItemEquipPrototype_Spell `json:"itemEquipSpells,omitempty" rule:"required"`
}

func (*ItemEquipPrototype) GetTableName() string {
	return "item_equip_prototype"
}
func (*ItemEquipPrototype) GetTableKeyName() string {
	return "itemTypeID"
}
func (obj *ItemEquipPrototype) GetTableKeyValue() uint {
	return uint(obj.ItemTypeID)
}
