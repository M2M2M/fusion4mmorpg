package worlddb

// PlayerCareer
const (
	PlayerCareer_None    = 0
	PlayerCareer_Terran  = 1
	PlayerCareer_Protoss = 2
	PlayerCareer_Demons  = 3
	PlayerCareer_Max     = 4
)

// PlayerGender
const (
	PlayerGender_None   = 0
	PlayerGender_Male   = 1
	PlayerGender_Female = 2
	PlayerGender_Max    = 3
)

// CurrencyType
const (
	CurrencyType_None    = 0
	CurrencyType_Gold    = 1 // 金币
	CurrencyType_Diamond = 2 // 钻石
	CurrencyType_Max     = 3
)

// ChequeType
const (
	ChequeType_None    = 0
	ChequeType_Gold    = 1 // 金币
	ChequeType_Diamond = 2 // 钻石
	ChequeType_Exp     = 3 // 经验
	ChequeType_VipExp  = 4 // VIP经验
	ChequeType_Max     = 5
)

// ItemFlag
const (
	ItemFlag_Binding  = 0
	ItemFlag_Constant = 1
)

type ChequeInfo struct {
	Type  uint8  `json:"type,omitempty"`
	Value uint64 `json:"value,omitempty"`
}

type ItemInfo struct {
	Id  uint32 `json:"id,omitempty"`
	Num uint32 `json:"num,omitempty"`
}

type FItemInfo struct {
	Id    uint32 `json:"id,omitempty"`
	Num   uint32 `json:"num,omitempty"`
	Flags uint32 `json:"flags,omitempty"`
}

type Configure struct {
	CfgIndex uint32 `json:"cfgIndex,omitempty" rule:"required"`
	CfgName  string `json:"cfgName,omitempty" rule:"required"`
	CfgVal   string `json:"cfgVal,omitempty" rule:"required"`
	CfgDesc  string `json:"cfgDesc,omitempty" rule:"required"`
}

func (*Configure) GetTableName() string {
	return "configure"
}
func (*Configure) GetTableKeyName() string {
	return ""
}
func (obj *Configure) GetTableKeyValue() uint {
	return 0
}
