package worlddb

// EditorTreeView_Type
const (
	EditorTreeView_Type_None  = 0
	EditorTreeView_Type_Spell = 1
	EditorTreeView_Type_Quest = 2
	EditorTreeView_Type_Char  = 3
	EditorTreeView_Type_SObj  = 4
	EditorTreeView_Type_Item  = 5
	EditorTreeView_Type_Loot  = 6
)

type EditorTreeView struct {
	EtvType uint32 `json:"etvType,omitempty" rule:"required"`
	EtvTree string `json:"etvTree,omitempty" rule:"required"`
}

func (*EditorTreeView) GetTableName() string {
	return "editor_tree_view"
}
func (*EditorTreeView) GetTableKeyName() string {
	return "etvType"
}
func (obj *EditorTreeView) GetTableKeyValue() uint {
	return uint(obj.EtvType)
}
