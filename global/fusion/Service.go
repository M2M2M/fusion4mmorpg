package fusion

import (
	"log"
	"time"
)

type ServiceBase struct {
	Name   string
	Tasks  chan func()
	RPCMgr *RPCManager
}

func (s *ServiceBase) Init(name string) {
	s.Name = name
	s.Tasks = make(chan func(), 1024)
	s.RPCMgr = NewRPCManager(32)
}

func (s *ServiceBase) Tick() {
	s.RPCMgr.OnTick()
}

func (s *ServiceBase) Start() {
	go SafeHandler(func() {
		log.Printf("start service `%s` successfully.", s.Name)
		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()
		for !IsStopService {
			SafeHandler(func() {
				select {
				case task := <-s.Tasks:
					task()
				case task := <-s.RPCMgr.tasks:
					task()
				case <-ticker.C:
					s.Tick()
				case <-SigStopService:
				}
			})()
		}
	})()
}

func (s *ServiceBase) AsyncBlockInvoke(task func()) {
	c := make(chan bool)
	s.Tasks <- func() {
		task()
		close(c)
	}
	<-c
}
