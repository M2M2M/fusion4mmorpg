package main

import (
	"log"
	"net/http"

	"test.com/fusion"
	"test.com/login/session"
)

func runWebClientListener() {
	var addr = cfg.WebClientListen
	log.Printf("start web client listener `%s` goroutine successfully.\n", addr)

	mux := http.NewServeMux()
	fusion.HttpHandleGzipFunc(mux, "/RegisterAccount.html", session.HandleRegisterAccount)
	fusion.HttpHandleGzipFunc(mux, "/VerifyAccount.html", session.HandleVerifyAccount)
	fusion.HttpHandleGzipFunc(mux, "/GetAccountCharaters.html", session.HandleGetAccountCharaters)
	fusion.HttpHandleGzipFunc(mux, "/GetServerList.html", session.HandleGetServerList)

	srv := &http.Server{Addr: addr, Handler: mux}
	fusion.AllWebServers.Store(srv, true)

	err := srv.ListenAndServeTLS("server.crt", "server.key")
	if err != http.ErrServerClosed {
		log.Fatalf("listen web `%s` failed, %s.\n", addr, err)
	}
}
