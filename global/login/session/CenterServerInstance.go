package session

import (
	"test.com/fusion"
	"test.com/protocol"
)

var InstCenterServer *CenterServerSession

type CenterServerSession struct {
	fusion.Session
	isReady bool
}

func NewCenterServerSessionInstance() {
	InstCenterServer = &CenterServerSession{}
}

func (obj *CenterServerSession) SendRegister() {
	pck := fusion.NetPacket{Opcode: protocol.CLC_REGISTER}
	obj.SendPacket(&pck)
}
