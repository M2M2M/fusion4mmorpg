package main

import (
	"net/http"
)

func writeJsonResponseData(w http.ResponseWriter, data []byte) {
	header := w.Header()
	header.Set("content-type", "application/json")
	w.Write(data)
}

func writeOKResponseData(w http.ResponseWriter) {
	w.Write([]byte("All is OK."))
}
