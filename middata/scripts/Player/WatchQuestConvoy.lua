local t = {}

t.events = {
    ObjectHookEvent.OnPlayerChangeQuestStatus,
}

function main(pPlayer)
    t.obj = pPlayer
    return pPlayer:AttachObjectHookInfo(t)
end

function t.OnPlayerChangeQuestStatus(pQuestLog, type)
    if type == QuestWhenType.Finish || type == QuestWhenType.Failed ||
       type == QuestWhenType.Cancel then
        t.obj:RemoveConvoyStatus(pQuestLog:GetQuestTypeID())
    end
end