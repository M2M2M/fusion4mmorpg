#include "preHeader.h"
#include "GameListener.h"
#include "GameSession.h"
#include "ServerMaster.h"

GameListener::GameListener()
{
}

GameListener::~GameListener()
{
}

std::string GameListener::GetBindAddress()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("DBP_SERVER", "HOST", "0.0.0.0");
}

std::string GameListener::GetBindPort()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("DBP_SERVER", "PORT", "9990");
}

Session *GameListener::NewSessionObject()
{
	return new GameSession();
}
