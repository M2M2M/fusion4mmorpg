#include "preHeader.h"
#include "GameSessionHandler.h"
#include "protocol/DBPProtocol.h"

GameSessionHandler::GameSessionHandler()
{
	handlers_[DBPProtocol::CDBP_REGISTER] = &GameSessionHandler::HandleRegister;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ALL_INST_CFG_DATA] = &GameSessionHandler::HandleLoadAllInstCfgData;
	rpc_handlers_[DBPProtocol::CDBP_SAVE_INST_CFG_DATA] = &GameSessionHandler::HandleSaveInstCfgData;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ALL_PLAYER_CHARACTER_INFO] = &GameSessionHandler::HandleLoadAllPlayerCharacterInfo;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ACCOUNT_PLAYER_PREVIEW_INFO] = &GameSessionHandler::HandleLoadAccountPlayerPreviewInfo;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ONE_PLAYER_CHARACTER_INFO] = &GameSessionHandler::HandleLoadOnePlayerCharacterInfo;
	rpc_handlers_[DBPProtocol::CDBP_NEW_ONE_PLAYER_INSTANCE] = &GameSessionHandler::HandleNewOnePlayerInstance;
	rpc_handlers_[DBPProtocol::CDBP_SAVE_ONE_PLAYER_INSTANCE] = &GameSessionHandler::HandleSaveOnePlayerInstance;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ONE_PLAYER_INSTANCE] = &GameSessionHandler::HandleLoadOnePlayerInstance;
	rpc_handlers_[DBPProtocol::CDBP_DELETE_ONE_PLAYER_INSTANCE] = &GameSessionHandler::HandleDeleteOnePlayerInstance;
	rpc_handlers_[DBPProtocol::CDBP_SAVE_MULTI_PLAYER_GS_INFO] = &GameSessionHandler::HandleSaveMultiPlayerGsInfo;
	rpc_handlers_[DBPProtocol::CDBP_ACQUIRE_MAIL_EXPIRE_TIME] = &GameSessionHandler::HandleAcquireMailExpireTime;
	rpc_handlers_[DBPProtocol::CDBP_CLEAN_EXPIRE_MAIL] = &GameSessionHandler::HandleCleanExpireMail;
	rpc_handlers_[DBPProtocol::CDBP_GET_PLAYER_MAIL_COUNT] = &GameSessionHandler::HandleGetPlayerMailCount;
	rpc_handlers_[DBPProtocol::CDBP_GET_PLAYER_MAIL_LIST] = &GameSessionHandler::HandleGetPlayerMailList;
	rpc_handlers_[DBPProtocol::CDBP_GET_PLAYER_MAIL_ATTACHMENT] = &GameSessionHandler::HandleGetPlayerMailAttachment;
	rpc_handlers_[DBPProtocol::CDBP_PLAYER_GET_MAIL_ATTACHMENT] = &GameSessionHandler::HandlePlayerGetMailAttachment;
	rpc_handlers_[DBPProtocol::CDBP_PLAYER_VIEW_MAIL_DETAIL] = &GameSessionHandler::HandlePlayerViewMailDetail;
	rpc_handlers_[DBPProtocol::CDBP_PLAYER_WRITE_MAIL] = &GameSessionHandler::HandlePlayerWriteMail;
	rpc_handlers_[DBPProtocol::CDBP_PLAYER_DELETE_MAIL] = &GameSessionHandler::HandlePlayerDeleteMail;
	rpc_handlers_[DBPProtocol::CDBP_SYSTEM_MULTI_SAME_MAIL] = &GameSessionHandler::HandleSystemMultiSameMail;
	rpc_handlers_[DBPProtocol::CDBP_SYSTEM_MULTI_DIFFERENT_MAIL] = &GameSessionHandler::HandleSystemMultiDifferentMail;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ALL_GUILD_INFO] = &GameSessionHandler::HandleLoadAllGuildInfo;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ALL_GUILD_MEMBER] = &GameSessionHandler::HandleLoadAllGuildMember;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ALL_GUILD_APPLY] = &GameSessionHandler::HandleLoadAllGuildApply;
	rpc_handlers_[DBPProtocol::CDBP_NEW_ONE_GUILD_INSTANCE] = &GameSessionHandler::HandleNewOneGuildInstance;
	rpc_handlers_[DBPProtocol::CDBP_SAVE_ONE_GUILD_INSTANCE] = &GameSessionHandler::HandleSaveOneGuildInstance;
	rpc_handlers_[DBPProtocol::CDBP_DELETE_ONE_GUILD_INSTANCE] = &GameSessionHandler::HandleDeleteOneGuildInstance;
	rpc_handlers_[DBPProtocol::CDBP_NEW_ONE_GUILD_MEMBER] = &GameSessionHandler::HandleNewOneGuildMember;
	rpc_handlers_[DBPProtocol::CDBP_SAVE_ONE_GUILD_MEMBER] = &GameSessionHandler::HandleSaveOneGuildMember;
	rpc_handlers_[DBPProtocol::CDBP_DELETE_ONE_GUILD_MEMBER] = &GameSessionHandler::HandleDeleteOneGuildMember;
	rpc_handlers_[DBPProtocol::CDBP_NEW_ONE_GUILD_APPLY] = &GameSessionHandler::HandleNewOneGuildApply;
	rpc_handlers_[DBPProtocol::CDBP_DELETE_GUILD_APPLY] = &GameSessionHandler::HandleDeleteGuildApply;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ALL_SOCIAL_FRIEND] = &GameSessionHandler::HandleLoadAllSocialFriend;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ALL_SOCIAL_IGNORE] = &GameSessionHandler::HandleLoadAllSocialIgnore;
	rpc_handlers_[DBPProtocol::CDBP_DELETE_ALL_SOCIAL_DATA] = &GameSessionHandler::HandleDeleteAllSocialData;
	rpc_handlers_[DBPProtocol::CDBP_NEW_SOCIAL_FRIEND] = &GameSessionHandler::HandleNewSocialFriend;
	rpc_handlers_[DBPProtocol::CDBP_DELETE_SOCIAL_FRIEND] = &GameSessionHandler::HandleDeleteSocialFriend;
	rpc_handlers_[DBPProtocol::CDBP_NEW_SOCIAL_IGNORE] = &GameSessionHandler::HandleNewSocialIgnore;
	rpc_handlers_[DBPProtocol::CDBP_DELETE_SOCIAL_IGNORE] = &GameSessionHandler::HandleDeleteSocialIgnore;
	rpc_handlers_[DBPProtocol::CDBP_LOAD_ALL_OPERATING_ACTIVITY] = &GameSessionHandler::HandleLoadAllOperatingActivity;
	rpc_handlers_[DBPProtocol::CDBP_SAVE_OPERATING_ACTIVITY] = &GameSessionHandler::HandleSaveOperatingActivity;
	rpc_handlers_[DBPProtocol::CDBP_CLEANUP_OPERATING_ACTIVITY] = &GameSessionHandler::HandleCleanupOperatingActivity;
};

GameSessionHandler::~GameSessionHandler()
{
}
