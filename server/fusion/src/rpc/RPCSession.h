#pragma once

#include "network/Session.h"
#include "RPCManager.h"

class RPCSession : public Session, public RPCActor
{
public:
    RPCSession(const std::string &name, uint32 rpc_resp_opcode);
    virtual ~RPCSession();

    size_t GetWaitReplyRequests();

protected:
    virtual void OnTick();
    virtual void OnRecvPacket(INetPacket *pck);
    virtual void OnShutdownSession();

    void OnRPCSessionReady();

private:
    virtual void PushRPCPacket(const INetPacket &trans,
        const INetPacket &pck, const std::string_view &args);

    virtual RPCManager *GetRPCManager() {
        return &mgr_;
    }

    const uint32 rpc_resp_opcode_;
    RPCManager mgr_;
};
