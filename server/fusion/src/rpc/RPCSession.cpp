#include "RPCSession.h"

RPCSession::RPCSession(const std::string &name, uint32 rpc_resp_opcode)
: Session(name)
, rpc_resp_opcode_(rpc_resp_opcode)
{
}

RPCSession::~RPCSession()
{
}

void RPCSession::OnTick()
{
    mgr_.TickObjs();
    Session::OnTick();
}

void RPCSession::OnRecvPacket(INetPacket *pck)
{
    if (pck->GetOpcode() == rpc_resp_opcode_) {
        mgr_.OnRPCReply(pck);
    } else {
        Session::OnRecvPacket(pck);
    }
}

void RPCSession::OnShutdownSession()
{
    SetReady(false);
    mgr_.InterruptAllRequests(this);
    Session::OnShutdownSession();
}

void RPCSession::OnRPCSessionReady()
{
    SetReady(true);
    mgr_.SendAllRequests(this);
}

void RPCSession::PushRPCPacket(const INetPacket &trans,
    const INetPacket &pck, const std::string_view &args)
{
    const size_t i = trans.IsOpNil() ? 1 : 0;
    const INetPacket *pcks[] = { &trans, &pck };
    const std::string_view datas[] = { args };
    PushSendPacket(pcks + i, ARRAY_SIZE(pcks) - i, datas, ARRAY_SIZE(datas));
}

size_t RPCSession::GetWaitReplyRequests()
{
    return mgr_.GetWaitReplyRequests();
}
