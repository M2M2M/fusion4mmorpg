#include "Crypto.hpp"
#include <openssl/x509.h>
#include <openssl/sha.h>
#include <openssl/md5.h>

namespace rsa {

RSA *generate_key()
{
    RSA *rsa = RSA_new();
    BIGNUM *e = BN_new();
    BN_set_word(e, RSA_F4);
    if (RSA_generate_key_ex(rsa, 2048, e, nullptr) != 1) {
        RSA_free(rsa), rsa = nullptr;
    }
    BN_free(e);
    return rsa;
}

void free_key(RSA *rsa)
{
    if (rsa != nullptr) {
        RSA_free(rsa);
    }
}

std::string dump_pubkey(RSA *rsa)
{
    std::string pubkey;
    unsigned char *pp = nullptr;
    int len = i2d_RSA_PUBKEY(rsa, &pp);
    if (len != -1) {
        pubkey.assign((const char *)pp, len);
    }
    if (pp != nullptr) {
        free(pp);
    }
    return pubkey;
}

std::string public_encrypt(
    const std::string_view &pubkey, const std::string_view &data)
{
    std::string ciphertext;
    auto pp = (const unsigned char *)pubkey.data();
    RSA *rsa = d2i_RSA_PUBKEY(nullptr, &pp, pubkey.size());
    if (rsa != nullptr) {
        ciphertext.resize(RSA_size(rsa));
        int size = RSA_public_encrypt(
            data.size(), (const unsigned char *)data.data(),
            (unsigned char *)ciphertext.data(), rsa, RSA_PKCS1_PADDING);
        ciphertext.resize(size != -1 ? size : 0);
        RSA_free(rsa);
    }
    return ciphertext;
}

std::string private_decrypt(RSA *rsa, const std::string_view &data)
{
    std::string plaintext(RSA_size(rsa), '\0');
    int size = RSA_private_decrypt(
        data.size(), (const unsigned char *)data.data(),
        (unsigned char *)plaintext.data(), rsa, RSA_PKCS1_PADDING);
    plaintext.resize(size != -1 ? size : 0);
    return plaintext;
}

}

namespace hex {

std::string dump(const std::string_view &data)
{
    std::string results(data.size() << 1, '\0');
    for (size_t i = 0, n = data.size(); i < n; ++i) {
        static const char hash_table[] = "0123456789abcdef";
        results[i << 1] = hash_table[((unsigned char)data[i]) >> 4];
        results[(i << 1) + 1] = hash_table[((unsigned char)data[i]) & 15];
    }
    return results;
}

}

namespace sha {

std::string feed256(const std::string_view &data)
{
    std::string results(SHA256_DIGEST_LENGTH, '\0');
    SHA256((const unsigned char *)data.data(), data.size(),
        (unsigned char *)results.data());
    return results;
}

std::string feed512(const std::string_view &data)
{
    std::string results(SHA512_DIGEST_LENGTH, '\0');
    SHA512((const unsigned char *)data.data(), data.size(),
        (unsigned char *)results.data());
    return results;
}

}

namespace md5 {

std::string feed(const std::string_view &data)
{
    std::string results(MD5_DIGEST_LENGTH, '\0');
    MD5((const unsigned char *)data.data(), data.size(),
        (unsigned char *)results.data());
    return results;
}

}

namespace base64 {

std::string encode(const std::string_view &data)
{
    BIO *b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    BIO_push(b64, BIO_new(BIO_s_mem()));
    BIO_write(b64, data.data(), data.size());
    BIO_flush(b64);
    BUF_MEM *bptr = nullptr;
    BIO_get_mem_ptr(b64, &bptr);
    std::string results(bptr->data, bptr->length);
    BIO_free_all(b64);
    return results;
}

std::string decode(const std::string_view &data)
{
    BIO *b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    BIO_push(b64, BIO_new_mem_buf(data.data(), data.size()));
    std::string results(data.size() / 4 * 3, '\0');
    int bytes = BIO_read(b64, &results[0], results.size());
    results.resize(bytes > 0 ? bytes : 0);
    BIO_free_all(b64);
    return results;
}

}
