class Node:
    def __init__(self):
        self.parent = None
    def set_parent(self, parent):
        self.parent = parent

class Preprocessor(Node):
    def __init__(self, text):
        super().__init__()
        self.text = text

class Comment(Node):
    def __init__(self, text):
        super().__init__()
        self.text = text

class TableMemberVarDeclaration(Node):
    def __init__(self, memRule, memType, memVarList):
        super().__init__()
        self.memRule = memRule
        self.memType = memType
        self.memVarList = memVarList

class TableDeclarationList(Node):
    def __init__(self, other, declaration):
        super().__init__()
        self.declarationList = other and other.declarationList or []
        if declaration:
            self.declarationList.append(declaration)

class TableDefinition(Node):
    def __init__(self, name, declarationList, metaInfoList):
        super().__init__()
        self.name = name
        self.declarationList = declarationList
        self.metaInfoList = metaInfoList

class StructMemberVarDeclaration(Node):
    def __init__(self, memType, memVarList):
        super().__init__()
        self.memType = memType
        self.memVarList = memVarList

class StructDeclarationList(Node):
    def __init__(self, other, declaration):
        super().__init__()
        self.declarationList = other and other.declarationList or []
        if declaration:
            self.declarationList.append(declaration)

class StructDefinition(Node):
    def __init__(self, name, inheritBase, declarationList):
        super().__init__()
        self.name = name
        self.inheritBase = inheritBase
        self.declarationList = declarationList

class EnumDeclaration(Node):
    def __init__(self, memName, memValue = None):
        super().__init__()
        self.memName = memName
        self.memValue = memValue

class EnumDeclarationList(Node):
    def __init__(self, other, declaration):
        super().__init__()
        self.declarationList = other and other.declarationList or []
        if declaration:
            self.declarationList.append(declaration)

class EnumDefinition(Node):
    def __init__(self, name, declarationList):
        super().__init__()
        self.name = name
        self.declarationList = declarationList

class MemberVarTypeSpecifier(Node):
    # container: None, 'Sequence', 'Associative', 'Unique'
    def __init__(self, container, *parts):
        super().__init__()
        self.container = container
        self.parts = parts

class MemberVarVarDeclaratorList(Node):
    def __init__(self, other, varName):
        super().__init__()
        self.varDeclaratorList = other and other.varDeclaratorList or []
        self.varDeclaratorList.append(varName)

class ExtraMetaInfoList(Node):
    def __init__(self, other, infoName, infoValue):
        super().__init__()
        self.metaInfoList = other and other.metaInfoList or []
        self.metaInfoList.append((infoName, infoValue))

class DateTime(Node):
    def __init__(self, id):
        super().__init__()
        self.id = id

class Time(Node):
    def __init__(self, id):
        super().__init__()
        self.id = id

class NsIdList(Node):
    def __init__(self, other, id):
        super().__init__()
        self.idList = other and other.idList or []
        self.idList.append(id)

class TranslationUnit(Node):
    def __init__(self, other, externalDeclaration):
        super().__init__()
        self.externalDeclarations = other and other.externalDeclarations or []
        if externalDeclaration:
            self.externalDeclarations.append(externalDeclaration)
