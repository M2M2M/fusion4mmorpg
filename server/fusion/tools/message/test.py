from table_cxx_generator import *
from table_go_generator import *
from table_cs_generator import *
from table_lua_generator import *

msgHandlers = [
    CxxMessageHandler('protocol', '../../../GateServer/Session', 'ClientSession', 'pSession', 'ClientSessionHandler',
                      (), ('gate',), 'GAME_OPCODE::CSMSG_COUNT'),
    CxxMessageHandler('protocol', '../../../GateServer/Session', 'GameServerSession', 'pSession', 'GameServerSessionHandler',
                      ('SGG',), (), 'GateServer2GameServer::GATE2GAME_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../GateServer/Session', 'MapServerSession', 'pSession', 'MapServerSessionHandler',
                      ('SGM',), (), 'GateServer2MapServer::GATE2MAP_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../GateServer/Session', 'SocialServerSession', 'pSession', 'SocialServerSessionHandler',
                      ('SGT',), (), 'GateServer2SocialServer::GATE2SOCIAL_MESSAGE_COUNT'),

    CxxMessageHandler('protocol', '../../../GameServer/Session', 'Account', 'pAccount', 'ClientPacketHandler',
                      (), ('game',), 'GAME_OPCODE::CSMSG_COUNT'),
    CxxMessageHandler('protocol', '../../../GameServer/Session', 'GateServerSession', 'pSession', 'GateServerSessionHandler',
                      ('CGG',), (), 'GateServer2GameServer::GATE2GAME_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../GameServer/Session', 'MapServerSession', 'pSession', 'MapServerSessionHandler',
                      ('MS',), (), 'MapServer2GameServer::MAP2GAME_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../GameServer/Session', 'SocialServerSession', 'pSession', 'SocialServerSessionHandler',
                      ('SGX',), (), 'GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT'),

    CxxMessageHandler('protocol', '../../../MapServer/Session', 'Player', 'pPlayer', 'PlayerPacketHandler',
                      ('CMSG','G2M',), ('player',), 'GameServer2MapInstance::GS2MI_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../MapServer/Session', 'MapInstance', 'pInstance', 'InstancePacketHandler',
                      ('G2M',), ('instance',), 'GameServer2MapInstance::GS2MI_MESSAGE_COUNT', ('size_t','gsIdx')),
    CxxMessageHandler('protocol', '../../../MapServer/Session', 'GateServerSession', 'pSession', 'GateServerSessionHandler',
                      ('CGM',), (), 'GateServer2MapServer::GATE2MAP_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../MapServer/Session', 'GameServerSession', 'pSession', 'GameServerSessionHandler',
                      ('GS',), (), 'MapServer2GameServer::MAP2GAME_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../MapServer/Session', 'MapServerService', 'pService', 'MapServerPacketHandler',
                      ('SMC',), (), 'MapServer2CrossServer::MAP2CROSS_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../MapServer/Session', 'CrossServerService', 'pService', 'CrossServerPacketHandler',
                      ('CMC',), (), 'MapServer2CrossServer::MAP2CROSS_MESSAGE_COUNT'),

    CxxMessageHandler('protocol', '../../../SocialServer/Session', 'GateServerSession', 'pSession', 'GateServerSessionHandler',
                      ('CGT',), (), 'GateServer2SocialServer::GATE2SOCIAL_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../SocialServer/Session', 'GameServerSession', 'pSession', 'GameServerSessionHandler',
                      ('CGX',), (), 'GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../SocialServer/Session', 'GuildMgr', 'mgr', 'C2GuildPacketHandler',
                      (), ('guild',), 'GAME_OPCODE::CSMSG_COUNT', ('uint32','uid')),
    CxxMessageHandler('protocol', '../../../SocialServer/Session', 'GuildMgr', 'mgr', 'S2GuildPacketHandler',
                      (), ('s2guild',), 'GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../SocialServer/Session', 'RankMgr', 'mgr', 'C2RankPacketHandler',
                      (), ('rank',), 'GAME_OPCODE::CSMSG_COUNT', ('uint32','uid')),
    CxxMessageHandler('protocol', '../../../SocialServer/Session', 'RankMgr', 'mgr', 'S2RankPacketHandler',
                      (), ('s2rank',), 'GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT'),
    CxxMessageHandler('protocol', '../../../SocialServer/Session', 'OperatingActivityMgr', 'mgr', 'C2OperatingPacketHandler',
                      (), ('operating',), 'GAME_OPCODE::CSMSG_COUNT', ('uint32','uid')),
    CxxMessageHandler('protocol', '../../../SocialServer/Session', 'OperatingActivityMgr', 'mgr', 'S2OperatingPacketHandler',
                      (), ('s2operating',), 'GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT'),

    CxxMessageHandler('protocol', '../../../DBPServer/Session', 'GameSession', 'pSession', 'GameSessionHandler',
                      ('CDBP',), (), 'DBPProtocol::DBP_PROTOCOL_COUNT'),
    CxxMessageHandler('protocol', '../../../GameBase/Session', 'DBPSession', 'pSession', 'DBPSessionHandler',
                      ('SDBP',), (), 'DBPProtocol::DBP_PROTOCOL_COUNT'),

    CxxMessageHandler('protocol', '../../../GateServer/Session', 'CenterSession', 'pSession', 'CenterSessionHandler',
                      ('SCT',), ('ct2gate',), 'CenterProtocol::CENTER_PROTOCOL_COUNT'),
    CxxMessageHandler('protocol', '../../../GameServer/Session', 'CenterSession', 'pSession', 'CenterSessionHandler',
                      ('SCT',), ('ct2game',), 'CenterProtocol::CENTER_PROTOCOL_COUNT'),
]
to_cxx_files('../../../GameData/protocol/protocol.dsl', '../../../GameData/protocol', msgHandlers)

msgHandlers = [
    GoMessageHandler('test.com', 'session', 'protocol', '../../../../global/center/session', 'GameSession', 'GameSessionHandler',
                     ('CCT',), ('center',), 'CENTER_PROTOCOL_COUNT'),
    GoMessageHandler('test.com', 'session', 'protocol', '../../../../global/center/session', 'LoginServerSession', 'LoginServerSessionHandler',
                     ('CLC',), (), 'LOGIN2CENTER_MESSAGE_COUNT'),

    GoMessageHandler('test.com', 'session', 'protocol', '../../../../global/login/session', 'CenterServerSession', 'CenterServerSessionHandler',
                     ('SLC',), (), 'LOGIN2CENTER_MESSAGE_COUNT'),
]
to_go_files('../../../GameData/protocol/protocol.dsl', '../../../../global/protocol', msgHandlers)

msgHandlers = [
    CsMessageHandler('../../../../../kbengine_unity3d_demo/Assets/Scripts/Network', 'GameSessionHandler',
                     ('SMSG',), ('client',), 'GAME_OPCODE.CSMSG_COUNT'),
]
to_cs_files('../../../GameData/protocol/protocol.dsl', '../../../../../kbengine_unity3d_demo/Assets/Scripts/protocol', msgHandlers)

to_lua_files('../../../GameData/protocol/protocol.dsl', '../../../../middata/protocol')
