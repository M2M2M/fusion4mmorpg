class Node:
    def __init__(self):
        pass

class Preprocessor(Node):
    def __init__(self, text):
        self.text = text

class Comment(Node):
    def __init__(self, text):
        self.text = text

class EnumDeclaration(Node):
    def __init__(self, memName, memValue = None):
        self.memName = memName
        self.memValue = memValue

class EnumDeclarationList(Node):
    def __init__(self, other, declaration):
        self.declarationList = other and other.declarationList or []
        if declaration:
            self.declarationList.append(declaration)

class EnumDefinition(Node):
    def __init__(self, name, declarationList):
        self.name = name
        self.declarationList = declarationList

class NsIdList(Node):
    def __init__(self, other, id):
        self.idList = other and other.idList or []
        self.idList.append(id)

class TranslationUnit(Node):
    def __init__(self, other, externalDeclaration):
        self.externalDeclarations = other and other.externalDeclarations or []
        if externalDeclaration:
            self.externalDeclarations.append(externalDeclaration)
