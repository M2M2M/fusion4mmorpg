#include "preHeader.h"
#include "S2RankPacketHandler.h"
#include "protocol/InternalProtocol.h"

S2RankPacketHandler::S2RankPacketHandler()
{
	handlers_[GameServer2SocialServer::CGX_UPDATE_GAME_SERVER] = &S2RankPacketHandler::HandleUpdateGameServer;
	handlers_[GameServer2SocialServer::CGX_RANK_NEW_PLAYER] = &S2RankPacketHandler::HandleRankNewPlayer;
	handlers_[GameServer2SocialServer::CGX_RANK_UPDATE_PLAYER] = &S2RankPacketHandler::HandleRankUpdatePlayer;
	handlers_[GameServer2SocialServer::CGX_RANK_REMOVE_PLAYER] = &S2RankPacketHandler::HandleRankRemovePlayer;
	handlers_[GameServer2SocialServer::CGX_RANK_NEW_GUILD] = &S2RankPacketHandler::HandleRankNewGuild;
	handlers_[GameServer2SocialServer::CGX_RANK_UPDATE_GUILD] = &S2RankPacketHandler::HandleRankUpdateGuild;
	handlers_[GameServer2SocialServer::CGX_RANK_REMOVE_GUILD] = &S2RankPacketHandler::HandleRankRemoveGuild;
	rpc_handlers_[GameServer2SocialServer::CGX_RANK_GET_GUILD_RANK_VALUE] = &S2RankPacketHandler::HandleRankGetGuildRankValue;
	rpc_handlers_[GameServer2SocialServer::CGX_RANK_GET_GUILD_ID_LIST] = &S2RankPacketHandler::HandleRankGetGuildIdList;
};

S2RankPacketHandler::~S2RankPacketHandler()
{
}
