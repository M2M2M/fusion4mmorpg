#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/OpCode.h"

class GuildMgr;

class C2GuildPacketHandler :
	public MessageHandler<C2GuildPacketHandler, GuildMgr, GAME_OPCODE::CSMSG_COUNT, uint32>,
	public Singleton<C2GuildPacketHandler>
{
public:
	C2GuildPacketHandler();
	virtual ~C2GuildPacketHandler();
private:
	int HandleGuildGetList(GuildMgr *mgr, INetPacket &pck, uint32 uid);
	int HandleGuildGetInfo(GuildMgr *mgr, INetPacket &pck, uint32 uid);
	int HandleGuildGetMember(GuildMgr *mgr, INetPacket &pck, uint32 uid);
};

#define sC2GuildPacketHandler (*C2GuildPacketHandler::instance())
