#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class RankMgr;

class S2RankPacketHandler :
	public MessageHandler<S2RankPacketHandler, RankMgr, GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT>,
	public Singleton<S2RankPacketHandler>
{
public:
	S2RankPacketHandler();
	virtual ~S2RankPacketHandler();
private:
	int HandleUpdateGameServer(RankMgr *mgr, INetPacket &pck);
	int HandleRankNewPlayer(RankMgr *mgr, INetPacket &pck);
	int HandleRankUpdatePlayer(RankMgr *mgr, INetPacket &pck);
	int HandleRankRemovePlayer(RankMgr *mgr, INetPacket &pck);
	int HandleRankNewGuild(RankMgr *mgr, INetPacket &pck);
	int HandleRankUpdateGuild(RankMgr *mgr, INetPacket &pck);
	int HandleRankRemoveGuild(RankMgr *mgr, INetPacket &pck);
	int HandleRankGetGuildRankValue(RankMgr *mgr, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleRankGetGuildIdList(RankMgr *mgr, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
};

#define sS2RankPacketHandler (*S2RankPacketHandler::instance())
