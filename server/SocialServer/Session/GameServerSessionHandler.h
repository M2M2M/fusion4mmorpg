#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class GameServerSession;

class GameServerSessionHandler :
	public MessageHandler<GameServerSessionHandler, GameServerSession, GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT>,
	public Singleton<GameServerSessionHandler>
{
public:
	GameServerSessionHandler();
	virtual ~GameServerSessionHandler();
private:
	int HandleRegister(GameServerSession *pSession, INetPacket &pck);
	int HandlePushServerId(GameServerSession *pSession, INetPacket &pck);
	int HandleRestoreCharacters(GameServerSession *pSession, INetPacket &pck);
	int HandleCharacterOnline(GameServerSession *pSession, INetPacket &pck);
	int HandleCharacterOffline(GameServerSession *pSession, INetPacket &pck);
};

#define sGameServerSessionHandler (*GameServerSessionHandler::instance())
