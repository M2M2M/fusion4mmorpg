#include "preHeader.h"
#include "GuildPacketHandler.h"
#include "protocol/InternalProtocol.h"

GuildPacketHandler::GuildPacketHandler()
{
	handlers_[GameServer2SocialServer::CGX_GET_ALL_CHARACTER_GUILD] = &GuildPacketHandler::HandleGetAllCharacterGuild;
};

GuildPacketHandler::~GuildPacketHandler()
{
}
