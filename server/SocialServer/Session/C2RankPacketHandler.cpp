#include "preHeader.h"
#include "C2RankPacketHandler.h"
#include "protocol/OpCode.h"

C2RankPacketHandler::C2RankPacketHandler()
{
	handlers_[GAME_OPCODE::CMSG_RANK_GET_PLAYER_LIST] = &C2RankPacketHandler::HandleRankGetPlayerList;
	handlers_[GAME_OPCODE::CMSG_RANK_GET_GUILD_LIST] = &C2RankPacketHandler::HandleRankGetGuildList;
};

C2RankPacketHandler::~C2RankPacketHandler()
{
}
