#pragma once

class OperatingActivityBase;

class OperatingActivityFactory
{
public:
	static OperatingActivityBase* Create(const OperatingActivity* pProto);
};
