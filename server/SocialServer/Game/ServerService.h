#pragma once

class IServerService
{
public:
	IServerService(const std::string& name);
	virtual ~IServerService();

	struct Packet {
		enum Type {
			Client,
			Server,
		};
		Type type;
		union {
			uint32 uid;
		};
		INetPacket* pck = NULL;
	};

	static Packet NewClientPacket(uint32 uid, INetPacket* pck);
	static Packet NewServerPacket(INetPacket* pck);

	void PushRecvPacket(const Packet& packet);

	void UpdateAllPackets();

protected:
	virtual int HandleClientPacket(uint32 uid, INetPacket& pck) = 0;
	virtual int HandleServerPacket(INetPacket& pck) = 0;

private:
	int HandleOneRecvPacket(const Packet& packet);
	int HandleOneRecvClientPacket(const Packet& packet);
	int HandleOneRecvServerPacket(const Packet& packet);

	const std::string m_name;
	MultiBufferQueue<Packet, 1024> m_PacketStorage;
};
