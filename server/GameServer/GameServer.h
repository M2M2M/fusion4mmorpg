#pragma once

#include "Singleton.h"
#include "FrameWorker.h"

class GameServer : public FrameWorker, public AsyncTaskOwner,
	public WheelTimerOwner, public Singleton<GameServer>
{
public:
	THREAD_RUNTIME(GameServer)

	GameServer();
	virtual ~GameServer();

	bool IsServing() const { return m_isServing; }

	WheelTimerMgr sWheelTimerMgr;

private:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	virtual bool Initialize();
	virtual void Finish();

	virtual void Update(uint64 diffTime);
	virtual void OnTick();

	bool LoadData();

	bool m_isServing;
};

#define sGameServer (*GameServer::instance())
