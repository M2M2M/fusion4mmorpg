#include "preHeader.h"
#include "DataMgr.h"
#include "InstCfgMgr.h"

DataMgr::DataMgr()
: m_startServerTime(-1)
, m_lastOperatingEventTime(-1)
{
}

DataMgr::~DataMgr()
{
}

void DataMgr::LoadData()
{
	InitStartServerTime();
}

void DataMgr::InitStartServerTime()
{
	auto& cfgVal = sInstCfgMgr.GetInstCfgVal(InstCfg_ServerStartTime);
	if (!cfgVal.empty()) {
		m_startServerTime = std::stoll(cfgVal);
	} else {
		m_startServerTime = GET_UNIX_TIME;
		sInstCfgMgr.SaveInstCfgVal(
			InstCfg_ServerStartTime, std::to_string(m_startServerTime));
	}
}

void DataMgr::setOperatingCares(const std::string& operatingCares)
{
	m_operatingCares = std::bitset<(int)OperatingCareType::Count>(operatingCares);
}

void DataMgr::saveLastOperatingEventTime(int64 lastOperatingEventTime)
{
	m_lastOperatingEventTime = lastOperatingEventTime;
	sInstCfgMgr.SaveInstCfgVal(
		InstCfg_LastOperatingEventTime, std::to_string(m_lastOperatingEventTime));
}

void DataMgr::LoadLastOperatingEventTime()
{
	auto& cfgVal = sInstCfgMgr.GetInstCfgVal(InstCfg_LastOperatingEventTime);
	if (!cfgVal.empty()) {
		m_lastOperatingEventTime = std::stoll(cfgVal);
	}
}
