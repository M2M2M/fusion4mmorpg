#pragma once

#include "Singleton.h"
#include "rpc/RPCSession.h"
#include "Game/Character.h"

class SocialServerSessionHandler;

class SocialServerSession : public RPCSession, public Singleton<SocialServerSession>
{
public:
	SocialServerSession();
	virtual ~SocialServerSession();

	void CheckConnection();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnConnected();
	virtual void OnShutdownSession();
	virtual void DeleteObject();

	void PushServerId(uint32 serverId);

	void RestoreCharacters();
	void PushCharacterOnline(Character* pChar);
	void PushCharacterOffline(Character* pChar);

	void RPCReplyError(uint64 sn, GErrorCode errCode);

	void BroadcastPacket2AllServices(const INetPacket& pck);

	const std::string& getSocialListenAddr() const { return m_socialListenAddr; }
	const std::string& getSocialListenPort() const { return m_socialListenPort; }

private:
	void PullAllGuilds();
	void PullAllCharacterGuilds();

	void OnCharacterJoinGuild(Character* pChar, GUILD_JOIN_CAUSE cause,
		uint32 guildId, int8 guildTitle, const std::string_view& guildName);
	void OnCharacterQuitGuild(Character* pChar, GUILD_QUIT_CAUSE cause,
		uint32 guildId, const std::string_view& guildName);

	friend SocialServerSessionHandler;
	std::string m_socialListenAddr;
	std::string m_socialListenPort;
};

#define sSocialServerSession (*SocialServerSession::instance())
