#pragma once

#include "Singleton.h"
#include "GateServerSession.h"

class GateServerMgr : public Singleton<GateServerMgr>
{
public:
	GateServerMgr();
	virtual ~GateServerMgr();

	void AddGateServer(GateServerSession* pSession);
	void RemoveGateServer(GateServerSession* pSession);

	void BroadcastServerId2AllGateServer(uint32 serverId) const;
	void BroadcastSocialListen2AllGateServer(const std::string& addr, const std::string& port) const;

	void BroadcastPacket2AllClient(const INetPacket& data) const;

	void BroadcastPacket2AllGateServer(const INetPacket& pck) const;
	void BroadcastPacket2AllGateServer(const INetPacket& pck, const INetPacket& data) const;

private:
	std::unordered_map<uint32, GateServerSession*> m_GateServerMap;
};

#define sGateServerMgr (*GateServerMgr::instance())
