#include "preHeader.h"
#include "GateServerListener.h"
#include "GateServerSession.h"
#include "ServerMaster.h"

GateServerListener::GateServerListener()
: m_sn(0)
{
}

GateServerListener::~GateServerListener()
{
}

std::string GateServerListener::GetBindAddress()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("GATE_SERVER", "HOST", "0.0.0.0");
}

std::string GateServerListener::GetBindPort()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("GATE_SERVER", "PORT", "9990");
}

Session *GateServerListener::NewSessionObject()
{
	return new GateServerSession(++m_sn);
}
