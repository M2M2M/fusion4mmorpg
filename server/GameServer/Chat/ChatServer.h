#pragma once

#include "Singleton.h"
#include "Game/Character.h"

class ChatServer : public Singleton<ChatServer>
{
public:
	ChatServer();
	virtual ~ChatServer();

	void SendPacketToAllPlayer(const INetPacket& pck);
	void SendSysMsgToAll(ChannelType channelType, uint32 msgFlags,
		uint32 msgId, const std::string_view& msgArgs = emptyStringView);

	int HandleChatMessage(
		ChannelType channelType, Character* pChar, ObjGUID toGuid,
		const std::string_view& strMsg, const std::string_view& strArgs);

private:
	GErrorCode HandleChatMessage4Whisper(Character* pChar, ObjGUID toGuid,
		const std::string_view& strMsg, const std::string_view& strArgs);
	GErrorCode HandleChatMessage4Speak(Character* pChar,
		const std::string_view& strMsg, const std::string_view& strArgs);
	GErrorCode HandleChatMessage4Team(Character* pChar,
		const std::string_view& strMsg, const std::string_view& strArgs);
	GErrorCode HandleChatMessage4Guild(Character* pChar,
		const std::string_view& strMsg, const std::string_view& strArgs);
	GErrorCode HandleChatMessage4World(Character* pChar,
		const std::string_view& strMsg, const std::string_view& strArgs);
	void TransClientChatMessage(
		ChannelType channelType, Character* pChar, ObjGUID toGuid,
		const std::string_view& strMsg, const std::string_view& strArgs);
	void BuildClientChatMessagePacket(
		INetPacket& pck, ChannelType channelType, Character* pChar,
		const std::string_view& strMsg, const std::string_view& strArgs);
};

#define sChatServer (*ChatServer::instance())
