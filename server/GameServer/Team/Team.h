#pragma once

#include "Game/Character.h"

class Team
{
public:
	Team(uint32 teamId);
	~Team();

	void AddMember(Character* pChar);
	void RemoveMemeber(Character* pChar, Character* pKicker);
	void ChangeLeader(Character* pChar);

	void SendTeamSyncInfoToAllMembers() const;
	void SendTeamInfoToAllMembers() const;
	void SendTeamInfoToAllMemberInstances() const;
	void SendTeamInfoToInstance(InstGUID instGuid) const;

	void SendPacketToAllMembers(const INetPacket& pck) const;
	void SendPacketToAllMemberInstances(const INetPacket& pck) const;

	bool IsTeamFull() const;
	bool IsTeamMember(ObjGUID guid) const;
	bool HasTeamMemberOnline() const;

	uint32 getTeamId() const { return m_teamId; }
	Character* getTeamLeader() const { return m_teamLeader; }
	const std::unordered_map<ObjGUID, Character*>& getTeamMembers() const {
		return m_teamMemebers;
	}

private:
	Character* GetPriorMember() const;

	void PackTeamSyncInfo4Member(INetPacket& pck) const;
	void PackTeamInfo4Member(INetPacket& pck) const;
	void PackTeamInfo4Instance(INetPacket& pck) const;

	const uint32 m_teamId;
	std::unordered_map<ObjGUID, Character*> m_teamMemebers;
	Character* m_teamLeader;
};

inline bool Team::IsTeamFull() const {
	return m_teamMemebers.size() >= MAX_TEAM_MEMBER;
}
inline bool Team::IsTeamMember(ObjGUID guid) const {
	return m_teamMemebers.count(guid) != 0;
}
