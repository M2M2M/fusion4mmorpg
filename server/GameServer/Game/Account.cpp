#include "preHeader.h"
#include "Account.h"
#include "Character.h"
#include "Session/GateServerSession.h"

Account::Account(GateServerSession* pSession, uint32 logicGsID, uint32 uid, uint32 sn)
: m_pSession(pSession)
, logicGsID(logicGsID)
, uid(uid)
, sn(sn)
, isCharLoginning(false)
, m_pChar(NULL)
{
}

Account::~Account()
{
}

void Account::SendError(GErrorCode error) const
{
	SendRespError(SMSG_ERROR, error);
}

void Account::SendError(const GErrorCodeP1& error) const
{
	SendRespError(SMSG_ERROR, {error.errCode, error.p1});
}

void Account::SendError(const GErrorCodeP2& error) const
{
	SendRespError(SMSG_ERROR, {error.errCode, error.p1, error.p2});
}

void Account::SendError(const GErrorCodeP3& error) const
{
	SendRespError(SMSG_ERROR, {error.errCode, error.p1, error.p2, error.p3});
}

void Account::SendError(const GErrorInfo& error) const
{
	SendRespError(SMSG_ERROR, error);
}

void Account::SendRespError(uint32 respOpCode, const GErrorInfo& error) const
{
	NetPacket pack(respOpCode);
	(pack << error.errCode).Append(error.errArgs.data(), error.errArgs.size());
	SendPacket(pack);
}

void Account::SendPacket(const INetPacket& pck) const
{
	NetPacket trans(SGG_TRANS_CLIENT_PACKET);
	trans << sn;
	m_pSession->PushSendPacket(trans, pck);
}

void Account::SendPacket2GateServer(const INetPacket& pck) const
{
	m_pSession->PushSendPacket(pck);
}

void Account::SetCharacter(Character* pChar)
{
	m_pChar = pChar;
	NetPacket pack(SGG_PUSH_CHARACTER);
	pack << sn << u32(pChar != NULL ? pChar->guid.UID : 0);
	SendPacket2GateServer(pack);
}
