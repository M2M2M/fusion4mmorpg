#include "preHeader.h"
#include "CrossServerService.h"
#include "Session/CrossServerPacketHandler.h"

CrossServerService::CrossServerService()
: IServerService("CrossServerService")
{
}

CrossServerService::~CrossServerService()
{
}

int CrossServerService::HandlePacket(INetPacket& pck)
{
	return sCrossServerPacketHandler.HandlePacket(this, pck);
}
