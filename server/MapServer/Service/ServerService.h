#pragma once

class IServerService
{
public:
	IServerService(const std::string& name);
	virtual ~IServerService();

	void PushRecvPacket(INetPacket* pck);

	void UpdateAllPackets();

protected:
	virtual void OnRecvPacket(INetPacket* pck);

	virtual int HandlePacket(INetPacket& pck) = 0;

private:
	int HandleOneRecvPacket(INetPacket& pck);

	const std::string m_name;
	MultiBufferQueue<INetPacket*, 1024> m_PacketStorage;
};
