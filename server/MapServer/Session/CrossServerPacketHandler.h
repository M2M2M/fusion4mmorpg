#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class CrossServerService;

class CrossServerPacketHandler :
	public MessageHandler<CrossServerPacketHandler, CrossServerService, MapServer2CrossServer::MAP2CROSS_MESSAGE_COUNT>,
	public Singleton<CrossServerPacketHandler>
{
public:
	CrossServerPacketHandler();
	virtual ~CrossServerPacketHandler();
private:
	int HandlePlaybossChangeTeam(CrossServerService *pService, INetPacket &pck);
	int HandlePlaybossBroadcast(CrossServerService *pService, INetPacket &pck);
	int HandlePlaybossStrike(CrossServerService *pService, INetPacket &pck);
	int HandlePlaybossSync(CrossServerService *pService, INetPacket &pck);
	int HandlePlaybossJoin(CrossServerService *pService, INetPacket &pck);
};

#define sCrossServerPacketHandler (*CrossServerPacketHandler::instance())
