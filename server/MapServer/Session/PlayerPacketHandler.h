#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class Player;

class PlayerPacketHandler :
	public MessageHandler<PlayerPacketHandler, Player, GameServer2MapInstance::GS2MI_MESSAGE_COUNT>,
	public Singleton<PlayerPacketHandler>
{
public:
	PlayerPacketHandler();
	virtual ~PlayerPacketHandler();
private:
	int HandleCharacterTeleportFailed(Player *pPlayer, INetPacket &pck);
	int HandleCharacterLeaveMap(Player *pPlayer, INetPacket &pck);
	int HandleTryGuildCreate(Player *pPlayer, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGuildCreateResp(Player *pPlayer, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleJoinGuildEvent(Player *pPlayer, INetPacket &pck);
	int HandleQuitGuildEvent(Player *pPlayer, INetPacket &pck);
	int HandleGetChatMessageThing(Player *pPlayer, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleBuySpecialShopItem(Player *pPlayer, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleGetOperatingRewardItems(Player *pPlayer, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandleMoveStart(Player *pPlayer, INetPacket &pck);
	int HandleMoveStop(Player *pPlayer, INetPacket &pck);
	int HandleMoveSync(Player *pPlayer, INetPacket &pck);
	int HandleMoveTurnDir(Player *pPlayer, INetPacket &pck);
	int HandleChatMessage(Player *pPlayer, INetPacket &pck);
	int HandleMail(Player *pPlayer, INetPacket &pck);
	int HandleUseItem(Player *pPlayer, INetPacket &pck);
	int HandleDestroyItem(Player *pPlayer, INetPacket &pck);
	int HandleArrangeItems(Player *pPlayer, INetPacket &pck);
	int HandleSwapItem(Player *pPlayer, INetPacket &pck);
	int HandleSplitItem(Player *pPlayer, INetPacket &pck);
	int HandleEquipItem(Player *pPlayer, INetPacket &pck);
	int HandleUnequipItem(Player *pPlayer, INetPacket &pck);
	int HandleEnterQuestGuide(Player *pPlayer, INetPacket &pck);
	int HandleQueryQuestGiver(Player *pPlayer, INetPacket &pck);
	int HandleAcceptQuest(Player *pPlayer, INetPacket &pck);
	int HandleCancelQuest(Player *pPlayer, INetPacket &pck);
	int HandleSubmitQuest(Player *pPlayer, INetPacket &pck);
	int HandleQueryQuestNavigation(Player *pPlayer, INetPacket &pck);
	int HandleObjectInteractive(Player *pPlayer, INetPacket &pck);
	int HandlePlayerRevive(Player *pPlayer, INetPacket &pck);
	int HandleSpellCast(Player *pPlayer, INetPacket &pck);
	int HandleSpellInterrupt(Player *pPlayer, INetPacket &pck);
	int HandleLocTeleportFinish(Player *pPlayer, INetPacket &pck);
	int HandleGetShopItemList(Player *pPlayer, INetPacket &pck);
	int HandleBuyShopItem(Player *pPlayer, INetPacket &pck);
	int HandleOperatingGetList(Player *pPlayer, INetPacket &pck);
	int HandleOperatingGetDetail(Player *pPlayer, INetPacket &pck);
	int HandleOperatingGetShortHot(Player *pPlayer, INetPacket &pck);
	int HandleOperatingBuy(Player *pPlayer, INetPacket &pck);
	int HandleOperatingGetReward(Player *pPlayer, INetPacket &pck);
	int HandleStartStoryMode(Player *pPlayer, INetPacket &pck);
};

#define sPlayerPacketHandler (*PlayerPacketHandler::instance())
