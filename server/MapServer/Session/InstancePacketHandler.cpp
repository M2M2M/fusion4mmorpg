#include "preHeader.h"
#include "InstancePacketHandler.h"
#include "protocol/InternalProtocol.h"

InstancePacketHandler::InstancePacketHandler()
{
	handlers_[GameServer2MapInstance::G2M_INITIALIZE_INSTANCE] = &InstancePacketHandler::HandleInitializeInstance;
	handlers_[GameServer2MapInstance::G2M_CHARACTER_TELEPORT_BEGIN_ENTER_INSTANCE_CANCEL] = &InstancePacketHandler::HandleCharacterTeleportBeginEnterInstanceCancel;
	handlers_[GameServer2MapInstance::G2M_CHARACTER_ENTER_MAP] = &InstancePacketHandler::HandleCharacterEnterMap;
	handlers_[GameServer2MapInstance::G2M_CHARACTER_LOGOUT_GAME] = &InstancePacketHandler::HandleCharacterLogoutGame;
	handlers_[GameServer2MapInstance::G2M_TEAM_INFO] = &InstancePacketHandler::HandleTeamInfo;
	handlers_[GameServer2MapInstance::G2M_TEAM_MEMBER_ADD] = &InstancePacketHandler::HandleTeamMemberAdd;
	handlers_[GameServer2MapInstance::G2M_TEAM_MEMBER_QUIT] = &InstancePacketHandler::HandleTeamMemberQuit;
	handlers_[GameServer2MapInstance::G2M_TEAM_LEADER_CHANGE] = &InstancePacketHandler::HandleTeamLeaderChange;
	handlers_[GameServer2MapInstance::G2M_TEAM_DISBAND] = &InstancePacketHandler::HandleTeamDisband;
	handlers_[GameServer2MapInstance::G2M_PLAYBOSS_STRIKE] = &InstancePacketHandler::HandlePlaybossStrike;
	handlers_[GameServer2MapInstance::G2M_PLAYBOSS_DEAD] = &InstancePacketHandler::HandlePlaybossDead;
	handlers_[GameServer2MapInstance::G2M_PLAYBOSS_STOP] = &InstancePacketHandler::HandlePlaybossStop;
};

InstancePacketHandler::~InstancePacketHandler()
{
}
