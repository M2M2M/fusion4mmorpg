#include "preHeader.h"
#include "PlayerPacketHandler.h"
#include "protocol/InternalProtocol.h"
#include "protocol/OpCode.h"

PlayerPacketHandler::PlayerPacketHandler()
{
	handlers_[GameServer2MapInstance::G2M_CHARACTER_TELEPORT_FAILED] = &PlayerPacketHandler::HandleCharacterTeleportFailed;
	handlers_[GameServer2MapInstance::G2M_CHARACTER_LEAVE_MAP] = &PlayerPacketHandler::HandleCharacterLeaveMap;
	rpc_handlers_[GameServer2MapInstance::G2M_TRY_GUILD_CREATE] = &PlayerPacketHandler::HandleTryGuildCreate;
	rpc_handlers_[GameServer2MapInstance::G2M_GUILD_CREATE_RESP] = &PlayerPacketHandler::HandleGuildCreateResp;
	handlers_[GameServer2MapInstance::G2M_JOIN_GUILD_EVENT] = &PlayerPacketHandler::HandleJoinGuildEvent;
	handlers_[GameServer2MapInstance::G2M_QUIT_GUILD_EVENT] = &PlayerPacketHandler::HandleQuitGuildEvent;
	rpc_handlers_[GameServer2MapInstance::G2M_GET_CHAT_MESSAGE_THING] = &PlayerPacketHandler::HandleGetChatMessageThing;
	rpc_handlers_[GameServer2MapInstance::G2M_BUY_SPECIAL_SHOP_ITEM] = &PlayerPacketHandler::HandleBuySpecialShopItem;
	rpc_handlers_[GameServer2MapInstance::G2M_GET_OPERATING_REWARD_ITEMS] = &PlayerPacketHandler::HandleGetOperatingRewardItems;
	handlers_[GAME_OPCODE::CMSG_MOVE_START] = &PlayerPacketHandler::HandleMoveStart;
	handlers_[GAME_OPCODE::CMSG_MOVE_STOP] = &PlayerPacketHandler::HandleMoveStop;
	handlers_[GAME_OPCODE::CMSG_MOVE_SYNC] = &PlayerPacketHandler::HandleMoveSync;
	handlers_[GAME_OPCODE::CMSG_MOVE_TURN_DIR] = &PlayerPacketHandler::HandleMoveTurnDir;
	handlers_[GAME_OPCODE::CMSG_CHAT_MESSAGE] = &PlayerPacketHandler::HandleChatMessage;
	handlers_[GAME_OPCODE::CMSG_MAIL] = &PlayerPacketHandler::HandleMail;
	handlers_[GAME_OPCODE::CMSG_USE_ITEM] = &PlayerPacketHandler::HandleUseItem;
	handlers_[GAME_OPCODE::CMSG_DESTROY_ITEM] = &PlayerPacketHandler::HandleDestroyItem;
	handlers_[GAME_OPCODE::CMSG_ARRANGE_ITEMS] = &PlayerPacketHandler::HandleArrangeItems;
	handlers_[GAME_OPCODE::CMSG_SWAP_ITEM] = &PlayerPacketHandler::HandleSwapItem;
	handlers_[GAME_OPCODE::CMSG_SPLIT_ITEM] = &PlayerPacketHandler::HandleSplitItem;
	handlers_[GAME_OPCODE::CMSG_EQUIP_ITEM] = &PlayerPacketHandler::HandleEquipItem;
	handlers_[GAME_OPCODE::CMSG_UNEQUIP_ITEM] = &PlayerPacketHandler::HandleUnequipItem;
	handlers_[GAME_OPCODE::CMSG_ENTER_QUEST_GUIDE] = &PlayerPacketHandler::HandleEnterQuestGuide;
	handlers_[GAME_OPCODE::CMSG_QUERY_QUEST_GIVER] = &PlayerPacketHandler::HandleQueryQuestGiver;
	handlers_[GAME_OPCODE::CMSG_ACCEPT_QUEST] = &PlayerPacketHandler::HandleAcceptQuest;
	handlers_[GAME_OPCODE::CMSG_CANCEL_QUEST] = &PlayerPacketHandler::HandleCancelQuest;
	handlers_[GAME_OPCODE::CMSG_SUBMIT_QUEST] = &PlayerPacketHandler::HandleSubmitQuest;
	handlers_[GAME_OPCODE::CMSG_QUERY_QUEST_NAVIGATION] = &PlayerPacketHandler::HandleQueryQuestNavigation;
	handlers_[GAME_OPCODE::CMSG_OBJECT_INTERACTIVE] = &PlayerPacketHandler::HandleObjectInteractive;
	handlers_[GAME_OPCODE::CMSG_PLAYER_REVIVE] = &PlayerPacketHandler::HandlePlayerRevive;
	handlers_[GAME_OPCODE::CMSG_SPELL_CAST] = &PlayerPacketHandler::HandleSpellCast;
	handlers_[GAME_OPCODE::CMSG_SPELL_INTERRUPT] = &PlayerPacketHandler::HandleSpellInterrupt;
	handlers_[GAME_OPCODE::CMSG_LOC_TELEPORT_FINISH] = &PlayerPacketHandler::HandleLocTeleportFinish;
	handlers_[GAME_OPCODE::CMSG_GET_SHOP_ITEM_LIST] = &PlayerPacketHandler::HandleGetShopItemList;
	handlers_[GAME_OPCODE::CMSG_BUY_SHOP_ITEM] = &PlayerPacketHandler::HandleBuyShopItem;
	handlers_[GAME_OPCODE::CMSG_OPERATING_GET_LIST] = &PlayerPacketHandler::HandleOperatingGetList;
	handlers_[GAME_OPCODE::CMSG_OPERATING_GET_DETAIL] = &PlayerPacketHandler::HandleOperatingGetDetail;
	handlers_[GAME_OPCODE::CMSG_OPERATING_GET_SHORT_HOT] = &PlayerPacketHandler::HandleOperatingGetShortHot;
	handlers_[GAME_OPCODE::CMSG_OPERATING_BUY] = &PlayerPacketHandler::HandleOperatingBuy;
	handlers_[GAME_OPCODE::CMSG_OPERATING_GET_REWARD] = &PlayerPacketHandler::HandleOperatingGetReward;
	handlers_[GAME_OPCODE::CMSG_START_STORY_MODE] = &PlayerPacketHandler::HandleStartStoryMode;
};

PlayerPacketHandler::~PlayerPacketHandler()
{
}
