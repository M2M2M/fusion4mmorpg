#include "preHeader.h"
#include "GameServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

GameServerSessionHandler::GameServerSessionHandler()
{
	handlers_[MapServer2GameServer::GS_REGISTER_RESP] = &GameServerSessionHandler::HandleRegisterResp;
	handlers_[MapServer2GameServer::GS_PUSH_SERVER_ID] = &GameServerSessionHandler::HandlePushServerId;
	handlers_[MapServer2GameServer::GS_START_WORLD_MAP] = &GameServerSessionHandler::HandleStartWorldMap;
	handlers_[MapServer2GameServer::GS_START_INSTANCE] = &GameServerSessionHandler::HandleStartInstance;
	handlers_[MapServer2GameServer::GS_STOP_INSTANCE] = &GameServerSessionHandler::HandleStopInstance;
	handlers_[MapServer2GameServer::GS_CHARACTER_TELEPORT_BEGIN_ENTER_INSTANCE] = &GameServerSessionHandler::HandleCharacterTeleportBeginEnterInstance;
	handlers_[MapServer2GameServer::GS_SYNC_PLAYER_LEVEL_MAX] = &GameServerSessionHandler::HandleSyncPlayerLevelMax;
	handlers_[MapServer2GameServer::GS_SYNC_OPERATING_CARES] = &GameServerSessionHandler::HandleSyncOperatingCares;
};

GameServerSessionHandler::~GameServerSessionHandler()
{
}
