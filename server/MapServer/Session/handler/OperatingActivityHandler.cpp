#include "preHeader.h"
#include "Session/PlayerPacketHandler.h"
#include "Object/Player.h"

int PlayerPacketHandler::HandleGetOperatingRewardItems(Player *pPlayer, INetPacket &pck, const RPCActor::RequestMetaInfo &info)
{
	bool isAllowMail;
	uint32 actType, actTimes, times;
	pck >> actType >> actTimes >> isAllowMail >> times;

	auto n = pck.Read<uint16>();
	std::vector<ChequeInfo> chequeInfos;
	std::vector<inst_item_prop> itemProps;
	chequeInfos.reserve(n);
	itemProps.reserve(n);
	for (uint16 i = 0; i < n; ++i) {
		FItemInfo rewardItem;
		LoadFromINetStream(rewardItem, pck);
		if (rewardItem.id < ITEM_CHEQUE_TYPE) {
			chequeInfos.push_back(
				NewChequeInfo(rewardItem.id, rewardItem.num * times));
		} else {
			itemProps.push_back(NewItemProp4Flags(
				rewardItem.id, rewardItem.num * times, rewardItem.flags));
		}
	}

	NetPacket rpcRespPck(CGX_RPC_INVOKE_RESP);
	size_t anchor = rpcRespPck.Placeholder<int32>(CommonSuccess);
	_defer_r(pPlayer->RPCReply2Ss(rpcRespPck, info.sn));

	if (!isAllowMail) {
		auto lackSlotCnt = pPlayer->GetItemStorage()->
			GetFillItemsLackSlotCount(NULL, itemProps.data(), itemProps.size());
		if (lackSlotCnt > 0) {
			rpcRespPck.Put(anchor, (int32)ErrItemStorageSlotNotEnough);
			return SessionHandleSuccess;
		}
	}

	pPlayer->GetItemStorage()->CreateAddItemsMail(
		NULL, itemProps.data(), itemProps.size(),
		ITEM_FLOW_TYPE(IFT_OPERATING_BEGIN + actType), {actTimes}, true);
	for (auto& chequeInfo : chequeInfos) {
		pPlayer->GainCheque((ChequeType)chequeInfo.type, chequeInfo.value,
			CHEQUE_FLOW_TYPE(CFT_OPERATING_BEGIN + actType), {actTimes}, true);
	}

	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleOperatingGetList(Player *pPlayer, INetPacket &pck)
{
	int32 actUIType;
	pck >> actUIType;
	NetPacket trans2SS(CGX_OPERATING_GET_LIST);
	trans2SS << actUIType;
	pPlayer->SendOperatingPacket2XSs(trans2SS);
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleOperatingGetDetail(Player *pPlayer, INetPacket &pck)
{
	uint64 actUniqueKey;
	pck >> actUniqueKey;
	NetPacket trans2SS(CGX_OPERATING_GET_DETAIL);
	trans2SS << actUniqueKey;
	pPlayer->SendOperatingPacket2XSs(trans2SS);
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleOperatingGetShortHot(Player *pPlayer, INetPacket &pck)
{
	uint64 actUniqueKey;
	pck >> actUniqueKey;
	NetPacket trans2SS(CGX_OPERATING_GET_SHORT_HOT);
	trans2SS << actUniqueKey;
	pPlayer->SendOperatingPacket2XSs(trans2SS);
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleOperatingBuy(Player *pPlayer, INetPacket &pck)
{
	uint64 actUniqueKey;
	uint32 index;
	pck >> actUniqueKey >> index;
	NetPacket trans2SS(CGX_OPERATING_BUY);
	trans2SS << actUniqueKey << index;
	pPlayer->SendOperatingPacket2XSs(trans2SS);
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleOperatingGetReward(Player *pPlayer, INetPacket &pck)
{
	uint64 actUniqueKey;
	uint32 index;
	pck >> actUniqueKey >> index;
	NetPacket trans2SS(CGX_OPERATING_GET_REWARD);
	trans2SS << actUniqueKey << index;
	pPlayer->SendOperatingPacket2XSs(trans2SS);
	return SessionHandleSuccess;
}
