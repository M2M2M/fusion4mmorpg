#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class MapInstance;

class InstancePacketHandler :
	public MessageHandler<InstancePacketHandler, MapInstance, GameServer2MapInstance::GS2MI_MESSAGE_COUNT, size_t>,
	public Singleton<InstancePacketHandler>
{
public:
	InstancePacketHandler();
	virtual ~InstancePacketHandler();
private:
	int HandleInitializeInstance(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandleCharacterTeleportBeginEnterInstanceCancel(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandleCharacterEnterMap(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandleCharacterLogoutGame(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandleTeamInfo(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandleTeamMemberAdd(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandleTeamMemberQuit(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandleTeamLeaderChange(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandleTeamDisband(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandlePlaybossStrike(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandlePlaybossDead(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
	int HandlePlaybossStop(MapInstance *pInstance, INetPacket &pck, size_t gsIdx);
};

#define sInstancePacketHandler (*InstancePacketHandler::instance())
