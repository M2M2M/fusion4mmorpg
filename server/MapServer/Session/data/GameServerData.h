#pragma once

class GameServerData
{
public:
	GameServerData();
	~GameServerData();

public:
	void SetPlayerLevelMax(uint32 playerLevelMax);
	uint32 GetPlayerLevelMax() const { return m_playerLevelMax; }
private:
	uint32 m_playerLevelMax;

public:
	void SetOperatingCares(const std::string& operatingCares);
	bool IsOperatingCare(int i) const { return m_operatingCares.test(i); }
	const auto& GetOperatingCares() const { return m_operatingCares; }
private:
	std::bitset<(int)OperatingCareType::Count> m_operatingCares;
};
