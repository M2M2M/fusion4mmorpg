#pragma once

class MapInstance;
class Player;

class MapTeam
{
public:
	MapTeam(MapInstance* pMapInstance, uint16 gsId, uint32 teamId);
	~MapTeam();

	void LoadTeamFromGSPacket(INetPacket& pack);
	void AddPlayer(Player* pPlayer);
	void RemovePlayer(Player* pPlayer);
	void ChangeLeader(ObjGUID playerGuid, const std::string& playerName);

	bool HasMember(ObjGUID guid) const;
	size_t GetInMapMemberCount() const;
	size_t GetAllMemberCount() const;

	uint16 GetGsId() const { return m_gsId; }
	uint32 GetId() const { return m_teamId; }
	ObjGUID GetLeader() const { return m_teamLeader; }
	const std::string& GetName() const { return m_teamName; }

	const std::unordered_map<ObjGUID, Player*>& getInMapTeamMembers() const {
		return m_inMapTeamMemebers;
	}
	const std::unordered_set<ObjGUID>& getAllTeamMembers() const {
		return m_allTeamMembers;
	}

private:
	MapInstance* const m_pMapInstance;
	const uint16 m_gsId;
	const uint32 m_teamId;
	std::unordered_map<ObjGUID, Player*> m_inMapTeamMemebers;
	std::unordered_set<ObjGUID> m_allTeamMembers;
	std::string m_teamName;
	ObjGUID m_teamLeader;
};

inline bool MapTeam::HasMember(ObjGUID guid) const {
	return m_allTeamMembers.count(guid) != 0;
}
inline size_t MapTeam::GetInMapMemberCount() const {
	return m_inMapTeamMemebers.size();
}
inline size_t MapTeam::GetAllMemberCount() const {
	return m_allTeamMembers.size();
}
