#pragma once

#include "Singleton.h"
#include "Spell.h"

class SpellMgr : public Singleton<SpellMgr>
{
public:
	SpellMgr();
	virtual ~SpellMgr();

	bool LoadSpellRelation();

	const SpellPrototype* GetSpellPrototype(uint32 spellID) const;

private:
	std::unordered_map<uint32, SpellPrototype> m_spellList;
};

#define sSpellMgr (*SpellMgr::instance())
