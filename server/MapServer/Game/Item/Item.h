#pragma once

class Player;
class AttrPartProxy;

class Item : public lua::binder, public WheelTimerOwner
{
public:
	Item(Player* pOwner);
	~Item();

	Player* GetOwner() const { return m_pOwner; }
	uint32 GetItemGuid() const { return m_itemProp.itemGuid; }
	uint32 GetItemTypeID() const { return m_pItemProto->itemTypeID; }
	uint32 GetItemCount() const { return m_itemProp.itemCount; }
	const ItemEquipPrototype* GetEquipProto() const { return m_pEquipProto; }
	const ItemPrototype* GetItemProto() const { return m_pItemProto; }
	const inst_item_prop& GetItemProp() const { return m_itemProp; }
	bool IsItemBinding() const { return m_itemProp.itemOwner != 0; }

	GErrorCode Load(const ItemPrototype* pItemProto,
		const inst_item_prop& itemProp, uint32 itemCount, bool isNew);
	void SetItemSlot(ItemSlotType type, uint32 slot);

	void AddItemCount(uint32 num,
		ITEM_FLOW_TYPE flowType, params<uint32> flowParams, uint32 flags = 0);
	void SubItemCount(uint32 num,
		ITEM_FLOW_TYPE flowType, params<uint32> flowParams, uint32 flags = 0);

private:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	Player* const m_pOwner;
	const ItemEquipPrototype* m_pEquipProto;
	const ItemPrototype* m_pItemProto;
	inst_item_prop m_itemProp;
	ItemSlotType m_itemSlotType;
	uint32 m_itemSlot;

public:
	void ApplyAttributes(AttrPartProxy& proxy) const;
};
