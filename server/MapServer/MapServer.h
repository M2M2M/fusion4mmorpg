#pragma once

#include "Singleton.h"
#include "FrameWorker.h"
#include "Service/ServerService.h"

class MapServer : public FrameWorker, public AsyncTaskOwner,
	public WheelTimerOwner, public WheelTriggerOwner,
	public Singleton<MapServer>
{
public:
	THREAD_RUNTIME(MapServer)

	MapServer();
	virtual ~MapServer();

	static void SendSingleMail(uint16 gsId, const inst_mail& mailProp);
	static void SendMultiMail(const std::vector<ObjGUID>& receiverGuids,
		const inst_mail& mailProp);
	static void SendMultiMail(const std::vector<uint16>& receiverGsIds,
		const std::vector<inst_mail>& mailProps);

	static void SendPacketToAllPlayer(const INetPacket& pck);
	static void SendSysMsgToAll(ChannelType channelType, uint32 msgFlags,
		uint32 msgId, const std::string_view& msgArgs = emptyStringView);

	static void KickPlayer(ObjGUID guid, GErrorCode error);

	static void OfflinePlayer(size_t gsIdx = -1);
	static void OfflineAllPlayer();

	IServerService* GetService() const { return m_pService; }

	WheelTimerMgr sWheelTimerMgr;

private:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	virtual bool Initialize();
	virtual void Finish();

	virtual void Update(uint64 diffTime);
	virtual void OnTick();

	void OnNewDaily();
	void OnNewWeekly();
	void OnNewMonthly();

	IServerService* const m_pService;
};

#define sMapServer (*MapServer::instance())
