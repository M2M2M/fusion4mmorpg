#include "preHeader.h"
#include "GateServer.h"
#include "Game/TransMgr.h"
#include "Session/MapServerMgr.h"

GateServer::GateServer()
: sWheelTimerMgr(1, GET_UNIX_TIME)
{
}

GateServer::~GateServer()
{
}

WheelTimerMgr *GateServer::GetWheelTimerMgr()
{
	return &sWheelTimerMgr;
}

void GateServer::Finish()
{
	sSessionManager.Stop();
	return FrameWorker::Finish();
}

void GateServer::Update(uint64 diffTime)
{
	AsyncTaskOwner::UpdateTask();
	sSessionManager.Update();
}

void GateServer::OnTick()
{
	sTransMgr.RunGC();
	sSessionManager.Tick();
	sMapServerMgr.CheckConnections();
	sWheelTimerMgr.Update(GET_UNIX_TIME);
}
