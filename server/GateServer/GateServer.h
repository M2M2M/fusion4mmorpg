#pragma once

#include "Singleton.h"
#include "FrameWorker.h"

class GateServer : public FrameWorker, public AsyncTaskOwner,
	public WheelTimerOwner, public Singleton<GateServer>
{
public:
	THREAD_RUNTIME(GateServer)

	GateServer();
	virtual ~GateServer();

	WheelTimerMgr sWheelTimerMgr;

private:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	virtual void Finish();

	virtual void Update(uint64 diffTime);
	virtual void OnTick();
};

#define sGateServer (*GateServer::instance())
