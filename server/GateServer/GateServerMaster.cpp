#include "preHeader.h"
#include "GateServerMaster.h"
#include "GateServer.h"
#include "Session/CenterSession.h"
#include "Session/SocialServerSession.h"
#include "Session/ClientListener.h"
#include "Session/ClientSessionHandler.h"
#include "Session/GameServerSessionHandler.h"
#include "Session/MapServerSessionHandler.h"
#include "Session/CenterSessionHandler.h"
#include "Session/SocialServerSessionHandler.h"
#include "Session/GameServerSession.h"
#include "Session/ClientMgr.h"
#include "Session/MapServerMgr.h"
#include "Game/TransMgr.h"

GateServerMaster::GateServerMaster()
{
}

GateServerMaster::~GateServerMaster()
{
}

bool GateServerMaster::InitSingleton()
{
	CenterSession::newInstance();
	SocialServerSession::newInstance();
	ClientSessionHandler::newInstance();
	GameServerSessionHandler::newInstance();
	MapServerSessionHandler::newInstance();
	CenterSessionHandler::newInstance();
	SocialServerSessionHandler::newInstance();
	ClientListener::newInstance();
	GameServerSession::newInstance();
	ClientMgr::newInstance();
	MapServerMgr::newInstance();
	TransMgr::newInstance();
	GateServer::newInstance();
	return true;
}

void GateServerMaster::FinishSingleton()
{
	CenterSession::deleteInstance();
	SocialServerSession::deleteInstance();
	ClientSessionHandler::deleteInstance();
	GameServerSessionHandler::deleteInstance();
	MapServerSessionHandler::deleteInstance();
	CenterSessionHandler::deleteInstance();
	SocialServerSessionHandler::deleteInstance();
	ClientListener::deleteInstance();
	GameServerSession::deleteInstance();
	ClientMgr::deleteInstance();
	MapServerMgr::deleteInstance();
	TransMgr::deleteInstance();
	GateServer::deleteInstance();
}

bool GateServerMaster::InitDBPool()
{
	return true;
}

bool GateServerMaster::LoadDBData()
{
	return true;
}

bool GateServerMaster::StartServices()
{
	if (!sClientListener.Start()) {
		ELOG("--- sClientListener.Start() failed.");
		return false;
	}

	if (!sGateServer.Start()) {
		ELOG("--- sGateServer.Start() failed.");
		return false;
	}

	return true;
}

void GateServerMaster::StopServices()
{
	sClientListener.Stop();
	sGateServer.Stop();
}

void GateServerMaster::Tick()
{
	sCenterSession.CheckConnection();
	sSocialServerSession.CheckConnection();
	sGameServerSession.CheckConnection();
}

std::string GateServerMaster::GetConfigFile()
{
	return "etc/GateServer.conf";
}

size_t GateServerMaster::GetDeferAsyncServiceCount()
{
	return GetConfig().GetInteger("OTHER", "DEFER_ASYNC_THREAD", 1);
}

size_t GateServerMaster::GetAsyncServiceCount()
{
	return GetConfig().GetInteger("OTHER", "ASYNC_THREAD", 1);
}

size_t GateServerMaster::GetIOServiceCount()
{
	return GetConfig().GetInteger("OTHER", "IO_THREAD", 1);
}
