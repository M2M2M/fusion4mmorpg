#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class GameServerSession;

class GameServerSessionHandler :
	public MessageHandler<GameServerSessionHandler, GameServerSession, GateServer2GameServer::GATE2GAME_MESSAGE_COUNT>,
	public Singleton<GameServerSessionHandler>
{
public:
	GameServerSessionHandler();
	virtual ~GameServerSessionHandler();
private:
	int HandleRegisterResp(GameServerSession *pSession, INetPacket &pck);
	int HandleAddMapServer(GameServerSession *pSession, INetPacket &pck);
	int HandleRemoveMapServer(GameServerSession *pSession, INetPacket &pck);
	int HandlePushServerId(GameServerSession *pSession, INetPacket &pck);
	int HandlePushSocialListen(GameServerSession *pSession, INetPacket &pck);
	int HandlePushPacketToAllClient(GameServerSession *pSession, INetPacket &pck);
	int HandleKickAccount(GameServerSession *pSession, INetPacket &pck);
	int HandleKillAccount(GameServerSession *pSession, INetPacket &pck);
	int HandlePushCharacter(GameServerSession *pSession, INetPacket &pck);
	int HandlePushPlayer(GameServerSession *pSession, INetPacket &pck);
};

#define sGameServerSessionHandler (*GameServerSessionHandler::instance())
