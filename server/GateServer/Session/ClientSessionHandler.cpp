#include "preHeader.h"
#include "ClientSessionHandler.h"
#include "protocol/OpCode.h"

ClientSessionHandler::ClientSessionHandler()
{
	handlers_[GAME_OPCODE::CMSG_PING] = &ClientSessionHandler::HandlePing;
	handlers_[GAME_OPCODE::CMSG_MMORPG_LOGIN] = &ClientSessionHandler::HandleMmorpgLogin;
	handlers_[GAME_OPCODE::CMSG_MMORPG_LOGOUT] = &ClientSessionHandler::HandleMmorpgLogout;
};

ClientSessionHandler::~ClientSessionHandler()
{
}
