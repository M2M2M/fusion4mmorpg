#pragma once

#include "Singleton.h"
#include "network/Session.h"

class SocialServerSessionHandler;

class SocialServerSession : public Session, public Singleton<SocialServerSession>
{
public:
	SocialServerSession();
	virtual ~SocialServerSession();

	void CheckConnection();

	void TransPacket(uint32 uid, const INetPacket& pck);

	void SetHostPort(const std::string& host, const std::string& port);
	void SetGsGateSN(uint32 gsGateSN);

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnConnected();
	virtual void OnShutdownSession();
	virtual void DeleteObject();

	bool CanHandle(uint32 opcode) const { return m_handlerFlags[opcode]; }

private:
	virtual void OnRecvPacket(INetPacket *pck);

	void TransClientPacket(INetPacket *pck) const;

	friend SocialServerSessionHandler;
	std::string m_host;
	std::string m_port;

	bool m_handlerFlags[GAME_OPCODE::CSMSG_COUNT];
};

#define sSocialServerSession (*SocialServerSession::instance())
