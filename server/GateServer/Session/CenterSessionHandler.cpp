#include "preHeader.h"
#include "CenterSessionHandler.h"
#include "protocol/CenterProtocol.h"

CenterSessionHandler::CenterSessionHandler()
{
	handlers_[CenterProtocol::SCT_REGISTER_RESP] = &CenterSessionHandler::HandleRegisterResp;
};

CenterSessionHandler::~CenterSessionHandler()
{
}
