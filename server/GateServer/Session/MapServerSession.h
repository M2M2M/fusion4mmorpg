#pragma once

#include "network/Session.h"

class MapServerSessionHandler;

class MapServerSession : public Session
{
public:
	MapServerSession(uint32 msSN, uint32 gsIdx,
		const std::string& host, const std::string& port);
	virtual ~MapServerSession();

	void CheckConnection();

	void TransPacket(ObjGUID guid, const INetPacket& pck);

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnConnected();
	virtual void OnManaged();
	virtual void OnShutdownSession();
	virtual void DeleteObject();

	void Cancel();
	void Reuse();

	uint32 GetMSSN() const { return m_msSN; }
	bool IsCancelled() const { return m_isCancelled; }
	static bool CanHandle(uint32 opcode) { return m_handlerFlags[opcode]; }

private:
	virtual void OnRecvPacket(INetPacket *pck);

	void TransClientPacket(INetPacket *pck) const;
	void TransMapServerPacket(INetPacket *pck) const;
	void TransSocialServerPacket(INetPacket *pck) const;

	friend MapServerSessionHandler;
	const uint32 m_msSN;
	const uint32 m_gsIdx;
	const std::string m_host;
	const std::string m_port;

	bool m_isCancelled;

	static bool m_handlerFlags[GAME_OPCODE::CSMSG_COUNT];
};
