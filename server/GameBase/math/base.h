#pragma once

#define PI (float(M_PI))
#define DEGTORAD(v) (f32((v)*M_PI*(1/180.f)))
#define RADTODEG(v) (f32((v)*M_1_PI*180.f))

constexpr float FLOAT_EPSINON = 1e-6f;
inline bool FLOAT_EQUALS_ZERO(float v) {
	return -FLOAT_EPSINON < v && v < FLOAT_EPSINON;
}
inline bool FLOAT_EQUALS(float v1, float v2) {
	return FLOAT_EQUALS_ZERO(v1 - v2);
}

inline float SQ(float v) {
	return v * v;
}
