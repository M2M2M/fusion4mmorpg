#pragma once

class CBitMask
{
public:
	CBitMask();
	~CBitMask();

	void Resize(size_t n);

	void Read(INetPacket& pck);
	void Write(INetPacket& pck) const;

	void SetDirty(size_t i);
	void ClearDirty(size_t i);
	void Reset();
	bool IsDirty(size_t i) const;
	bool IsDirty() const;

	size_t FindFirst() const;
	size_t FindNext(size_t i) const;

	size_t Size() const { return m_BitCount; }

	static const size_t npos = size_t(-1);

private:
	uint8* m_BitMask;
	size_t m_BitCount;
	size_t m_BlockCount;
};
