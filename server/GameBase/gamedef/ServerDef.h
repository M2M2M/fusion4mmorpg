#pragma once

#include <boost/functional/hash.hpp>

#define ONE_FRAME_MS (67)

enum SERVER_OBJECT_TYPE
{
	TYPE_GUILD = TYPE_MAX,
};

enum class SocialRPCReply
{
	GameServer,
	Guild,
	Rank,
	Operating,
};

struct GateServerGUID {
	uint32 serverId = 0;
	uint32 gateSN = 0;
	uint32 msSN = 0;
	bool operator==(const GateServerGUID& other) const {
		return serverId == other.serverId &&
			gateSN == other.gateSN &&
			msSN == other.msSN;
	}
};
namespace std {
	template <> struct hash<GateServerGUID> {
		std::size_t operator()(const GateServerGUID& arg) const {
			return boost::hash_range(
				(u32*)&arg, (u32*)((u8*)&arg + sizeof(arg)));
		}
	};
}

struct ClientAddr4Cross {
	GateServerGUID guid;
	uint32 sn;
};
inline INetStream& operator<<(INetStream& pck, const ClientAddr4Cross& arg) {
	return pck << arg.guid.serverId << arg.guid.gateSN << arg.guid.msSN << arg.sn;
}
inline INetStream& operator>>(INetStream& pck, ClientAddr4Cross& arg) {
	return pck >> arg.guid.serverId >> arg.guid.gateSN >> arg.guid.msSN >> arg.sn;
}

inline int64 FixActvtTime(int64 referTime, int64 actvtTime, time_t startServerDayTime) {
	return referTime == -1 ? actvtTime : actvtTime - referTime + startServerDayTime;
}
