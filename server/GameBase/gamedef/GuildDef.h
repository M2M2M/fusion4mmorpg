#pragma once

#define GUILD_INVITE_TIMEOUT (30)

struct GuildInvite
{
	uint32 inviterId;
	uint32 targetId;
	uint32 timer;
};

enum class GUILD_JOIN_CAUSE
{
	CREATE,
	INVITE,
	APPLY,
	RISE,
	SYNC,
	NET,
};

enum class GUILD_QUIT_CAUSE
{
	DISBAND,
	QUIT,
	KICK,
	NET,
};

enum class GUILD_AUTHORITY_TYPE
{
	NONE =				0,
	INVITE =			1 << 0,
	REPLY =				1 << 1,
	KICK =				1 << 2,
	RISE =				1 << 3,
	DISBAND =			1 << 4,
	LEVELUP =			1 << 5,
};

enum class GUILD_TITLE_AUTHORITY
{
	MASTER =			(int)GUILD_AUTHORITY_TYPE::INVITE |
						(int)GUILD_AUTHORITY_TYPE::REPLY |
						(int)GUILD_AUTHORITY_TYPE::KICK |
						(int)GUILD_AUTHORITY_TYPE::RISE |
						(int)GUILD_AUTHORITY_TYPE::DISBAND |
						(int)GUILD_AUTHORITY_TYPE::LEVELUP,
	SUBMASTER =			(int)GUILD_AUTHORITY_TYPE::INVITE |
						(int)GUILD_AUTHORITY_TYPE::REPLY |
						(int)GUILD_AUTHORITY_TYPE::KICK |
						(int)GUILD_AUTHORITY_TYPE::RISE |
						(int)GUILD_AUTHORITY_TYPE::LEVELUP,
	OFFICER =			(int)GUILD_AUTHORITY_TYPE::INVITE |
						(int)GUILD_AUTHORITY_TYPE::REPLY |
						(int)GUILD_AUTHORITY_TYPE::KICK,
	MEMBER =			(int)GUILD_AUTHORITY_TYPE::NONE,
};

extern const int GuildTitleAuthorities[];
