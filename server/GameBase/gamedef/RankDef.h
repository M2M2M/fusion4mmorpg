#pragma once

#define GET_RANK_COUNT_MAX (50)

struct RankData4Player
{
	uint32 playerId;
	uint32 lastLevel;
	int64 lastLevelTime;
	uint64 lastFightValue;
	int64 lastFightValueTime;
};

struct RankData4Guild
{
	uint32 guildId;
	uint32 lastLevel;
	int64 lastLevelTime;
	uint64 lastFightValue;
	int64 lastFightValueTime;
};

enum class RankDataFlag {
	UID = 1 << 0,
	TYPE = 1 << 1,
};
