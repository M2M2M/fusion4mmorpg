#pragma once

#define BUY_SHOP_ITEM_COUNT (999)

struct ShopItemStatus {
	uint32 dailyCount = 0;
	uint32 weeklyCount = 0;
	uint32 totalCount = 0;
	time_t expireTime = -1;
};

extern const ShopItemStatus defaultShopItemStatus;
bool IsDefaultShopItemStatus(const ShopItemStatus& siStatus);

bool IsShopPrototypeBuyLimit(const ShopPrototype* pProto);
bool IsSpecialShopPrototypeServerBuyLimit(const SpecialShopPrototype* pProto);
bool IsSpecialShopPrototypeCharBuyLimit(const SpecialShopPrototype* pProto);

uint32 GetBuyShopItemMaxCount4BuyLimit(
	const ShopPrototype* pProto, const ShopItemStatus& siStatus);
uint32 GetBuySpecialShopItemMaxCount4ServerBuyLimit(
	const SpecialShopPrototype* pProto, const ShopItemStatus& siStatus);
uint32 GetBuySpecialShopItemMaxCount4CharBuyLimit(
	const SpecialShopPrototype* pProto, const ShopItemStatus& siStatus);

void ApplyBuyShopItem4BuyLimit(
	const ShopPrototype* pProto, ShopItemStatus& siStatus, uint32 num);
void ApplyBuySpecialShopItem4ServerBuyLimit(
	const SpecialShopPrototype* pProto, ShopItemStatus& siStatus, uint32 num);
void ApplyBuySpecialShopItem4CharBuyLimit(
	const SpecialShopPrototype* pProto, ShopItemStatus& siStatus, uint32 num);

void RevertBuySpecialShopItem4ServerBuyLimit(
	const SpecialShopPrototype* pProto, ShopItemStatus& siStatus, uint32 num);
