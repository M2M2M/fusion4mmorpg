#include "preHeader.h"
#include "MailDef.h"

inst_mail NewSystemMailInstance(uint32 receiver,
	uint32 flags, const std::string& subject, const std::string& body)
{
	inst_mail mailProp;
	mailProp.mailType = (u32)MAIL_TYPE::SYSTEM;
	mailProp.mailFlags = flags;
	mailProp.mailReceiver = receiver;
	mailProp.mailDeliverTime = GET_UNIX_TIME;
	mailProp.mailExpireTime = GET_UNIX_TIME + MAIL_EXPIRE_TIME;
	mailProp.mailSubject = subject;
	mailProp.mailBody = body;
	return mailProp;
}

void AppendCheque2MailInstance(
	inst_mail& mailProp, uint8 chequeType, uint64 chequeValue)
{
	TextPacker packer;
	packer << chequeType << chequeValue;
	mailProp.mailCheques.push_back(packer.str());
}

void AppendCheque2MailInstance(
	inst_mail& mailProp, const std::string& chequeProp)
{
	mailProp.mailCheques.push_back(chequeProp);
}

void AppendItem2MailInstance(
	inst_mail& mailProp, uint32 itemTypeID, uint32 itemCount)
{
	mailProp.mailItems.push_back(
		inst_item_prop(itemTypeID, itemCount).Save());
}

void AppendItem2MailInstance(
	inst_mail& mailProp, const std::string& itemProp)
{
	mailProp.mailItems.push_back(itemProp);
}
