#pragma once

enum class OperatingEventType {
	Online,
	Offline,
	Count
};

enum class OperatingCareType {
	Login,
	Pay,
	Cost,
	GainScore,
	BeginPlayActvt,
	LastPlayActvt = BeginPlayActvt + (int)OperatingPlayType::Count - 1,
	BeginEventActvt,
	LastEventActvt = BeginEventActvt + (int)OperatingEventType::Count - 1,
	Count
};

struct OperatingPlayerInfo {
	uint32 playerId;
	uint32 vipLevel;
	uint32 level;
};

inline uint64 GetOperatingUniqueKey(uint32 nType, uint32 nTimes) {
	return ((uint64)nType << 32) + nTimes;
}
inline uint64 GetOperatingUniqueKey(const OperatingActivity& actProto) {
	return GetOperatingUniqueKey(actProto.nType, actProto.nTimes);
}

inline ChequeInfo NewChequeInfo(uint8 chequeType, uint64 chequeValue) {
	ChequeInfo chequeInfo;
	chequeInfo.type = chequeType;
	chequeInfo.value = chequeValue;
	return chequeInfo;
}
