table OperatingActivityData
{
	required uint32 playerId;
	required uint32 actType;
	required uint32 actTimes;
	required uint32[] saveData;
	required string userData;
	(tblname=inst_operating_activities)
}