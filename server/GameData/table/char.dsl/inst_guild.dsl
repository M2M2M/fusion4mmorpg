enum GUILD_TITLE
{
	INVALID = -1,
	MASTER = 0,
	SUBMASTER,
	OFFICER,
	MEMBER,
	COUNT,
}

table GuildApply
{
	required uint32 playerId;
	required uint32 guildId;
	required int64 applyTime;
	(tblname=guild_apply)
}

table GuildMember
{
	required uint32 playerId;
	required uint32 guildId;
	required int8 guildTitle;
	required int64 joinTime;
	(key=playerId,tblname=guild_member)
}

table GuildInformation
{
	required uint32 Id;
	required uint32 gsId;
	required string name;
	required int64 createTime;
	required uint32 level;
	required int64 levelTime;
	(key=Id,tblname=guild)
}