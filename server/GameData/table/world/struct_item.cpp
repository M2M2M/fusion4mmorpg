#include "jsontable/table_helper.h"
#include "struct_item.h"

ItemPrototype::Flags::Flags()
: canUse(false)
, canSell(false)
, canDestroy(false)
, isDestroyAfterUse(false)
{
}

ItemPrototype::ItemPrototype()
: itemTypeID(0)
, itemClass(0)
, itemSubClass(0)
, itemQuality(0)
, itemLevel(0)
, itemStack(0)
, itemSellPrice(0)
, itemLootId(0)
, itemSpellId(0)
, itemSpellLevel(0)
, itemScriptId(0)
{
}

ItemEquipPrototype::Attr::Attr()
: type(0)
, value(.0)
{
}

ItemEquipPrototype::Spell::Spell()
: id(0)
, level(0)
{
}

ItemEquipPrototype::ItemEquipPrototype()
: itemTypeID(0)
{
}
