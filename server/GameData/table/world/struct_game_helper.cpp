#include "jsontable/table_helper.h"
#include "struct_game.h"

template<> void LoadFromStream(GameTime::Type &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const GameTime::Type &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(GameTime::Type &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const GameTime::Type &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(GameTime::Type &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const GameTime::Type &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> const char *GetTableName<GameTime>()
{
	return "game_time";
}

template<> const char *GetTableKeyName<GameTime>()
{
	return "Id";
}

template<> uint64 GetTableKeyValue(const GameTime &entity)
{
	return (uint64)entity.Id;
}

template<> void SetTableKeyValue(GameTime &entity, uint64 key)
{
	entity.Id = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<GameTime>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "strTime";
		case 2: return "strDesc";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<GameTime>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "strTime") == 0) return 1;
	if (strcmp(name, "strDesc") == 0) return 2;
	return -1;
}

template<> size_t GetTableFieldNumber<GameTime>()
{
	return 3;
}

template<> std::string GetTableFieldValue(const GameTime &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.strTime);
		case 2: return StringHelper::ToString(entity.strDesc);
	}
	return "";
}

template<> void SetTableFieldValue(GameTime &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.strTime, value);
		case 2: return StringHelper::FromString(entity.strDesc, value);
	}
}

template<> void LoadFromStream(GameTime &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.strTime, stream);
	StreamHelper::FromStream(entity.strDesc, stream);
}

template<> void SaveToStream(const GameTime &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.strTime, stream);
	StreamHelper::ToStream(entity.strDesc, stream);
}

template<> void LoadFromText(GameTime &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const GameTime &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(GameTime &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.strTime, value, "strTime");
	FromJson(entity.strDesc, value, "strDesc");
}

template<> void JsonHelper::BlockToJson(const GameTime &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.strTime, value, "strTime");
	ToJson(entity.strDesc, value, "strDesc");
}
