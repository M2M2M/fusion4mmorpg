#pragma once

#include "struct_base.h"

struct PayShop
{
	PayShop();

	uint32 Id;
	std::string strName;
	uint32 platformId;
	std::string strChannels;
	uint32 itemType;
	uint32 buyPrice;
	uint32 buyGoldValue;
	std::vector<ChequeInfo> buyCheques;
	std::vector<FItemInfo> buyItems;
	uint32 buyShowItemId;
	std::string strRemark;
};
