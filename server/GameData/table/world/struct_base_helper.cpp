#include "jsontable/table_helper.h"
#include "struct_base.h"

template<> void LoadFromStream(PlayerCareer &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const PlayerCareer &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(PlayerCareer &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const PlayerCareer &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(PlayerCareer &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const PlayerCareer &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(PlayerGender &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const PlayerGender &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(PlayerGender &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const PlayerGender &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(PlayerGender &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const PlayerGender &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(CurrencyType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const CurrencyType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(CurrencyType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CurrencyType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CurrencyType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const CurrencyType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(ChequeType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const ChequeType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(ChequeType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const ChequeType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(ChequeType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const ChequeType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(ItemFlag &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const ItemFlag &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(ItemFlag &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const ItemFlag &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(ItemFlag &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const ItemFlag &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(ChequeInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.type, stream);
	StreamHelper::FromStream(entity.value, stream);
}

template<> void SaveToStream(const ChequeInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.type, stream);
	StreamHelper::ToStream(entity.value, stream);
}

template<> void LoadFromText(ChequeInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const ChequeInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(ChequeInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.type, value, "type");
	FromJson(entity.value, value, "value");
}

template<> void JsonHelper::BlockToJson(const ChequeInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.type, value, "type");
	ToJson(entity.value, value, "value");
}

template<> void LoadFromStream(ItemInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.id, stream);
	StreamHelper::FromStream(entity.num, stream);
}

template<> void SaveToStream(const ItemInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.id, stream);
	StreamHelper::ToStream(entity.num, stream);
}

template<> void LoadFromText(ItemInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const ItemInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(ItemInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.id, value, "id");
	FromJson(entity.num, value, "num");
}

template<> void JsonHelper::BlockToJson(const ItemInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.id, value, "id");
	ToJson(entity.num, value, "num");
}

template<> void LoadFromStream(FItemInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.id, stream);
	StreamHelper::FromStream(entity.num, stream);
	StreamHelper::FromStream(entity.flags, stream);
}

template<> void SaveToStream(const FItemInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.id, stream);
	StreamHelper::ToStream(entity.num, stream);
	StreamHelper::ToStream(entity.flags, stream);
}

template<> void LoadFromText(FItemInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const FItemInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(FItemInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.id, value, "id");
	FromJson(entity.num, value, "num");
	FromJson(entity.flags, value, "flags");
}

template<> void JsonHelper::BlockToJson(const FItemInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.id, value, "id");
	ToJson(entity.num, value, "num");
	ToJson(entity.flags, value, "flags");
}

template<> const char *GetTableName<Configure>()
{
	return "configure";
}

template<> const char *GetTableKeyName<Configure>()
{
	return "";
}

template<> uint64 GetTableKeyValue(const Configure &entity)
{
	return 0;
}

template<> void SetTableKeyValue(Configure &entity, uint64 key)
{
}

template<> const char *GetTableFieldNameByIndex<Configure>(size_t index)
{
	switch (index)
	{
		case 0: return "cfgIndex";
		case 1: return "cfgName";
		case 2: return "cfgVal";
		case 3: return "cfgDesc";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<Configure>(const char *name)
{
	if (strcmp(name, "cfgIndex") == 0) return 0;
	if (strcmp(name, "cfgName") == 0) return 1;
	if (strcmp(name, "cfgVal") == 0) return 2;
	if (strcmp(name, "cfgDesc") == 0) return 3;
	return -1;
}

template<> size_t GetTableFieldNumber<Configure>()
{
	return 4;
}

template<> std::string GetTableFieldValue(const Configure &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.cfgIndex);
		case 1: return StringHelper::ToString(entity.cfgName);
		case 2: return StringHelper::ToString(entity.cfgVal);
		case 3: return StringHelper::ToString(entity.cfgDesc);
	}
	return "";
}

template<> void SetTableFieldValue(Configure &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.cfgIndex, value);
		case 1: return StringHelper::FromString(entity.cfgName, value);
		case 2: return StringHelper::FromString(entity.cfgVal, value);
		case 3: return StringHelper::FromString(entity.cfgDesc, value);
	}
}

template<> void LoadFromStream(Configure &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.cfgIndex, stream);
	StreamHelper::FromStream(entity.cfgName, stream);
	StreamHelper::FromStream(entity.cfgVal, stream);
	StreamHelper::FromStream(entity.cfgDesc, stream);
}

template<> void SaveToStream(const Configure &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.cfgIndex, stream);
	StreamHelper::ToStream(entity.cfgName, stream);
	StreamHelper::ToStream(entity.cfgVal, stream);
	StreamHelper::ToStream(entity.cfgDesc, stream);
}

template<> void LoadFromText(Configure &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const Configure &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(Configure &entity, const rapidjson::Value &value)
{
	FromJson(entity.cfgIndex, value, "cfgIndex");
	FromJson(entity.cfgName, value, "cfgName");
	FromJson(entity.cfgVal, value, "cfgVal");
	FromJson(entity.cfgDesc, value, "cfgDesc");
}

template<> void JsonHelper::BlockToJson(const Configure &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.cfgIndex, value, "cfgIndex");
	ToJson(entity.cfgName, value, "cfgName");
	ToJson(entity.cfgVal, value, "cfgVal");
	ToJson(entity.cfgDesc, value, "cfgDesc");
}
