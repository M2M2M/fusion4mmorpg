#include "jsontable/table_helper.h"
#include "struct_spell.h"

template<> void LoadFromStream(SpellStageType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellStageType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellStageType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellStageType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellStageType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellStageType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellResumeType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellResumeType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellResumeType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellResumeType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellResumeType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellResumeType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellEffectStyle &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellEffectStyle &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellEffectStyle &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellEffectStyle &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellEffectStyle &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellEffectStyle &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellInterruptBy &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellInterruptBy &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellInterruptBy &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellInterruptBy &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellInterruptBy &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellInterruptBy &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellEffectType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellEffectType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellEffectType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellEffectType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellEffectType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellEffectType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellTargetType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellTargetType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellTargetType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellTargetType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellTargetType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellTargetType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellSelectType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellSelectType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellSelectType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellSelectType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellSelectType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellSelectType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellSelectMode &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellSelectMode &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellSelectMode &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellSelectMode &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellSelectMode &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellSelectMode &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellPassiveMode &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellPassiveMode &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellPassiveMode &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellPassiveMode &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellPassiveMode &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellPassiveMode &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellPassiveBy &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const SpellPassiveBy &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(SpellPassiveBy &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellPassiveBy &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellPassiveBy &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const SpellPassiveBy &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(SpellInfo::Flags &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.isExclusive, stream);
	StreamHelper::FromStream(entity.isPassive, stream);
	StreamHelper::FromStream(entity.isAppearance, stream);
	StreamHelper::FromStream(entity.isSync2Client, stream);
	StreamHelper::FromStream(entity.isSync2AllClient, stream);
	StreamHelper::FromStream(entity.isTargetDeadable, stream);
	StreamHelper::FromStream(entity.isCheckCastDist, stream);
	StreamHelper::FromStream(entity.isIgnoreOutOfControl, stream);
}

template<> void SaveToStream(const SpellInfo::Flags &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.isExclusive, stream);
	StreamHelper::ToStream(entity.isPassive, stream);
	StreamHelper::ToStream(entity.isAppearance, stream);
	StreamHelper::ToStream(entity.isSync2Client, stream);
	StreamHelper::ToStream(entity.isSync2AllClient, stream);
	StreamHelper::ToStream(entity.isTargetDeadable, stream);
	StreamHelper::ToStream(entity.isCheckCastDist, stream);
	StreamHelper::ToStream(entity.isIgnoreOutOfControl, stream);
}

template<> void LoadFromText(SpellInfo::Flags &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellInfo::Flags &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellInfo::Flags &entity, const rapidjson::Value &value)
{
	FromJson(entity.isExclusive, value, "isExclusive");
	FromJson(entity.isPassive, value, "isPassive");
	FromJson(entity.isAppearance, value, "isAppearance");
	FromJson(entity.isSync2Client, value, "isSync2Client");
	FromJson(entity.isSync2AllClient, value, "isSync2AllClient");
	FromJson(entity.isTargetDeadable, value, "isTargetDeadable");
	FromJson(entity.isCheckCastDist, value, "isCheckCastDist");
	FromJson(entity.isIgnoreOutOfControl, value, "isIgnoreOutOfControl");
}

template<> void JsonHelper::BlockToJson(const SpellInfo::Flags &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.isExclusive, value, "isExclusive");
	ToJson(entity.isPassive, value, "isPassive");
	ToJson(entity.isAppearance, value, "isAppearance");
	ToJson(entity.isSync2Client, value, "isSync2Client");
	ToJson(entity.isSync2AllClient, value, "isSync2AllClient");
	ToJson(entity.isTargetDeadable, value, "isTargetDeadable");
	ToJson(entity.isCheckCastDist, value, "isCheckCastDist");
	ToJson(entity.isIgnoreOutOfControl, value, "isIgnoreOutOfControl");
}

template<> const char *GetTableName<SpellInfo>()
{
	return "spell_info";
}

template<> const char *GetTableKeyName<SpellInfo>()
{
	return "spellID";
}

template<> uint64 GetTableKeyValue(const SpellInfo &entity)
{
	return (uint64)entity.spellID;
}

template<> void SetTableKeyValue(SpellInfo &entity, uint64 key)
{
	entity.spellID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<SpellInfo>(size_t index)
{
	switch (index)
	{
		case 0: return "spellID";
		case 1: return "spellIcon";
		case 2: return "spellFlags";
		case 3: return "spellBuffType";
		case 4: return "spellCooldownType";
		case 5: return "spellTargetType";
		case 6: return "spellPassiveMode";
		case 7: return "spellPassiveBy";
		case 8: return "spellPassiveArgs";
		case 9: return "spellPassiveChance";
		case 10: return "spellLimitCareer";
		case 11: return "spellLimitMapType";
		case 12: return "spellLimitMapID";
		case 13: return "spellLimitScriptID";
		case 14: return "spellLimitScriptArgs";
		case 15: return "spellInterruptBys";
		case 16: return "spellStageTime";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<SpellInfo>(const char *name)
{
	if (strcmp(name, "spellID") == 0) return 0;
	if (strcmp(name, "spellIcon") == 0) return 1;
	if (strcmp(name, "spellFlags") == 0) return 2;
	if (strcmp(name, "spellBuffType") == 0) return 3;
	if (strcmp(name, "spellCooldownType") == 0) return 4;
	if (strcmp(name, "spellTargetType") == 0) return 5;
	if (strcmp(name, "spellPassiveMode") == 0) return 6;
	if (strcmp(name, "spellPassiveBy") == 0) return 7;
	if (strcmp(name, "spellPassiveArgs") == 0) return 8;
	if (strcmp(name, "spellPassiveChance") == 0) return 9;
	if (strcmp(name, "spellLimitCareer") == 0) return 10;
	if (strcmp(name, "spellLimitMapType") == 0) return 11;
	if (strcmp(name, "spellLimitMapID") == 0) return 12;
	if (strcmp(name, "spellLimitScriptID") == 0) return 13;
	if (strcmp(name, "spellLimitScriptArgs") == 0) return 14;
	if (strcmp(name, "spellInterruptBys") == 0) return 15;
	if (strcmp(name, "spellStageTime") == 0) return 16;
	return -1;
}

template<> size_t GetTableFieldNumber<SpellInfo>()
{
	return 17;
}

template<> std::string GetTableFieldValue(const SpellInfo &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.spellID);
		case 1: return StringHelper::ToString(entity.spellIcon);
		case 2: return JsonHelper::BlockToJsonText(entity.spellFlags);
		case 3: return StringHelper::ToString(entity.spellBuffType);
		case 4: return StringHelper::ToString(entity.spellCooldownType);
		case 5: return StringHelper::ToString(entity.spellTargetType);
		case 6: return StringHelper::ToString(entity.spellPassiveMode);
		case 7: return StringHelper::ToString(entity.spellPassiveBy);
		case 8: return JsonHelper::SequenceToJsonText(entity.spellPassiveArgs);
		case 9: return StringHelper::ToString(entity.spellPassiveChance);
		case 10: return StringHelper::ToString(entity.spellLimitCareer);
		case 11: return StringHelper::ToString(entity.spellLimitMapType);
		case 12: return StringHelper::ToString(entity.spellLimitMapID);
		case 13: return StringHelper::ToString(entity.spellLimitScriptID);
		case 14: return StringHelper::ToString(entity.spellLimitScriptArgs);
		case 15: return StringHelper::ToString(entity.spellInterruptBys);
		case 16: return JsonHelper::SequenceToJsonText(entity.spellStageTime);
	}
	return "";
}

template<> void SetTableFieldValue(SpellInfo &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.spellID, value);
		case 1: return StringHelper::FromString(entity.spellIcon, value);
		case 2: return JsonHelper::BlockFromJsonText(entity.spellFlags, value);
		case 3: return StringHelper::FromString(entity.spellBuffType, value);
		case 4: return StringHelper::FromString(entity.spellCooldownType, value);
		case 5: return StringHelper::FromString(entity.spellTargetType, value);
		case 6: return StringHelper::FromString(entity.spellPassiveMode, value);
		case 7: return StringHelper::FromString(entity.spellPassiveBy, value);
		case 8: return JsonHelper::SequenceFromJsonText(entity.spellPassiveArgs, value);
		case 9: return StringHelper::FromString(entity.spellPassiveChance, value);
		case 10: return StringHelper::FromString(entity.spellLimitCareer, value);
		case 11: return StringHelper::FromString(entity.spellLimitMapType, value);
		case 12: return StringHelper::FromString(entity.spellLimitMapID, value);
		case 13: return StringHelper::FromString(entity.spellLimitScriptID, value);
		case 14: return StringHelper::FromString(entity.spellLimitScriptArgs, value);
		case 15: return StringHelper::FromString(entity.spellInterruptBys, value);
		case 16: return JsonHelper::SequenceFromJsonText(entity.spellStageTime, value);
	}
}

template<> void LoadFromStream(SpellInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.spellID, stream);
	StreamHelper::FromStream(entity.spellIcon, stream);
	LoadFromStream(entity.spellFlags, stream);
	StreamHelper::FromStream(entity.spellBuffType, stream);
	StreamHelper::FromStream(entity.spellCooldownType, stream);
	StreamHelper::FromStream(entity.spellTargetType, stream);
	StreamHelper::FromStream(entity.spellPassiveMode, stream);
	StreamHelper::FromStream(entity.spellPassiveBy, stream);
	StreamHelper::SequenceFromStream(entity.spellPassiveArgs, stream);
	StreamHelper::FromStream(entity.spellPassiveChance, stream);
	StreamHelper::FromStream(entity.spellLimitCareer, stream);
	StreamHelper::FromStream(entity.spellLimitMapType, stream);
	StreamHelper::FromStream(entity.spellLimitMapID, stream);
	StreamHelper::FromStream(entity.spellLimitScriptID, stream);
	StreamHelper::FromStream(entity.spellLimitScriptArgs, stream);
	StreamHelper::FromStream(entity.spellInterruptBys, stream);
	StreamHelper::SequenceFromStream(entity.spellStageTime, stream);
}

template<> void SaveToStream(const SpellInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.spellID, stream);
	StreamHelper::ToStream(entity.spellIcon, stream);
	SaveToStream(entity.spellFlags, stream);
	StreamHelper::ToStream(entity.spellBuffType, stream);
	StreamHelper::ToStream(entity.spellCooldownType, stream);
	StreamHelper::ToStream(entity.spellTargetType, stream);
	StreamHelper::ToStream(entity.spellPassiveMode, stream);
	StreamHelper::ToStream(entity.spellPassiveBy, stream);
	StreamHelper::SequenceToStream(entity.spellPassiveArgs, stream);
	StreamHelper::ToStream(entity.spellPassiveChance, stream);
	StreamHelper::ToStream(entity.spellLimitCareer, stream);
	StreamHelper::ToStream(entity.spellLimitMapType, stream);
	StreamHelper::ToStream(entity.spellLimitMapID, stream);
	StreamHelper::ToStream(entity.spellLimitScriptID, stream);
	StreamHelper::ToStream(entity.spellLimitScriptArgs, stream);
	StreamHelper::ToStream(entity.spellInterruptBys, stream);
	StreamHelper::SequenceToStream(entity.spellStageTime, stream);
}

template<> void LoadFromText(SpellInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.spellID, value, "spellID");
	FromJson(entity.spellIcon, value, "spellIcon");
	BlockFromJson(entity.spellFlags, value, "spellFlags");
	FromJson(entity.spellBuffType, value, "spellBuffType");
	FromJson(entity.spellCooldownType, value, "spellCooldownType");
	FromJson(entity.spellTargetType, value, "spellTargetType");
	FromJson(entity.spellPassiveMode, value, "spellPassiveMode");
	FromJson(entity.spellPassiveBy, value, "spellPassiveBy");
	SequenceFromJson(entity.spellPassiveArgs, value, "spellPassiveArgs");
	FromJson(entity.spellPassiveChance, value, "spellPassiveChance");
	FromJson(entity.spellLimitCareer, value, "spellLimitCareer");
	FromJson(entity.spellLimitMapType, value, "spellLimitMapType");
	FromJson(entity.spellLimitMapID, value, "spellLimitMapID");
	FromJson(entity.spellLimitScriptID, value, "spellLimitScriptID");
	FromJson(entity.spellLimitScriptArgs, value, "spellLimitScriptArgs");
	FromJson(entity.spellInterruptBys, value, "spellInterruptBys");
	SequenceFromJson(entity.spellStageTime, value, "spellStageTime");
}

template<> void JsonHelper::BlockToJson(const SpellInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.spellID, value, "spellID");
	ToJson(entity.spellIcon, value, "spellIcon");
	BlockToJson(entity.spellFlags, value, "spellFlags");
	ToJson(entity.spellBuffType, value, "spellBuffType");
	ToJson(entity.spellCooldownType, value, "spellCooldownType");
	ToJson(entity.spellTargetType, value, "spellTargetType");
	ToJson(entity.spellPassiveMode, value, "spellPassiveMode");
	ToJson(entity.spellPassiveBy, value, "spellPassiveBy");
	SequenceToJson(entity.spellPassiveArgs, value, "spellPassiveArgs");
	ToJson(entity.spellPassiveChance, value, "spellPassiveChance");
	ToJson(entity.spellLimitCareer, value, "spellLimitCareer");
	ToJson(entity.spellLimitMapType, value, "spellLimitMapType");
	ToJson(entity.spellLimitMapID, value, "spellLimitMapID");
	ToJson(entity.spellLimitScriptID, value, "spellLimitScriptID");
	ToJson(entity.spellLimitScriptArgs, value, "spellLimitScriptArgs");
	ToJson(entity.spellInterruptBys, value, "spellInterruptBys");
	SequenceToJson(entity.spellStageTime, value, "spellStageTime");
}

template<> const char *GetTableName<SpellLevelInfo>()
{
	return "spell_level_info";
}

template<> const char *GetTableKeyName<SpellLevelInfo>()
{
	return "spellLevelID";
}

template<> uint64 GetTableKeyValue(const SpellLevelInfo &entity)
{
	return (uint64)entity.spellLevelID;
}

template<> void SetTableKeyValue(SpellLevelInfo &entity, uint64 key)
{
	entity.spellLevelID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<SpellLevelInfo>(size_t index)
{
	switch (index)
	{
		case 0: return "spellLevelID";
		case 1: return "spellID";
		case 2: return "spellLimitLevel";
		case 3: return "spellCastDistMin";
		case 4: return "spellCastDistMax";
		case 5: return "spellEvalScore";
		case 6: return "spellCostHP";
		case 7: return "spellCostHPRate";
		case 8: return "spellCostMP";
		case 9: return "spellCostMPRate";
		case 10: return "spellCDTime";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<SpellLevelInfo>(const char *name)
{
	if (strcmp(name, "spellLevelID") == 0) return 0;
	if (strcmp(name, "spellID") == 0) return 1;
	if (strcmp(name, "spellLimitLevel") == 0) return 2;
	if (strcmp(name, "spellCastDistMin") == 0) return 3;
	if (strcmp(name, "spellCastDistMax") == 0) return 4;
	if (strcmp(name, "spellEvalScore") == 0) return 5;
	if (strcmp(name, "spellCostHP") == 0) return 6;
	if (strcmp(name, "spellCostHPRate") == 0) return 7;
	if (strcmp(name, "spellCostMP") == 0) return 8;
	if (strcmp(name, "spellCostMPRate") == 0) return 9;
	if (strcmp(name, "spellCDTime") == 0) return 10;
	return -1;
}

template<> size_t GetTableFieldNumber<SpellLevelInfo>()
{
	return 11;
}

template<> std::string GetTableFieldValue(const SpellLevelInfo &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.spellLevelID);
		case 1: return StringHelper::ToString(entity.spellID);
		case 2: return StringHelper::ToString(entity.spellLimitLevel);
		case 3: return StringHelper::ToString(entity.spellCastDistMin);
		case 4: return StringHelper::ToString(entity.spellCastDistMax);
		case 5: return StringHelper::ToString(entity.spellEvalScore);
		case 6: return StringHelper::ToString(entity.spellCostHP);
		case 7: return StringHelper::ToString(entity.spellCostHPRate);
		case 8: return StringHelper::ToString(entity.spellCostMP);
		case 9: return StringHelper::ToString(entity.spellCostMPRate);
		case 10: return StringHelper::ToString(entity.spellCDTime);
	}
	return "";
}

template<> void SetTableFieldValue(SpellLevelInfo &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.spellLevelID, value);
		case 1: return StringHelper::FromString(entity.spellID, value);
		case 2: return StringHelper::FromString(entity.spellLimitLevel, value);
		case 3: return StringHelper::FromString(entity.spellCastDistMin, value);
		case 4: return StringHelper::FromString(entity.spellCastDistMax, value);
		case 5: return StringHelper::FromString(entity.spellEvalScore, value);
		case 6: return StringHelper::FromString(entity.spellCostHP, value);
		case 7: return StringHelper::FromString(entity.spellCostHPRate, value);
		case 8: return StringHelper::FromString(entity.spellCostMP, value);
		case 9: return StringHelper::FromString(entity.spellCostMPRate, value);
		case 10: return StringHelper::FromString(entity.spellCDTime, value);
	}
}

template<> void LoadFromStream(SpellLevelInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.spellLevelID, stream);
	StreamHelper::FromStream(entity.spellID, stream);
	StreamHelper::FromStream(entity.spellLimitLevel, stream);
	StreamHelper::FromStream(entity.spellCastDistMin, stream);
	StreamHelper::FromStream(entity.spellCastDistMax, stream);
	StreamHelper::FromStream(entity.spellEvalScore, stream);
	StreamHelper::FromStream(entity.spellCostHP, stream);
	StreamHelper::FromStream(entity.spellCostHPRate, stream);
	StreamHelper::FromStream(entity.spellCostMP, stream);
	StreamHelper::FromStream(entity.spellCostMPRate, stream);
	StreamHelper::FromStream(entity.spellCDTime, stream);
}

template<> void SaveToStream(const SpellLevelInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.spellLevelID, stream);
	StreamHelper::ToStream(entity.spellID, stream);
	StreamHelper::ToStream(entity.spellLimitLevel, stream);
	StreamHelper::ToStream(entity.spellCastDistMin, stream);
	StreamHelper::ToStream(entity.spellCastDistMax, stream);
	StreamHelper::ToStream(entity.spellEvalScore, stream);
	StreamHelper::ToStream(entity.spellCostHP, stream);
	StreamHelper::ToStream(entity.spellCostHPRate, stream);
	StreamHelper::ToStream(entity.spellCostMP, stream);
	StreamHelper::ToStream(entity.spellCostMPRate, stream);
	StreamHelper::ToStream(entity.spellCDTime, stream);
}

template<> void LoadFromText(SpellLevelInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellLevelInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellLevelInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.spellLevelID, value, "spellLevelID");
	FromJson(entity.spellID, value, "spellID");
	FromJson(entity.spellLimitLevel, value, "spellLimitLevel");
	FromJson(entity.spellCastDistMin, value, "spellCastDistMin");
	FromJson(entity.spellCastDistMax, value, "spellCastDistMax");
	FromJson(entity.spellEvalScore, value, "spellEvalScore");
	FromJson(entity.spellCostHP, value, "spellCostHP");
	FromJson(entity.spellCostHPRate, value, "spellCostHPRate");
	FromJson(entity.spellCostMP, value, "spellCostMP");
	FromJson(entity.spellCostMPRate, value, "spellCostMPRate");
	FromJson(entity.spellCDTime, value, "spellCDTime");
}

template<> void JsonHelper::BlockToJson(const SpellLevelInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.spellLevelID, value, "spellLevelID");
	ToJson(entity.spellID, value, "spellID");
	ToJson(entity.spellLimitLevel, value, "spellLimitLevel");
	ToJson(entity.spellCastDistMin, value, "spellCastDistMin");
	ToJson(entity.spellCastDistMax, value, "spellCastDistMax");
	ToJson(entity.spellEvalScore, value, "spellEvalScore");
	ToJson(entity.spellCostHP, value, "spellCostHP");
	ToJson(entity.spellCostHPRate, value, "spellCostHPRate");
	ToJson(entity.spellCostMP, value, "spellCostMP");
	ToJson(entity.spellCostMPRate, value, "spellCostMPRate");
	ToJson(entity.spellCDTime, value, "spellCDTime");
}

template<> void LoadFromStream(SpellLevelEffectInfo::Flags &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.isResumable, stream);
	StreamHelper::FromStream(entity.isDeadResumable, stream);
	StreamHelper::FromStream(entity.isSync2Client, stream);
	StreamHelper::FromStream(entity.isSync2AllClient, stream);
	StreamHelper::FromStream(entity.isTargetDeadable, stream);
	StreamHelper::FromStream(entity.isClientSelectTarget, stream);
}

template<> void SaveToStream(const SpellLevelEffectInfo::Flags &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.isResumable, stream);
	StreamHelper::ToStream(entity.isDeadResumable, stream);
	StreamHelper::ToStream(entity.isSync2Client, stream);
	StreamHelper::ToStream(entity.isSync2AllClient, stream);
	StreamHelper::ToStream(entity.isTargetDeadable, stream);
	StreamHelper::ToStream(entity.isClientSelectTarget, stream);
}

template<> void LoadFromText(SpellLevelEffectInfo::Flags &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellLevelEffectInfo::Flags &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellLevelEffectInfo::Flags &entity, const rapidjson::Value &value)
{
	FromJson(entity.isResumable, value, "isResumable");
	FromJson(entity.isDeadResumable, value, "isDeadResumable");
	FromJson(entity.isSync2Client, value, "isSync2Client");
	FromJson(entity.isSync2AllClient, value, "isSync2AllClient");
	FromJson(entity.isTargetDeadable, value, "isTargetDeadable");
	FromJson(entity.isClientSelectTarget, value, "isClientSelectTarget");
}

template<> void JsonHelper::BlockToJson(const SpellLevelEffectInfo::Flags &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.isResumable, value, "isResumable");
	ToJson(entity.isDeadResumable, value, "isDeadResumable");
	ToJson(entity.isSync2Client, value, "isSync2Client");
	ToJson(entity.isSync2AllClient, value, "isSync2AllClient");
	ToJson(entity.isTargetDeadable, value, "isTargetDeadable");
	ToJson(entity.isClientSelectTarget, value, "isClientSelectTarget");
}

template<> const char *GetTableName<SpellLevelEffectInfo>()
{
	return "spell_level_effect_info";
}

template<> const char *GetTableKeyName<SpellLevelEffectInfo>()
{
	return "spellLevelEffectID";
}

template<> uint64 GetTableKeyValue(const SpellLevelEffectInfo &entity)
{
	return (uint64)entity.spellLevelEffectID;
}

template<> void SetTableKeyValue(SpellLevelEffectInfo &entity, uint64 key)
{
	entity.spellLevelEffectID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<SpellLevelEffectInfo>(size_t index)
{
	switch (index)
	{
		case 0: return "spellLevelEffectID";
		case 1: return "spellID";
		case 2: return "spellLevelID";
		case 3: return "spellEffectFlags";
		case 4: return "spellStageTrigger";
		case 5: return "spellDelayTrigger";
		case 6: return "spellSelectType";
		case 7: return "spellSelectMode";
		case 8: return "spellSelectArgs";
		case 9: return "spellSelectNumber";
		case 10: return "spellDelayEffective";
		case 11: return "spellEffectiveChance";
		case 12: return "spellEffectStyle";
		case 13: return "spellEffectType";
		case 14: return "spellEffectArgs";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<SpellLevelEffectInfo>(const char *name)
{
	if (strcmp(name, "spellLevelEffectID") == 0) return 0;
	if (strcmp(name, "spellID") == 0) return 1;
	if (strcmp(name, "spellLevelID") == 0) return 2;
	if (strcmp(name, "spellEffectFlags") == 0) return 3;
	if (strcmp(name, "spellStageTrigger") == 0) return 4;
	if (strcmp(name, "spellDelayTrigger") == 0) return 5;
	if (strcmp(name, "spellSelectType") == 0) return 6;
	if (strcmp(name, "spellSelectMode") == 0) return 7;
	if (strcmp(name, "spellSelectArgs") == 0) return 8;
	if (strcmp(name, "spellSelectNumber") == 0) return 9;
	if (strcmp(name, "spellDelayEffective") == 0) return 10;
	if (strcmp(name, "spellEffectiveChance") == 0) return 11;
	if (strcmp(name, "spellEffectStyle") == 0) return 12;
	if (strcmp(name, "spellEffectType") == 0) return 13;
	if (strcmp(name, "spellEffectArgs") == 0) return 14;
	return -1;
}

template<> size_t GetTableFieldNumber<SpellLevelEffectInfo>()
{
	return 15;
}

template<> std::string GetTableFieldValue(const SpellLevelEffectInfo &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.spellLevelEffectID);
		case 1: return StringHelper::ToString(entity.spellID);
		case 2: return StringHelper::ToString(entity.spellLevelID);
		case 3: return JsonHelper::BlockToJsonText(entity.spellEffectFlags);
		case 4: return StringHelper::ToString(entity.spellStageTrigger);
		case 5: return StringHelper::ToString(entity.spellDelayTrigger);
		case 6: return StringHelper::ToString(entity.spellSelectType);
		case 7: return StringHelper::ToString(entity.spellSelectMode);
		case 8: return JsonHelper::SequenceToJsonText(entity.spellSelectArgs);
		case 9: return StringHelper::ToString(entity.spellSelectNumber);
		case 10: return StringHelper::ToString(entity.spellDelayEffective);
		case 11: return StringHelper::ToString(entity.spellEffectiveChance);
		case 12: return StringHelper::ToString(entity.spellEffectStyle);
		case 13: return StringHelper::ToString(entity.spellEffectType);
		case 14: return StringHelper::ToString(entity.spellEffectArgs);
	}
	return "";
}

template<> void SetTableFieldValue(SpellLevelEffectInfo &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.spellLevelEffectID, value);
		case 1: return StringHelper::FromString(entity.spellID, value);
		case 2: return StringHelper::FromString(entity.spellLevelID, value);
		case 3: return JsonHelper::BlockFromJsonText(entity.spellEffectFlags, value);
		case 4: return StringHelper::FromString(entity.spellStageTrigger, value);
		case 5: return StringHelper::FromString(entity.spellDelayTrigger, value);
		case 6: return StringHelper::FromString(entity.spellSelectType, value);
		case 7: return StringHelper::FromString(entity.spellSelectMode, value);
		case 8: return JsonHelper::SequenceFromJsonText(entity.spellSelectArgs, value);
		case 9: return StringHelper::FromString(entity.spellSelectNumber, value);
		case 10: return StringHelper::FromString(entity.spellDelayEffective, value);
		case 11: return StringHelper::FromString(entity.spellEffectiveChance, value);
		case 12: return StringHelper::FromString(entity.spellEffectStyle, value);
		case 13: return StringHelper::FromString(entity.spellEffectType, value);
		case 14: return StringHelper::FromString(entity.spellEffectArgs, value);
	}
}

template<> void LoadFromStream(SpellLevelEffectInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.spellLevelEffectID, stream);
	StreamHelper::FromStream(entity.spellID, stream);
	StreamHelper::FromStream(entity.spellLevelID, stream);
	LoadFromStream(entity.spellEffectFlags, stream);
	StreamHelper::FromStream(entity.spellStageTrigger, stream);
	StreamHelper::FromStream(entity.spellDelayTrigger, stream);
	StreamHelper::FromStream(entity.spellSelectType, stream);
	StreamHelper::FromStream(entity.spellSelectMode, stream);
	StreamHelper::SequenceFromStream(entity.spellSelectArgs, stream);
	StreamHelper::FromStream(entity.spellSelectNumber, stream);
	StreamHelper::FromStream(entity.spellDelayEffective, stream);
	StreamHelper::FromStream(entity.spellEffectiveChance, stream);
	StreamHelper::FromStream(entity.spellEffectStyle, stream);
	StreamHelper::FromStream(entity.spellEffectType, stream);
	StreamHelper::FromStream(entity.spellEffectArgs, stream);
}

template<> void SaveToStream(const SpellLevelEffectInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.spellLevelEffectID, stream);
	StreamHelper::ToStream(entity.spellID, stream);
	StreamHelper::ToStream(entity.spellLevelID, stream);
	SaveToStream(entity.spellEffectFlags, stream);
	StreamHelper::ToStream(entity.spellStageTrigger, stream);
	StreamHelper::ToStream(entity.spellDelayTrigger, stream);
	StreamHelper::ToStream(entity.spellSelectType, stream);
	StreamHelper::ToStream(entity.spellSelectMode, stream);
	StreamHelper::SequenceToStream(entity.spellSelectArgs, stream);
	StreamHelper::ToStream(entity.spellSelectNumber, stream);
	StreamHelper::ToStream(entity.spellDelayEffective, stream);
	StreamHelper::ToStream(entity.spellEffectiveChance, stream);
	StreamHelper::ToStream(entity.spellEffectStyle, stream);
	StreamHelper::ToStream(entity.spellEffectType, stream);
	StreamHelper::ToStream(entity.spellEffectArgs, stream);
}

template<> void LoadFromText(SpellLevelEffectInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpellLevelEffectInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpellLevelEffectInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.spellLevelEffectID, value, "spellLevelEffectID");
	FromJson(entity.spellID, value, "spellID");
	FromJson(entity.spellLevelID, value, "spellLevelID");
	BlockFromJson(entity.spellEffectFlags, value, "spellEffectFlags");
	FromJson(entity.spellStageTrigger, value, "spellStageTrigger");
	FromJson(entity.spellDelayTrigger, value, "spellDelayTrigger");
	FromJson(entity.spellSelectType, value, "spellSelectType");
	FromJson(entity.spellSelectMode, value, "spellSelectMode");
	SequenceFromJson(entity.spellSelectArgs, value, "spellSelectArgs");
	FromJson(entity.spellSelectNumber, value, "spellSelectNumber");
	FromJson(entity.spellDelayEffective, value, "spellDelayEffective");
	FromJson(entity.spellEffectiveChance, value, "spellEffectiveChance");
	FromJson(entity.spellEffectStyle, value, "spellEffectStyle");
	FromJson(entity.spellEffectType, value, "spellEffectType");
	FromJson(entity.spellEffectArgs, value, "spellEffectArgs");
}

template<> void JsonHelper::BlockToJson(const SpellLevelEffectInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.spellLevelEffectID, value, "spellLevelEffectID");
	ToJson(entity.spellID, value, "spellID");
	ToJson(entity.spellLevelID, value, "spellLevelID");
	BlockToJson(entity.spellEffectFlags, value, "spellEffectFlags");
	ToJson(entity.spellStageTrigger, value, "spellStageTrigger");
	ToJson(entity.spellDelayTrigger, value, "spellDelayTrigger");
	ToJson(entity.spellSelectType, value, "spellSelectType");
	ToJson(entity.spellSelectMode, value, "spellSelectMode");
	SequenceToJson(entity.spellSelectArgs, value, "spellSelectArgs");
	ToJson(entity.spellSelectNumber, value, "spellSelectNumber");
	ToJson(entity.spellDelayEffective, value, "spellDelayEffective");
	ToJson(entity.spellEffectiveChance, value, "spellEffectiveChance");
	ToJson(entity.spellEffectStyle, value, "spellEffectStyle");
	ToJson(entity.spellEffectType, value, "spellEffectType");
	ToJson(entity.spellEffectArgs, value, "spellEffectArgs");
}

template<> void LoadFromStream(AuraSelectType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const AuraSelectType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(AuraSelectType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const AuraSelectType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(AuraSelectType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const AuraSelectType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}
