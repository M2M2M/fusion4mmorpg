#pragma once

enum class PlayerCareer
{
	None,
	Terran,
	Protoss,
	Demons,
	Max,
};

enum class PlayerGender
{
	None,
	Male,
	Female,
	Max,
};

enum class CurrencyType
{
	None,
	Gold,  // 金币
	Diamond,  // 钻石
	Max,
};

enum class ChequeType
{
	None,
	Gold,  // 金币
	Diamond,  // 钻石
	Exp,  // 经验
	VipExp,  // VIP经验
	Max,
};

enum class ItemFlag
{
	Binding,
	Constant,
};

struct ChequeInfo
{
	ChequeInfo();

	uint8 type;
	uint64 value;
};

struct ItemInfo
{
	ItemInfo();

	uint32 id;
	uint32 num;
};

struct FItemInfo
{
	FItemInfo();

	uint32 id;
	uint32 num;
	uint32 flags;
};

struct Configure
{
	Configure();

	uint32 cfgIndex;
	std::string cfgName;
	std::string cfgVal;
	std::string cfgDesc;
};
