#include "jsontable/table_helper.h"
#include "struct_vip.h"

template<> const char *GetTableName<PayShop>()
{
	return "pay_shop";
}

template<> const char *GetTableKeyName<PayShop>()
{
	return "Id";
}

template<> uint64 GetTableKeyValue(const PayShop &entity)
{
	return (uint64)entity.Id;
}

template<> void SetTableKeyValue(PayShop &entity, uint64 key)
{
	entity.Id = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<PayShop>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "strName";
		case 2: return "platformId";
		case 3: return "strChannels";
		case 4: return "itemType";
		case 5: return "buyPrice";
		case 6: return "buyGoldValue";
		case 7: return "buyCheques";
		case 8: return "buyItems";
		case 9: return "buyShowItemId";
		case 10: return "strRemark";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<PayShop>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "strName") == 0) return 1;
	if (strcmp(name, "platformId") == 0) return 2;
	if (strcmp(name, "strChannels") == 0) return 3;
	if (strcmp(name, "itemType") == 0) return 4;
	if (strcmp(name, "buyPrice") == 0) return 5;
	if (strcmp(name, "buyGoldValue") == 0) return 6;
	if (strcmp(name, "buyCheques") == 0) return 7;
	if (strcmp(name, "buyItems") == 0) return 8;
	if (strcmp(name, "buyShowItemId") == 0) return 9;
	if (strcmp(name, "strRemark") == 0) return 10;
	return -1;
}

template<> size_t GetTableFieldNumber<PayShop>()
{
	return 11;
}

template<> std::string GetTableFieldValue(const PayShop &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.strName);
		case 2: return StringHelper::ToString(entity.platformId);
		case 3: return StringHelper::ToString(entity.strChannels);
		case 4: return StringHelper::ToString(entity.itemType);
		case 5: return StringHelper::ToString(entity.buyPrice);
		case 6: return StringHelper::ToString(entity.buyGoldValue);
		case 7: return JsonHelper::BlockSequenceToJsonText(entity.buyCheques);
		case 8: return JsonHelper::BlockSequenceToJsonText(entity.buyItems);
		case 9: return StringHelper::ToString(entity.buyShowItemId);
		case 10: return StringHelper::ToString(entity.strRemark);
	}
	return "";
}

template<> void SetTableFieldValue(PayShop &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.strName, value);
		case 2: return StringHelper::FromString(entity.platformId, value);
		case 3: return StringHelper::FromString(entity.strChannels, value);
		case 4: return StringHelper::FromString(entity.itemType, value);
		case 5: return StringHelper::FromString(entity.buyPrice, value);
		case 6: return StringHelper::FromString(entity.buyGoldValue, value);
		case 7: return JsonHelper::BlockSequenceFromJsonText(entity.buyCheques, value);
		case 8: return JsonHelper::BlockSequenceFromJsonText(entity.buyItems, value);
		case 9: return StringHelper::FromString(entity.buyShowItemId, value);
		case 10: return StringHelper::FromString(entity.strRemark, value);
	}
}

template<> void LoadFromStream(PayShop &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.strName, stream);
	StreamHelper::FromStream(entity.platformId, stream);
	StreamHelper::FromStream(entity.strChannels, stream);
	StreamHelper::FromStream(entity.itemType, stream);
	StreamHelper::FromStream(entity.buyPrice, stream);
	StreamHelper::FromStream(entity.buyGoldValue, stream);
	StreamHelper::BlockSequenceFromStream(entity.buyCheques, stream);
	StreamHelper::BlockSequenceFromStream(entity.buyItems, stream);
	StreamHelper::FromStream(entity.buyShowItemId, stream);
	StreamHelper::FromStream(entity.strRemark, stream);
}

template<> void SaveToStream(const PayShop &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.strName, stream);
	StreamHelper::ToStream(entity.platformId, stream);
	StreamHelper::ToStream(entity.strChannels, stream);
	StreamHelper::ToStream(entity.itemType, stream);
	StreamHelper::ToStream(entity.buyPrice, stream);
	StreamHelper::ToStream(entity.buyGoldValue, stream);
	StreamHelper::BlockSequenceToStream(entity.buyCheques, stream);
	StreamHelper::BlockSequenceToStream(entity.buyItems, stream);
	StreamHelper::ToStream(entity.buyShowItemId, stream);
	StreamHelper::ToStream(entity.strRemark, stream);
}

template<> void LoadFromText(PayShop &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const PayShop &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(PayShop &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.strName, value, "strName");
	FromJson(entity.platformId, value, "platformId");
	FromJson(entity.strChannels, value, "strChannels");
	FromJson(entity.itemType, value, "itemType");
	FromJson(entity.buyPrice, value, "buyPrice");
	FromJson(entity.buyGoldValue, value, "buyGoldValue");
	BlockSequenceFromJson(entity.buyCheques, value, "buyCheques");
	BlockSequenceFromJson(entity.buyItems, value, "buyItems");
	FromJson(entity.buyShowItemId, value, "buyShowItemId");
	FromJson(entity.strRemark, value, "strRemark");
}

template<> void JsonHelper::BlockToJson(const PayShop &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.strName, value, "strName");
	ToJson(entity.platformId, value, "platformId");
	ToJson(entity.strChannels, value, "strChannels");
	ToJson(entity.itemType, value, "itemType");
	ToJson(entity.buyPrice, value, "buyPrice");
	ToJson(entity.buyGoldValue, value, "buyGoldValue");
	BlockSequenceToJson(entity.buyCheques, value, "buyCheques");
	BlockSequenceToJson(entity.buyItems, value, "buyItems");
	ToJson(entity.buyShowItemId, value, "buyShowItemId");
	ToJson(entity.strRemark, value, "strRemark");
}
