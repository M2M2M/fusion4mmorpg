#include "jsontable/table_helper.h"
#include "struct_map.h"

MapInfo::Flags::Flags()
: isParallelMap(false)
, isRecoveryHPDisable(false)
{
}

MapInfo::MapInfo()
: Id(0)
, type(0)
, viewing_distance(.0f)
, load_value(0)
, pop_map_id(0)
, pop_pos_x(.0f)
, pop_pos_y(.0f)
, pop_pos_z(.0f)
, pop_o(.0f)
{
}

MapZone::MapZone()
: Id(0)
, parentId(0)
, priority(0)
, map_id(0)
, map_type(0)
, x1(.0f)
, y1(.0f)
, z1(.0f)
, x2(.0f)
, y2(.0f)
, z2(.0f)
, pvp_flags(0)
{
}

MapGraveyard::MapGraveyard()
: Id(0)
, map_id(0)
, map_type(0)
, x(.0f)
, y(.0f)
, z(.0f)
, o(.0f)
, trait(0)
{
}

TeleportPoint::TeleportPoint()
: Id(0)
, map_id(0)
, map_type(0)
, x(.0f)
, y(.0f)
, z(.0f)
, o(.0f)
, trait(0)
{
}

WayPoint::WayPoint()
: Id(0)
, first_wp_id(0)
, prev_wp_id(0)
, next_wp_id(0)
, map_id(0)
, x(.0f)
, y(.0f)
, z(.0f)
, keep_idle(0)
, emote_state(0)
{
}

LandmarkPoint::LandmarkPoint()
: Id(0)
, map_id(0)
, map_type(0)
, x(.0f)
, y(.0f)
, z(.0f)
{
}
