#include "jsontable/table_helper.h"
#include "struct_char.h"

template<> void LoadFromStream(CharRaceType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const CharRaceType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(CharRaceType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CharRaceType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CharRaceType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const CharRaceType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(CharEliteType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const CharEliteType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(CharEliteType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CharEliteType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CharEliteType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const CharEliteType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(CharPrototype::Flags &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.isUndead, stream);
	StreamHelper::FromStream(entity.isJoinCombat, stream);
	StreamHelper::FromStream(entity.isFastTurn, stream);
	StreamHelper::FromStream(entity.isKeepDir, stream);
	StreamHelper::FromStream(entity.isActivity, stream);
	StreamHelper::FromStream(entity.isIgnoreInvisible, stream);
	StreamHelper::FromStream(entity.isSenseCreature, stream);
	StreamHelper::FromStream(entity.isSensePlayer, stream);
}

template<> void SaveToStream(const CharPrototype::Flags &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.isUndead, stream);
	StreamHelper::ToStream(entity.isJoinCombat, stream);
	StreamHelper::ToStream(entity.isFastTurn, stream);
	StreamHelper::ToStream(entity.isKeepDir, stream);
	StreamHelper::ToStream(entity.isActivity, stream);
	StreamHelper::ToStream(entity.isIgnoreInvisible, stream);
	StreamHelper::ToStream(entity.isSenseCreature, stream);
	StreamHelper::ToStream(entity.isSensePlayer, stream);
}

template<> void LoadFromText(CharPrototype::Flags &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CharPrototype::Flags &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CharPrototype::Flags &entity, const rapidjson::Value &value)
{
	FromJson(entity.isUndead, value, "isUndead");
	FromJson(entity.isJoinCombat, value, "isJoinCombat");
	FromJson(entity.isFastTurn, value, "isFastTurn");
	FromJson(entity.isKeepDir, value, "isKeepDir");
	FromJson(entity.isActivity, value, "isActivity");
	FromJson(entity.isIgnoreInvisible, value, "isIgnoreInvisible");
	FromJson(entity.isSenseCreature, value, "isSenseCreature");
	FromJson(entity.isSensePlayer, value, "isSensePlayer");
}

template<> void JsonHelper::BlockToJson(const CharPrototype::Flags &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.isUndead, value, "isUndead");
	ToJson(entity.isJoinCombat, value, "isJoinCombat");
	ToJson(entity.isFastTurn, value, "isFastTurn");
	ToJson(entity.isKeepDir, value, "isKeepDir");
	ToJson(entity.isActivity, value, "isActivity");
	ToJson(entity.isIgnoreInvisible, value, "isIgnoreInvisible");
	ToJson(entity.isSenseCreature, value, "isSenseCreature");
	ToJson(entity.isSensePlayer, value, "isSensePlayer");
}

template<> const char *GetTableName<CharPrototype>()
{
	return "char_prototype";
}

template<> const char *GetTableKeyName<CharPrototype>()
{
	return "charTypeId";
}

template<> uint64 GetTableKeyValue(const CharPrototype &entity)
{
	return (uint64)entity.charTypeId;
}

template<> void SetTableKeyValue(CharPrototype &entity, uint64 key)
{
	entity.charTypeId = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<CharPrototype>(size_t index)
{
	switch (index)
	{
		case 0: return "charTypeId";
		case 1: return "charFlags";
		case 2: return "charRace";
		case 3: return "charElite";
		case 4: return "attrType";
		case 5: return "level";
		case 6: return "speedWalk";
		case 7: return "speedRun";
		case 8: return "speedTurn";
		case 9: return "patrolRange";
		case 10: return "boundRadius";
		case 11: return "senseRadius";
		case 12: return "combatRadius";
		case 13: return "combatBestDist";
		case 14: return "minRespawnTime";
		case 15: return "maxRespawnTime";
		case 16: return "deadDisappearTime";
		case 17: return "recoveryHPRate";
		case 18: return "recoveryHPValue";
		case 19: return "recoveryMPRate";
		case 20: return "recoveryMPValue";
		case 21: return "lootSetID";
		case 22: return "aiScriptId";
		case 23: return "spawnScriptId";
		case 24: return "spawnScriptArgs";
		case 25: return "deadScriptId";
		case 26: return "deadScriptArgs";
		case 27: return "playScriptId";
		case 28: return "playScriptArgs";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<CharPrototype>(const char *name)
{
	if (strcmp(name, "charTypeId") == 0) return 0;
	if (strcmp(name, "charFlags") == 0) return 1;
	if (strcmp(name, "charRace") == 0) return 2;
	if (strcmp(name, "charElite") == 0) return 3;
	if (strcmp(name, "attrType") == 0) return 4;
	if (strcmp(name, "level") == 0) return 5;
	if (strcmp(name, "speedWalk") == 0) return 6;
	if (strcmp(name, "speedRun") == 0) return 7;
	if (strcmp(name, "speedTurn") == 0) return 8;
	if (strcmp(name, "patrolRange") == 0) return 9;
	if (strcmp(name, "boundRadius") == 0) return 10;
	if (strcmp(name, "senseRadius") == 0) return 11;
	if (strcmp(name, "combatRadius") == 0) return 12;
	if (strcmp(name, "combatBestDist") == 0) return 13;
	if (strcmp(name, "minRespawnTime") == 0) return 14;
	if (strcmp(name, "maxRespawnTime") == 0) return 15;
	if (strcmp(name, "deadDisappearTime") == 0) return 16;
	if (strcmp(name, "recoveryHPRate") == 0) return 17;
	if (strcmp(name, "recoveryHPValue") == 0) return 18;
	if (strcmp(name, "recoveryMPRate") == 0) return 19;
	if (strcmp(name, "recoveryMPValue") == 0) return 20;
	if (strcmp(name, "lootSetID") == 0) return 21;
	if (strcmp(name, "aiScriptId") == 0) return 22;
	if (strcmp(name, "spawnScriptId") == 0) return 23;
	if (strcmp(name, "spawnScriptArgs") == 0) return 24;
	if (strcmp(name, "deadScriptId") == 0) return 25;
	if (strcmp(name, "deadScriptArgs") == 0) return 26;
	if (strcmp(name, "playScriptId") == 0) return 27;
	if (strcmp(name, "playScriptArgs") == 0) return 28;
	return -1;
}

template<> size_t GetTableFieldNumber<CharPrototype>()
{
	return 29;
}

template<> std::string GetTableFieldValue(const CharPrototype &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.charTypeId);
		case 1: return JsonHelper::BlockToJsonText(entity.charFlags);
		case 2: return StringHelper::ToString(entity.charRace);
		case 3: return StringHelper::ToString(entity.charElite);
		case 4: return StringHelper::ToString(entity.attrType);
		case 5: return StringHelper::ToString(entity.level);
		case 6: return StringHelper::ToString(entity.speedWalk);
		case 7: return StringHelper::ToString(entity.speedRun);
		case 8: return StringHelper::ToString(entity.speedTurn);
		case 9: return StringHelper::ToString(entity.patrolRange);
		case 10: return StringHelper::ToString(entity.boundRadius);
		case 11: return StringHelper::ToString(entity.senseRadius);
		case 12: return StringHelper::ToString(entity.combatRadius);
		case 13: return StringHelper::ToString(entity.combatBestDist);
		case 14: return StringHelper::ToString(entity.minRespawnTime);
		case 15: return StringHelper::ToString(entity.maxRespawnTime);
		case 16: return StringHelper::ToString(entity.deadDisappearTime);
		case 17: return StringHelper::ToString(entity.recoveryHPRate);
		case 18: return StringHelper::ToString(entity.recoveryHPValue);
		case 19: return StringHelper::ToString(entity.recoveryMPRate);
		case 20: return StringHelper::ToString(entity.recoveryMPValue);
		case 21: return StringHelper::ToString(entity.lootSetID);
		case 22: return StringHelper::ToString(entity.aiScriptId);
		case 23: return StringHelper::ToString(entity.spawnScriptId);
		case 24: return StringHelper::ToString(entity.spawnScriptArgs);
		case 25: return StringHelper::ToString(entity.deadScriptId);
		case 26: return StringHelper::ToString(entity.deadScriptArgs);
		case 27: return StringHelper::ToString(entity.playScriptId);
		case 28: return StringHelper::ToString(entity.playScriptArgs);
	}
	return "";
}

template<> void SetTableFieldValue(CharPrototype &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.charTypeId, value);
		case 1: return JsonHelper::BlockFromJsonText(entity.charFlags, value);
		case 2: return StringHelper::FromString(entity.charRace, value);
		case 3: return StringHelper::FromString(entity.charElite, value);
		case 4: return StringHelper::FromString(entity.attrType, value);
		case 5: return StringHelper::FromString(entity.level, value);
		case 6: return StringHelper::FromString(entity.speedWalk, value);
		case 7: return StringHelper::FromString(entity.speedRun, value);
		case 8: return StringHelper::FromString(entity.speedTurn, value);
		case 9: return StringHelper::FromString(entity.patrolRange, value);
		case 10: return StringHelper::FromString(entity.boundRadius, value);
		case 11: return StringHelper::FromString(entity.senseRadius, value);
		case 12: return StringHelper::FromString(entity.combatRadius, value);
		case 13: return StringHelper::FromString(entity.combatBestDist, value);
		case 14: return StringHelper::FromString(entity.minRespawnTime, value);
		case 15: return StringHelper::FromString(entity.maxRespawnTime, value);
		case 16: return StringHelper::FromString(entity.deadDisappearTime, value);
		case 17: return StringHelper::FromString(entity.recoveryHPRate, value);
		case 18: return StringHelper::FromString(entity.recoveryHPValue, value);
		case 19: return StringHelper::FromString(entity.recoveryMPRate, value);
		case 20: return StringHelper::FromString(entity.recoveryMPValue, value);
		case 21: return StringHelper::FromString(entity.lootSetID, value);
		case 22: return StringHelper::FromString(entity.aiScriptId, value);
		case 23: return StringHelper::FromString(entity.spawnScriptId, value);
		case 24: return StringHelper::FromString(entity.spawnScriptArgs, value);
		case 25: return StringHelper::FromString(entity.deadScriptId, value);
		case 26: return StringHelper::FromString(entity.deadScriptArgs, value);
		case 27: return StringHelper::FromString(entity.playScriptId, value);
		case 28: return StringHelper::FromString(entity.playScriptArgs, value);
	}
}

template<> void LoadFromStream(CharPrototype &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.charTypeId, stream);
	LoadFromStream(entity.charFlags, stream);
	StreamHelper::FromStream(entity.charRace, stream);
	StreamHelper::FromStream(entity.charElite, stream);
	StreamHelper::FromStream(entity.attrType, stream);
	StreamHelper::FromStream(entity.level, stream);
	StreamHelper::FromStream(entity.speedWalk, stream);
	StreamHelper::FromStream(entity.speedRun, stream);
	StreamHelper::FromStream(entity.speedTurn, stream);
	StreamHelper::FromStream(entity.patrolRange, stream);
	StreamHelper::FromStream(entity.boundRadius, stream);
	StreamHelper::FromStream(entity.senseRadius, stream);
	StreamHelper::FromStream(entity.combatRadius, stream);
	StreamHelper::FromStream(entity.combatBestDist, stream);
	StreamHelper::FromStream(entity.minRespawnTime, stream);
	StreamHelper::FromStream(entity.maxRespawnTime, stream);
	StreamHelper::FromStream(entity.deadDisappearTime, stream);
	StreamHelper::FromStream(entity.recoveryHPRate, stream);
	StreamHelper::FromStream(entity.recoveryHPValue, stream);
	StreamHelper::FromStream(entity.recoveryMPRate, stream);
	StreamHelper::FromStream(entity.recoveryMPValue, stream);
	StreamHelper::FromStream(entity.lootSetID, stream);
	StreamHelper::FromStream(entity.aiScriptId, stream);
	StreamHelper::FromStream(entity.spawnScriptId, stream);
	StreamHelper::FromStream(entity.spawnScriptArgs, stream);
	StreamHelper::FromStream(entity.deadScriptId, stream);
	StreamHelper::FromStream(entity.deadScriptArgs, stream);
	StreamHelper::FromStream(entity.playScriptId, stream);
	StreamHelper::FromStream(entity.playScriptArgs, stream);
}

template<> void SaveToStream(const CharPrototype &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.charTypeId, stream);
	SaveToStream(entity.charFlags, stream);
	StreamHelper::ToStream(entity.charRace, stream);
	StreamHelper::ToStream(entity.charElite, stream);
	StreamHelper::ToStream(entity.attrType, stream);
	StreamHelper::ToStream(entity.level, stream);
	StreamHelper::ToStream(entity.speedWalk, stream);
	StreamHelper::ToStream(entity.speedRun, stream);
	StreamHelper::ToStream(entity.speedTurn, stream);
	StreamHelper::ToStream(entity.patrolRange, stream);
	StreamHelper::ToStream(entity.boundRadius, stream);
	StreamHelper::ToStream(entity.senseRadius, stream);
	StreamHelper::ToStream(entity.combatRadius, stream);
	StreamHelper::ToStream(entity.combatBestDist, stream);
	StreamHelper::ToStream(entity.minRespawnTime, stream);
	StreamHelper::ToStream(entity.maxRespawnTime, stream);
	StreamHelper::ToStream(entity.deadDisappearTime, stream);
	StreamHelper::ToStream(entity.recoveryHPRate, stream);
	StreamHelper::ToStream(entity.recoveryHPValue, stream);
	StreamHelper::ToStream(entity.recoveryMPRate, stream);
	StreamHelper::ToStream(entity.recoveryMPValue, stream);
	StreamHelper::ToStream(entity.lootSetID, stream);
	StreamHelper::ToStream(entity.aiScriptId, stream);
	StreamHelper::ToStream(entity.spawnScriptId, stream);
	StreamHelper::ToStream(entity.spawnScriptArgs, stream);
	StreamHelper::ToStream(entity.deadScriptId, stream);
	StreamHelper::ToStream(entity.deadScriptArgs, stream);
	StreamHelper::ToStream(entity.playScriptId, stream);
	StreamHelper::ToStream(entity.playScriptArgs, stream);
}

template<> void LoadFromText(CharPrototype &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CharPrototype &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CharPrototype &entity, const rapidjson::Value &value)
{
	FromJson(entity.charTypeId, value, "charTypeId");
	BlockFromJson(entity.charFlags, value, "charFlags");
	FromJson(entity.charRace, value, "charRace");
	FromJson(entity.charElite, value, "charElite");
	FromJson(entity.attrType, value, "attrType");
	FromJson(entity.level, value, "level");
	FromJson(entity.speedWalk, value, "speedWalk");
	FromJson(entity.speedRun, value, "speedRun");
	FromJson(entity.speedTurn, value, "speedTurn");
	FromJson(entity.patrolRange, value, "patrolRange");
	FromJson(entity.boundRadius, value, "boundRadius");
	FromJson(entity.senseRadius, value, "senseRadius");
	FromJson(entity.combatRadius, value, "combatRadius");
	FromJson(entity.combatBestDist, value, "combatBestDist");
	FromJson(entity.minRespawnTime, value, "minRespawnTime");
	FromJson(entity.maxRespawnTime, value, "maxRespawnTime");
	FromJson(entity.deadDisappearTime, value, "deadDisappearTime");
	FromJson(entity.recoveryHPRate, value, "recoveryHPRate");
	FromJson(entity.recoveryHPValue, value, "recoveryHPValue");
	FromJson(entity.recoveryMPRate, value, "recoveryMPRate");
	FromJson(entity.recoveryMPValue, value, "recoveryMPValue");
	FromJson(entity.lootSetID, value, "lootSetID");
	FromJson(entity.aiScriptId, value, "aiScriptId");
	FromJson(entity.spawnScriptId, value, "spawnScriptId");
	FromJson(entity.spawnScriptArgs, value, "spawnScriptArgs");
	FromJson(entity.deadScriptId, value, "deadScriptId");
	FromJson(entity.deadScriptArgs, value, "deadScriptArgs");
	FromJson(entity.playScriptId, value, "playScriptId");
	FromJson(entity.playScriptArgs, value, "playScriptArgs");
}

template<> void JsonHelper::BlockToJson(const CharPrototype &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.charTypeId, value, "charTypeId");
	BlockToJson(entity.charFlags, value, "charFlags");
	ToJson(entity.charRace, value, "charRace");
	ToJson(entity.charElite, value, "charElite");
	ToJson(entity.attrType, value, "attrType");
	ToJson(entity.level, value, "level");
	ToJson(entity.speedWalk, value, "speedWalk");
	ToJson(entity.speedRun, value, "speedRun");
	ToJson(entity.speedTurn, value, "speedTurn");
	ToJson(entity.patrolRange, value, "patrolRange");
	ToJson(entity.boundRadius, value, "boundRadius");
	ToJson(entity.senseRadius, value, "senseRadius");
	ToJson(entity.combatRadius, value, "combatRadius");
	ToJson(entity.combatBestDist, value, "combatBestDist");
	ToJson(entity.minRespawnTime, value, "minRespawnTime");
	ToJson(entity.maxRespawnTime, value, "maxRespawnTime");
	ToJson(entity.deadDisappearTime, value, "deadDisappearTime");
	ToJson(entity.recoveryHPRate, value, "recoveryHPRate");
	ToJson(entity.recoveryHPValue, value, "recoveryHPValue");
	ToJson(entity.recoveryMPRate, value, "recoveryMPRate");
	ToJson(entity.recoveryMPValue, value, "recoveryMPValue");
	ToJson(entity.lootSetID, value, "lootSetID");
	ToJson(entity.aiScriptId, value, "aiScriptId");
	ToJson(entity.spawnScriptId, value, "spawnScriptId");
	ToJson(entity.spawnScriptArgs, value, "spawnScriptArgs");
	ToJson(entity.deadScriptId, value, "deadScriptId");
	ToJson(entity.deadScriptArgs, value, "deadScriptArgs");
	ToJson(entity.playScriptId, value, "playScriptId");
	ToJson(entity.playScriptArgs, value, "playScriptArgs");
}

template<> void LoadFromStream(CreatureSpawn::Flags &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.isRespawn, stream);
	StreamHelper::FromStream(entity.isPlaceholder, stream);
}

template<> void SaveToStream(const CreatureSpawn::Flags &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.isRespawn, stream);
	StreamHelper::ToStream(entity.isPlaceholder, stream);
}

template<> void LoadFromText(CreatureSpawn::Flags &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CreatureSpawn::Flags &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CreatureSpawn::Flags &entity, const rapidjson::Value &value)
{
	FromJson(entity.isRespawn, value, "isRespawn");
	FromJson(entity.isPlaceholder, value, "isPlaceholder");
}

template<> void JsonHelper::BlockToJson(const CreatureSpawn::Flags &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.isRespawn, value, "isRespawn");
	ToJson(entity.isPlaceholder, value, "isPlaceholder");
}

template<> const char *GetTableName<CreatureSpawn>()
{
	return "creature_spawn";
}

template<> const char *GetTableKeyName<CreatureSpawn>()
{
	return "spawnId";
}

template<> uint64 GetTableKeyValue(const CreatureSpawn &entity)
{
	return (uint64)entity.spawnId;
}

template<> void SetTableKeyValue(CreatureSpawn &entity, uint64 key)
{
	entity.spawnId = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<CreatureSpawn>(size_t index)
{
	switch (index)
	{
		case 0: return "spawnId";
		case 1: return "flags";
		case 2: return "entry";
		case 3: return "level";
		case 4: return "round";
		case 5: return "map_id";
		case 6: return "map_type";
		case 7: return "x";
		case 8: return "y";
		case 9: return "z";
		case 10: return "o";
		case 11: return "idleType";
		case 12: return "wayPointId";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<CreatureSpawn>(const char *name)
{
	if (strcmp(name, "spawnId") == 0) return 0;
	if (strcmp(name, "flags") == 0) return 1;
	if (strcmp(name, "entry") == 0) return 2;
	if (strcmp(name, "level") == 0) return 3;
	if (strcmp(name, "round") == 0) return 4;
	if (strcmp(name, "map_id") == 0) return 5;
	if (strcmp(name, "map_type") == 0) return 6;
	if (strcmp(name, "x") == 0) return 7;
	if (strcmp(name, "y") == 0) return 8;
	if (strcmp(name, "z") == 0) return 9;
	if (strcmp(name, "o") == 0) return 10;
	if (strcmp(name, "idleType") == 0) return 11;
	if (strcmp(name, "wayPointId") == 0) return 12;
	return -1;
}

template<> size_t GetTableFieldNumber<CreatureSpawn>()
{
	return 13;
}

template<> std::string GetTableFieldValue(const CreatureSpawn &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.spawnId);
		case 1: return JsonHelper::BlockToJsonText(entity.flags);
		case 2: return StringHelper::ToString(entity.entry);
		case 3: return StringHelper::ToString(entity.level);
		case 4: return StringHelper::ToString(entity.round);
		case 5: return StringHelper::ToString(entity.map_id);
		case 6: return StringHelper::ToString(entity.map_type);
		case 7: return StringHelper::ToString(entity.x);
		case 8: return StringHelper::ToString(entity.y);
		case 9: return StringHelper::ToString(entity.z);
		case 10: return StringHelper::ToString(entity.o);
		case 11: return StringHelper::ToString(entity.idleType);
		case 12: return StringHelper::ToString(entity.wayPointId);
	}
	return "";
}

template<> void SetTableFieldValue(CreatureSpawn &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.spawnId, value);
		case 1: return JsonHelper::BlockFromJsonText(entity.flags, value);
		case 2: return StringHelper::FromString(entity.entry, value);
		case 3: return StringHelper::FromString(entity.level, value);
		case 4: return StringHelper::FromString(entity.round, value);
		case 5: return StringHelper::FromString(entity.map_id, value);
		case 6: return StringHelper::FromString(entity.map_type, value);
		case 7: return StringHelper::FromString(entity.x, value);
		case 8: return StringHelper::FromString(entity.y, value);
		case 9: return StringHelper::FromString(entity.z, value);
		case 10: return StringHelper::FromString(entity.o, value);
		case 11: return StringHelper::FromString(entity.idleType, value);
		case 12: return StringHelper::FromString(entity.wayPointId, value);
	}
}

template<> void LoadFromStream(CreatureSpawn &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.spawnId, stream);
	LoadFromStream(entity.flags, stream);
	StreamHelper::FromStream(entity.entry, stream);
	StreamHelper::FromStream(entity.level, stream);
	StreamHelper::FromStream(entity.round, stream);
	StreamHelper::FromStream(entity.map_id, stream);
	StreamHelper::FromStream(entity.map_type, stream);
	StreamHelper::FromStream(entity.x, stream);
	StreamHelper::FromStream(entity.y, stream);
	StreamHelper::FromStream(entity.z, stream);
	StreamHelper::FromStream(entity.o, stream);
	StreamHelper::FromStream(entity.idleType, stream);
	StreamHelper::FromStream(entity.wayPointId, stream);
}

template<> void SaveToStream(const CreatureSpawn &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.spawnId, stream);
	SaveToStream(entity.flags, stream);
	StreamHelper::ToStream(entity.entry, stream);
	StreamHelper::ToStream(entity.level, stream);
	StreamHelper::ToStream(entity.round, stream);
	StreamHelper::ToStream(entity.map_id, stream);
	StreamHelper::ToStream(entity.map_type, stream);
	StreamHelper::ToStream(entity.x, stream);
	StreamHelper::ToStream(entity.y, stream);
	StreamHelper::ToStream(entity.z, stream);
	StreamHelper::ToStream(entity.o, stream);
	StreamHelper::ToStream(entity.idleType, stream);
	StreamHelper::ToStream(entity.wayPointId, stream);
}

template<> void LoadFromText(CreatureSpawn &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CreatureSpawn &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CreatureSpawn &entity, const rapidjson::Value &value)
{
	FromJson(entity.spawnId, value, "spawnId");
	BlockFromJson(entity.flags, value, "flags");
	FromJson(entity.entry, value, "entry");
	FromJson(entity.level, value, "level");
	FromJson(entity.round, value, "round");
	FromJson(entity.map_id, value, "map_id");
	FromJson(entity.map_type, value, "map_type");
	FromJson(entity.x, value, "x");
	FromJson(entity.y, value, "y");
	FromJson(entity.z, value, "z");
	FromJson(entity.o, value, "o");
	FromJson(entity.idleType, value, "idleType");
	FromJson(entity.wayPointId, value, "wayPointId");
}

template<> void JsonHelper::BlockToJson(const CreatureSpawn &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.spawnId, value, "spawnId");
	BlockToJson(entity.flags, value, "flags");
	ToJson(entity.entry, value, "entry");
	ToJson(entity.level, value, "level");
	ToJson(entity.round, value, "round");
	ToJson(entity.map_id, value, "map_id");
	ToJson(entity.map_type, value, "map_type");
	ToJson(entity.x, value, "x");
	ToJson(entity.y, value, "y");
	ToJson(entity.z, value, "z");
	ToJson(entity.o, value, "o");
	ToJson(entity.idleType, value, "idleType");
	ToJson(entity.wayPointId, value, "wayPointId");
}
