#pragma once

struct SObjPrototype
{
	SObjPrototype();

	struct Flags {
		Flags();
		bool isExclusiveTrigger;
		bool isRemoveAfterTriggerDone;
		bool isPlayerInteresting;
		bool isCreatureInteresting;
		bool isActivity;
	};
	struct CostItem {
		CostItem();
		uint32 itemId;
		uint32 itemNum;
	};

	uint32 sobjTypeId;
	Flags sobjFlags;

	uint32 minRespawnTime;
	uint32 maxRespawnTime;

	uint32 teleportPointID;
	uint32 teleportDelayTime;

	uint32 lootSetID;
	uint32 triggerSpellID;
	uint32 triggerSpellLv;
	uint32 triggerAnimTime;
	std::vector<CostItem> triggerCostItems;

	uint32 sobjReqQuestDoing;

	float radius;

	uint32 spawnScriptId;
	std::string spawnScriptArgs;
	uint32 playScriptId;
	std::string playScriptArgs;
};


struct StaticObjectSpawn
{
	StaticObjectSpawn();

	struct Flags {
		Flags();
		bool isRespawn;
		bool isPlaceholder;
	};
	uint32 spawnId;
	Flags flags;
	uint32 entry;
	uint32 map_id;
	uint32 map_type;
	float x, y, z, o;
	float radius;
};
