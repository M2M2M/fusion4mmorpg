#include "jsontable/table_helper.h"
#include "struct_shop.h"

template<> const char *GetTableName<ShopPrototype>()
{
	return "shop_prototype";
}

template<> const char *GetTableKeyName<ShopPrototype>()
{
	return "Id";
}

template<> uint64 GetTableKeyValue(const ShopPrototype &entity)
{
	return (uint64)entity.Id;
}

template<> void SetTableKeyValue(ShopPrototype &entity, uint64 key)
{
	entity.Id = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<ShopPrototype>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "shopType";
		case 2: return "itemTypeID";
		case 3: return "itemCount";
		case 4: return "itemFlags";
		case 5: return "currencyType";
		case 6: return "sellPrice";
		case 7: return "discountPrice";
		case 8: return "bundleNumMax";
		case 9: return "genderLimit";
		case 10: return "careerLimit";
		case 11: return "dailyBuyLimit";
		case 12: return "weeklyBuyLimit";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<ShopPrototype>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "shopType") == 0) return 1;
	if (strcmp(name, "itemTypeID") == 0) return 2;
	if (strcmp(name, "itemCount") == 0) return 3;
	if (strcmp(name, "itemFlags") == 0) return 4;
	if (strcmp(name, "currencyType") == 0) return 5;
	if (strcmp(name, "sellPrice") == 0) return 6;
	if (strcmp(name, "discountPrice") == 0) return 7;
	if (strcmp(name, "bundleNumMax") == 0) return 8;
	if (strcmp(name, "genderLimit") == 0) return 9;
	if (strcmp(name, "careerLimit") == 0) return 10;
	if (strcmp(name, "dailyBuyLimit") == 0) return 11;
	if (strcmp(name, "weeklyBuyLimit") == 0) return 12;
	return -1;
}

template<> size_t GetTableFieldNumber<ShopPrototype>()
{
	return 13;
}

template<> std::string GetTableFieldValue(const ShopPrototype &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.shopType);
		case 2: return StringHelper::ToString(entity.itemTypeID);
		case 3: return StringHelper::ToString(entity.itemCount);
		case 4: return StringHelper::ToString(entity.itemFlags);
		case 5: return StringHelper::ToString(entity.currencyType);
		case 6: return StringHelper::ToString(entity.sellPrice);
		case 7: return StringHelper::ToString(entity.discountPrice);
		case 8: return StringHelper::ToString(entity.bundleNumMax);
		case 9: return StringHelper::ToString(entity.genderLimit);
		case 10: return StringHelper::ToString(entity.careerLimit);
		case 11: return StringHelper::ToString(entity.dailyBuyLimit);
		case 12: return StringHelper::ToString(entity.weeklyBuyLimit);
	}
	return "";
}

template<> void SetTableFieldValue(ShopPrototype &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.shopType, value);
		case 2: return StringHelper::FromString(entity.itemTypeID, value);
		case 3: return StringHelper::FromString(entity.itemCount, value);
		case 4: return StringHelper::FromString(entity.itemFlags, value);
		case 5: return StringHelper::FromString(entity.currencyType, value);
		case 6: return StringHelper::FromString(entity.sellPrice, value);
		case 7: return StringHelper::FromString(entity.discountPrice, value);
		case 8: return StringHelper::FromString(entity.bundleNumMax, value);
		case 9: return StringHelper::FromString(entity.genderLimit, value);
		case 10: return StringHelper::FromString(entity.careerLimit, value);
		case 11: return StringHelper::FromString(entity.dailyBuyLimit, value);
		case 12: return StringHelper::FromString(entity.weeklyBuyLimit, value);
	}
}

template<> void LoadFromStream(ShopPrototype &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.shopType, stream);
	StreamHelper::FromStream(entity.itemTypeID, stream);
	StreamHelper::FromStream(entity.itemCount, stream);
	StreamHelper::FromStream(entity.itemFlags, stream);
	StreamHelper::FromStream(entity.currencyType, stream);
	StreamHelper::FromStream(entity.sellPrice, stream);
	StreamHelper::FromStream(entity.discountPrice, stream);
	StreamHelper::FromStream(entity.bundleNumMax, stream);
	StreamHelper::FromStream(entity.genderLimit, stream);
	StreamHelper::FromStream(entity.careerLimit, stream);
	StreamHelper::FromStream(entity.dailyBuyLimit, stream);
	StreamHelper::FromStream(entity.weeklyBuyLimit, stream);
}

template<> void SaveToStream(const ShopPrototype &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.shopType, stream);
	StreamHelper::ToStream(entity.itemTypeID, stream);
	StreamHelper::ToStream(entity.itemCount, stream);
	StreamHelper::ToStream(entity.itemFlags, stream);
	StreamHelper::ToStream(entity.currencyType, stream);
	StreamHelper::ToStream(entity.sellPrice, stream);
	StreamHelper::ToStream(entity.discountPrice, stream);
	StreamHelper::ToStream(entity.bundleNumMax, stream);
	StreamHelper::ToStream(entity.genderLimit, stream);
	StreamHelper::ToStream(entity.careerLimit, stream);
	StreamHelper::ToStream(entity.dailyBuyLimit, stream);
	StreamHelper::ToStream(entity.weeklyBuyLimit, stream);
}

template<> void LoadFromText(ShopPrototype &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const ShopPrototype &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(ShopPrototype &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.shopType, value, "shopType");
	FromJson(entity.itemTypeID, value, "itemTypeID");
	FromJson(entity.itemCount, value, "itemCount");
	FromJson(entity.itemFlags, value, "itemFlags");
	FromJson(entity.currencyType, value, "currencyType");
	FromJson(entity.sellPrice, value, "sellPrice");
	FromJson(entity.discountPrice, value, "discountPrice");
	FromJson(entity.bundleNumMax, value, "bundleNumMax");
	FromJson(entity.genderLimit, value, "genderLimit");
	FromJson(entity.careerLimit, value, "careerLimit");
	FromJson(entity.dailyBuyLimit, value, "dailyBuyLimit");
	FromJson(entity.weeklyBuyLimit, value, "weeklyBuyLimit");
}

template<> void JsonHelper::BlockToJson(const ShopPrototype &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.shopType, value, "shopType");
	ToJson(entity.itemTypeID, value, "itemTypeID");
	ToJson(entity.itemCount, value, "itemCount");
	ToJson(entity.itemFlags, value, "itemFlags");
	ToJson(entity.currencyType, value, "currencyType");
	ToJson(entity.sellPrice, value, "sellPrice");
	ToJson(entity.discountPrice, value, "discountPrice");
	ToJson(entity.bundleNumMax, value, "bundleNumMax");
	ToJson(entity.genderLimit, value, "genderLimit");
	ToJson(entity.careerLimit, value, "careerLimit");
	ToJson(entity.dailyBuyLimit, value, "dailyBuyLimit");
	ToJson(entity.weeklyBuyLimit, value, "weeklyBuyLimit");
}

template<> void LoadFromStream(SpecialShopPrototype &entity, std::istream &stream)
{
	LoadFromStream<ShopPrototype>(entity, stream);
	StreamHelper::FromStream(entity.itemUniqueKey, stream);
	StreamHelper::FromStream(entity.itemBeginTime, stream);
	StreamHelper::FromStream(entity.itemEndTime, stream);
	StreamHelper::FromStream(entity.serverBuyLimit, stream);
	StreamHelper::FromStream(entity.charBuyLimit, stream);
	StreamHelper::FromStream(entity.dailyServerBuyLimit, stream);
	StreamHelper::FromStream(entity.weeklyServerBuyLimit, stream);
}

template<> void SaveToStream(const SpecialShopPrototype &entity, std::ostream &stream)
{
	SaveToStream<ShopPrototype>(entity, stream);
	StreamHelper::ToStream(entity.itemUniqueKey, stream);
	StreamHelper::ToStream(entity.itemBeginTime, stream);
	StreamHelper::ToStream(entity.itemEndTime, stream);
	StreamHelper::ToStream(entity.serverBuyLimit, stream);
	StreamHelper::ToStream(entity.charBuyLimit, stream);
	StreamHelper::ToStream(entity.dailyServerBuyLimit, stream);
	StreamHelper::ToStream(entity.weeklyServerBuyLimit, stream);
}

template<> void LoadFromText(SpecialShopPrototype &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const SpecialShopPrototype &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(SpecialShopPrototype &entity, const rapidjson::Value &value)
{
	BlockFromJson<ShopPrototype>(entity, value);
	FromJson(entity.itemUniqueKey, value, "itemUniqueKey");
	FromJson(entity.itemBeginTime, value, "itemBeginTime");
	FromJson(entity.itemEndTime, value, "itemEndTime");
	FromJson(entity.serverBuyLimit, value, "serverBuyLimit");
	FromJson(entity.charBuyLimit, value, "charBuyLimit");
	FromJson(entity.dailyServerBuyLimit, value, "dailyServerBuyLimit");
	FromJson(entity.weeklyServerBuyLimit, value, "weeklyServerBuyLimit");
}

template<> void JsonHelper::BlockToJson(const SpecialShopPrototype &entity, rapidjson::Value &value)
{
	BlockToJson<ShopPrototype>(entity, value);
	ToJson(entity.itemUniqueKey, value, "itemUniqueKey");
	ToJson(entity.itemBeginTime, value, "itemBeginTime");
	ToJson(entity.itemEndTime, value, "itemEndTime");
	ToJson(entity.serverBuyLimit, value, "serverBuyLimit");
	ToJson(entity.charBuyLimit, value, "charBuyLimit");
	ToJson(entity.dailyServerBuyLimit, value, "dailyServerBuyLimit");
	ToJson(entity.weeklyServerBuyLimit, value, "weeklyServerBuyLimit");
}
