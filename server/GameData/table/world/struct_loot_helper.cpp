#include "jsontable/table_helper.h"
#include "struct_loot.h"

template<> const char *GetTableName<LootSet>()
{
	return "loot_set";
}

template<> const char *GetTableKeyName<LootSet>()
{
	return "lsID";
}

template<> uint64 GetTableKeyValue(const LootSet &entity)
{
	return (uint64)entity.lsID;
}

template<> void SetTableKeyValue(LootSet &entity, uint64 key)
{
	entity.lsID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<LootSet>(size_t index)
{
	switch (index)
	{
		case 0: return "lsID";
		case 1: return "lsName";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<LootSet>(const char *name)
{
	if (strcmp(name, "lsID") == 0) return 0;
	if (strcmp(name, "lsName") == 0) return 1;
	return -1;
}

template<> size_t GetTableFieldNumber<LootSet>()
{
	return 2;
}

template<> std::string GetTableFieldValue(const LootSet &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.lsID);
		case 1: return StringHelper::ToString(entity.lsName);
	}
	return "";
}

template<> void SetTableFieldValue(LootSet &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.lsID, value);
		case 1: return StringHelper::FromString(entity.lsName, value);
	}
}

template<> void LoadFromStream(LootSet &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.lsID, stream);
	StreamHelper::FromStream(entity.lsName, stream);
}

template<> void SaveToStream(const LootSet &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.lsID, stream);
	StreamHelper::ToStream(entity.lsName, stream);
}

template<> void LoadFromText(LootSet &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const LootSet &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(LootSet &entity, const rapidjson::Value &value)
{
	FromJson(entity.lsID, value, "lsID");
	FromJson(entity.lsName, value, "lsName");
}

template<> void JsonHelper::BlockToJson(const LootSet &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.lsID, value, "lsID");
	ToJson(entity.lsName, value, "lsName");
}

template<> const char *GetTableName<LootSetGroup>()
{
	return "loot_set_group";
}

template<> const char *GetTableKeyName<LootSetGroup>()
{
	return "lsgID";
}

template<> uint64 GetTableKeyValue(const LootSetGroup &entity)
{
	return (uint64)entity.lsgID;
}

template<> void SetTableKeyValue(LootSetGroup &entity, uint64 key)
{
	entity.lsgID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<LootSetGroup>(size_t index)
{
	switch (index)
	{
		case 0: return "lsgID";
		case 1: return "lsID";
		case 2: return "lsgName";
		case 3: return "lsgOdds";
		case 4: return "lsgMinItemTimes";
		case 5: return "lsgMaxItemTimes";
		case 6: return "lsgMinChequeTimes";
		case 7: return "lsgMaxChequeTimes";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<LootSetGroup>(const char *name)
{
	if (strcmp(name, "lsgID") == 0) return 0;
	if (strcmp(name, "lsID") == 0) return 1;
	if (strcmp(name, "lsgName") == 0) return 2;
	if (strcmp(name, "lsgOdds") == 0) return 3;
	if (strcmp(name, "lsgMinItemTimes") == 0) return 4;
	if (strcmp(name, "lsgMaxItemTimes") == 0) return 5;
	if (strcmp(name, "lsgMinChequeTimes") == 0) return 6;
	if (strcmp(name, "lsgMaxChequeTimes") == 0) return 7;
	return -1;
}

template<> size_t GetTableFieldNumber<LootSetGroup>()
{
	return 8;
}

template<> std::string GetTableFieldValue(const LootSetGroup &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.lsgID);
		case 1: return StringHelper::ToString(entity.lsID);
		case 2: return StringHelper::ToString(entity.lsgName);
		case 3: return StringHelper::ToString(entity.lsgOdds);
		case 4: return StringHelper::ToString(entity.lsgMinItemTimes);
		case 5: return StringHelper::ToString(entity.lsgMaxItemTimes);
		case 6: return StringHelper::ToString(entity.lsgMinChequeTimes);
		case 7: return StringHelper::ToString(entity.lsgMaxChequeTimes);
	}
	return "";
}

template<> void SetTableFieldValue(LootSetGroup &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.lsgID, value);
		case 1: return StringHelper::FromString(entity.lsID, value);
		case 2: return StringHelper::FromString(entity.lsgName, value);
		case 3: return StringHelper::FromString(entity.lsgOdds, value);
		case 4: return StringHelper::FromString(entity.lsgMinItemTimes, value);
		case 5: return StringHelper::FromString(entity.lsgMaxItemTimes, value);
		case 6: return StringHelper::FromString(entity.lsgMinChequeTimes, value);
		case 7: return StringHelper::FromString(entity.lsgMaxChequeTimes, value);
	}
}

template<> void LoadFromStream(LootSetGroup &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.lsgID, stream);
	StreamHelper::FromStream(entity.lsID, stream);
	StreamHelper::FromStream(entity.lsgName, stream);
	StreamHelper::FromStream(entity.lsgOdds, stream);
	StreamHelper::FromStream(entity.lsgMinItemTimes, stream);
	StreamHelper::FromStream(entity.lsgMaxItemTimes, stream);
	StreamHelper::FromStream(entity.lsgMinChequeTimes, stream);
	StreamHelper::FromStream(entity.lsgMaxChequeTimes, stream);
}

template<> void SaveToStream(const LootSetGroup &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.lsgID, stream);
	StreamHelper::ToStream(entity.lsID, stream);
	StreamHelper::ToStream(entity.lsgName, stream);
	StreamHelper::ToStream(entity.lsgOdds, stream);
	StreamHelper::ToStream(entity.lsgMinItemTimes, stream);
	StreamHelper::ToStream(entity.lsgMaxItemTimes, stream);
	StreamHelper::ToStream(entity.lsgMinChequeTimes, stream);
	StreamHelper::ToStream(entity.lsgMaxChequeTimes, stream);
}

template<> void LoadFromText(LootSetGroup &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const LootSetGroup &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(LootSetGroup &entity, const rapidjson::Value &value)
{
	FromJson(entity.lsgID, value, "lsgID");
	FromJson(entity.lsID, value, "lsID");
	FromJson(entity.lsgName, value, "lsgName");
	FromJson(entity.lsgOdds, value, "lsgOdds");
	FromJson(entity.lsgMinItemTimes, value, "lsgMinItemTimes");
	FromJson(entity.lsgMaxItemTimes, value, "lsgMaxItemTimes");
	FromJson(entity.lsgMinChequeTimes, value, "lsgMinChequeTimes");
	FromJson(entity.lsgMaxChequeTimes, value, "lsgMaxChequeTimes");
}

template<> void JsonHelper::BlockToJson(const LootSetGroup &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.lsgID, value, "lsgID");
	ToJson(entity.lsID, value, "lsID");
	ToJson(entity.lsgName, value, "lsgName");
	ToJson(entity.lsgOdds, value, "lsgOdds");
	ToJson(entity.lsgMinItemTimes, value, "lsgMinItemTimes");
	ToJson(entity.lsgMaxItemTimes, value, "lsgMaxItemTimes");
	ToJson(entity.lsgMinChequeTimes, value, "lsgMinChequeTimes");
	ToJson(entity.lsgMaxChequeTimes, value, "lsgMaxChequeTimes");
}

template<> void LoadFromStream(LootSetGroupItem::Flags &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.bindToPicker, stream);
}

template<> void SaveToStream(const LootSetGroupItem::Flags &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.bindToPicker, stream);
}

template<> void LoadFromText(LootSetGroupItem::Flags &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const LootSetGroupItem::Flags &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(LootSetGroupItem::Flags &entity, const rapidjson::Value &value)
{
	FromJson(entity.bindToPicker, value, "bindToPicker");
}

template<> void JsonHelper::BlockToJson(const LootSetGroupItem::Flags &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.bindToPicker, value, "bindToPicker");
}

template<> const char *GetTableName<LootSetGroupItem>()
{
	return "loot_set_group_item";
}

template<> const char *GetTableKeyName<LootSetGroupItem>()
{
	return "lsgiID";
}

template<> uint64 GetTableKeyValue(const LootSetGroupItem &entity)
{
	return (uint64)entity.lsgiID;
}

template<> void SetTableKeyValue(LootSetGroupItem &entity, uint64 key)
{
	entity.lsgiID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<LootSetGroupItem>(size_t index)
{
	switch (index)
	{
		case 0: return "lsgiID";
		case 1: return "lsID";
		case 2: return "lsgID";
		case 3: return "lsgiFlags";
		case 4: return "lsgiWeight";
		case 5: return "lsgiItemTypeID";
		case 6: return "lsgiMinCount";
		case 7: return "lsgiMaxCount";
		case 8: return "lsgiMaxTimes";
		case 9: return "lsgiLimitQuest";
		case 10: return "lsgiLimitCareer";
		case 11: return "lsgiLimitGender";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<LootSetGroupItem>(const char *name)
{
	if (strcmp(name, "lsgiID") == 0) return 0;
	if (strcmp(name, "lsID") == 0) return 1;
	if (strcmp(name, "lsgID") == 0) return 2;
	if (strcmp(name, "lsgiFlags") == 0) return 3;
	if (strcmp(name, "lsgiWeight") == 0) return 4;
	if (strcmp(name, "lsgiItemTypeID") == 0) return 5;
	if (strcmp(name, "lsgiMinCount") == 0) return 6;
	if (strcmp(name, "lsgiMaxCount") == 0) return 7;
	if (strcmp(name, "lsgiMaxTimes") == 0) return 8;
	if (strcmp(name, "lsgiLimitQuest") == 0) return 9;
	if (strcmp(name, "lsgiLimitCareer") == 0) return 10;
	if (strcmp(name, "lsgiLimitGender") == 0) return 11;
	return -1;
}

template<> size_t GetTableFieldNumber<LootSetGroupItem>()
{
	return 12;
}

template<> std::string GetTableFieldValue(const LootSetGroupItem &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.lsgiID);
		case 1: return StringHelper::ToString(entity.lsID);
		case 2: return StringHelper::ToString(entity.lsgID);
		case 3: return JsonHelper::BlockToJsonText(entity.lsgiFlags);
		case 4: return StringHelper::ToString(entity.lsgiWeight);
		case 5: return StringHelper::ToString(entity.lsgiItemTypeID);
		case 6: return StringHelper::ToString(entity.lsgiMinCount);
		case 7: return StringHelper::ToString(entity.lsgiMaxCount);
		case 8: return StringHelper::ToString(entity.lsgiMaxTimes);
		case 9: return StringHelper::ToString(entity.lsgiLimitQuest);
		case 10: return StringHelper::ToString(entity.lsgiLimitCareer);
		case 11: return StringHelper::ToString(entity.lsgiLimitGender);
	}
	return "";
}

template<> void SetTableFieldValue(LootSetGroupItem &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.lsgiID, value);
		case 1: return StringHelper::FromString(entity.lsID, value);
		case 2: return StringHelper::FromString(entity.lsgID, value);
		case 3: return JsonHelper::BlockFromJsonText(entity.lsgiFlags, value);
		case 4: return StringHelper::FromString(entity.lsgiWeight, value);
		case 5: return StringHelper::FromString(entity.lsgiItemTypeID, value);
		case 6: return StringHelper::FromString(entity.lsgiMinCount, value);
		case 7: return StringHelper::FromString(entity.lsgiMaxCount, value);
		case 8: return StringHelper::FromString(entity.lsgiMaxTimes, value);
		case 9: return StringHelper::FromString(entity.lsgiLimitQuest, value);
		case 10: return StringHelper::FromString(entity.lsgiLimitCareer, value);
		case 11: return StringHelper::FromString(entity.lsgiLimitGender, value);
	}
}

template<> void LoadFromStream(LootSetGroupItem &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.lsgiID, stream);
	StreamHelper::FromStream(entity.lsID, stream);
	StreamHelper::FromStream(entity.lsgID, stream);
	LoadFromStream(entity.lsgiFlags, stream);
	StreamHelper::FromStream(entity.lsgiWeight, stream);
	StreamHelper::FromStream(entity.lsgiItemTypeID, stream);
	StreamHelper::FromStream(entity.lsgiMinCount, stream);
	StreamHelper::FromStream(entity.lsgiMaxCount, stream);
	StreamHelper::FromStream(entity.lsgiMaxTimes, stream);
	StreamHelper::FromStream(entity.lsgiLimitQuest, stream);
	StreamHelper::FromStream(entity.lsgiLimitCareer, stream);
	StreamHelper::FromStream(entity.lsgiLimitGender, stream);
}

template<> void SaveToStream(const LootSetGroupItem &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.lsgiID, stream);
	StreamHelper::ToStream(entity.lsID, stream);
	StreamHelper::ToStream(entity.lsgID, stream);
	SaveToStream(entity.lsgiFlags, stream);
	StreamHelper::ToStream(entity.lsgiWeight, stream);
	StreamHelper::ToStream(entity.lsgiItemTypeID, stream);
	StreamHelper::ToStream(entity.lsgiMinCount, stream);
	StreamHelper::ToStream(entity.lsgiMaxCount, stream);
	StreamHelper::ToStream(entity.lsgiMaxTimes, stream);
	StreamHelper::ToStream(entity.lsgiLimitQuest, stream);
	StreamHelper::ToStream(entity.lsgiLimitCareer, stream);
	StreamHelper::ToStream(entity.lsgiLimitGender, stream);
}

template<> void LoadFromText(LootSetGroupItem &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const LootSetGroupItem &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(LootSetGroupItem &entity, const rapidjson::Value &value)
{
	FromJson(entity.lsgiID, value, "lsgiID");
	FromJson(entity.lsID, value, "lsID");
	FromJson(entity.lsgID, value, "lsgID");
	BlockFromJson(entity.lsgiFlags, value, "lsgiFlags");
	FromJson(entity.lsgiWeight, value, "lsgiWeight");
	FromJson(entity.lsgiItemTypeID, value, "lsgiItemTypeID");
	FromJson(entity.lsgiMinCount, value, "lsgiMinCount");
	FromJson(entity.lsgiMaxCount, value, "lsgiMaxCount");
	FromJson(entity.lsgiMaxTimes, value, "lsgiMaxTimes");
	FromJson(entity.lsgiLimitQuest, value, "lsgiLimitQuest");
	FromJson(entity.lsgiLimitCareer, value, "lsgiLimitCareer");
	FromJson(entity.lsgiLimitGender, value, "lsgiLimitGender");
}

template<> void JsonHelper::BlockToJson(const LootSetGroupItem &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.lsgiID, value, "lsgiID");
	ToJson(entity.lsID, value, "lsID");
	ToJson(entity.lsgID, value, "lsgID");
	BlockToJson(entity.lsgiFlags, value, "lsgiFlags");
	ToJson(entity.lsgiWeight, value, "lsgiWeight");
	ToJson(entity.lsgiItemTypeID, value, "lsgiItemTypeID");
	ToJson(entity.lsgiMinCount, value, "lsgiMinCount");
	ToJson(entity.lsgiMaxCount, value, "lsgiMaxCount");
	ToJson(entity.lsgiMaxTimes, value, "lsgiMaxTimes");
	ToJson(entity.lsgiLimitQuest, value, "lsgiLimitQuest");
	ToJson(entity.lsgiLimitCareer, value, "lsgiLimitCareer");
	ToJson(entity.lsgiLimitGender, value, "lsgiLimitGender");
}

template<> const char *GetTableName<LootSetGroupCheque>()
{
	return "loot_set_group_cheque";
}

template<> const char *GetTableKeyName<LootSetGroupCheque>()
{
	return "lsgcID";
}

template<> uint64 GetTableKeyValue(const LootSetGroupCheque &entity)
{
	return (uint64)entity.lsgcID;
}

template<> void SetTableKeyValue(LootSetGroupCheque &entity, uint64 key)
{
	entity.lsgcID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<LootSetGroupCheque>(size_t index)
{
	switch (index)
	{
		case 0: return "lsgcID";
		case 1: return "lsID";
		case 2: return "lsgID";
		case 3: return "lsgcWeight";
		case 4: return "lsgcChequeType";
		case 5: return "lsgcMinValue";
		case 6: return "lsgcMaxValue";
		case 7: return "lsgcMaxTimes";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<LootSetGroupCheque>(const char *name)
{
	if (strcmp(name, "lsgcID") == 0) return 0;
	if (strcmp(name, "lsID") == 0) return 1;
	if (strcmp(name, "lsgID") == 0) return 2;
	if (strcmp(name, "lsgcWeight") == 0) return 3;
	if (strcmp(name, "lsgcChequeType") == 0) return 4;
	if (strcmp(name, "lsgcMinValue") == 0) return 5;
	if (strcmp(name, "lsgcMaxValue") == 0) return 6;
	if (strcmp(name, "lsgcMaxTimes") == 0) return 7;
	return -1;
}

template<> size_t GetTableFieldNumber<LootSetGroupCheque>()
{
	return 8;
}

template<> std::string GetTableFieldValue(const LootSetGroupCheque &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.lsgcID);
		case 1: return StringHelper::ToString(entity.lsID);
		case 2: return StringHelper::ToString(entity.lsgID);
		case 3: return StringHelper::ToString(entity.lsgcWeight);
		case 4: return StringHelper::ToString(entity.lsgcChequeType);
		case 5: return StringHelper::ToString(entity.lsgcMinValue);
		case 6: return StringHelper::ToString(entity.lsgcMaxValue);
		case 7: return StringHelper::ToString(entity.lsgcMaxTimes);
	}
	return "";
}

template<> void SetTableFieldValue(LootSetGroupCheque &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.lsgcID, value);
		case 1: return StringHelper::FromString(entity.lsID, value);
		case 2: return StringHelper::FromString(entity.lsgID, value);
		case 3: return StringHelper::FromString(entity.lsgcWeight, value);
		case 4: return StringHelper::FromString(entity.lsgcChequeType, value);
		case 5: return StringHelper::FromString(entity.lsgcMinValue, value);
		case 6: return StringHelper::FromString(entity.lsgcMaxValue, value);
		case 7: return StringHelper::FromString(entity.lsgcMaxTimes, value);
	}
}

template<> void LoadFromStream(LootSetGroupCheque &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.lsgcID, stream);
	StreamHelper::FromStream(entity.lsID, stream);
	StreamHelper::FromStream(entity.lsgID, stream);
	StreamHelper::FromStream(entity.lsgcWeight, stream);
	StreamHelper::FromStream(entity.lsgcChequeType, stream);
	StreamHelper::FromStream(entity.lsgcMinValue, stream);
	StreamHelper::FromStream(entity.lsgcMaxValue, stream);
	StreamHelper::FromStream(entity.lsgcMaxTimes, stream);
}

template<> void SaveToStream(const LootSetGroupCheque &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.lsgcID, stream);
	StreamHelper::ToStream(entity.lsID, stream);
	StreamHelper::ToStream(entity.lsgID, stream);
	StreamHelper::ToStream(entity.lsgcWeight, stream);
	StreamHelper::ToStream(entity.lsgcChequeType, stream);
	StreamHelper::ToStream(entity.lsgcMinValue, stream);
	StreamHelper::ToStream(entity.lsgcMaxValue, stream);
	StreamHelper::ToStream(entity.lsgcMaxTimes, stream);
}

template<> void LoadFromText(LootSetGroupCheque &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const LootSetGroupCheque &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(LootSetGroupCheque &entity, const rapidjson::Value &value)
{
	FromJson(entity.lsgcID, value, "lsgcID");
	FromJson(entity.lsID, value, "lsID");
	FromJson(entity.lsgID, value, "lsgID");
	FromJson(entity.lsgcWeight, value, "lsgcWeight");
	FromJson(entity.lsgcChequeType, value, "lsgcChequeType");
	FromJson(entity.lsgcMinValue, value, "lsgcMinValue");
	FromJson(entity.lsgcMaxValue, value, "lsgcMaxValue");
	FromJson(entity.lsgcMaxTimes, value, "lsgcMaxTimes");
}

template<> void JsonHelper::BlockToJson(const LootSetGroupCheque &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.lsgcID, value, "lsgcID");
	ToJson(entity.lsID, value, "lsID");
	ToJson(entity.lsgID, value, "lsgID");
	ToJson(entity.lsgcWeight, value, "lsgcWeight");
	ToJson(entity.lsgcChequeType, value, "lsgcChequeType");
	ToJson(entity.lsgcMinValue, value, "lsgcMinValue");
	ToJson(entity.lsgcMaxValue, value, "lsgcMaxValue");
	ToJson(entity.lsgcMaxTimes, value, "lsgcMaxTimes");
}
