#pragma once

enum class STRING_TEXT_TYPE
{
	SYS_MSG,
	CLIENT_TEXT,
	ERROR_TEXT,
	MAP_NAME,
	ZONE_NAME,
	CHAR_NAME,
	CHAR_DESC,
	SOBJ_NAME,
	SOBJ_DESC,
	QUEST_NAME,
	QUEST_DESC,
	QUEST_INTRO,
	QUEST_DONE,
	QUEST_NOT_DONE,
	QUEST_CUSTOMIZE,
	ITEM_NAME,
	ITEM_DESC,
	SPELL_NAME,
	SPELL_DESC,
	BUFF_DESC,
	QUESTION,
};

struct string_text_list
{
	string_text_list();

	uint32 stringID;
	std::string stringEN;
	std::string stringCN;
};
