#include "jsontable/table_helper.h"
#include "inst_configure.h"

template<> const char *GetTableName<inst_configure>()
{
	return "inst_configure";
}

template<> const char *GetTableKeyName<inst_configure>()
{
	return "cfgID";
}

template<> uint64 GetTableKeyValue(const inst_configure &entity)
{
	return (uint64)entity.cfgID;
}

template<> void SetTableKeyValue(inst_configure &entity, uint64 key)
{
	entity.cfgID = (uint32)key;
}

template<> const char *GetTableFieldNameByIndex<inst_configure>(size_t index)
{
	switch (index)
	{
		case 0: return "cfgID";
		case 1: return "cfgValue";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<inst_configure>(const char *name)
{
	if (strcmp(name, "cfgID") == 0) return 0;
	if (strcmp(name, "cfgValue") == 0) return 1;
	return -1;
}

template<> size_t GetTableFieldNumber<inst_configure>()
{
	return 2;
}

template<> std::string GetTableFieldValue(const inst_configure &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.cfgID);
		case 1: return StringHelper::ToString(entity.cfgValue);
	}
	return "";
}

template<> void SetTableFieldValue(inst_configure &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.cfgID, value);
		case 1: return StringHelper::FromString(entity.cfgValue, value);
	}
}

template<> void LoadFromStream(inst_configure &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.cfgID, stream);
	StreamHelper::FromStream(entity.cfgValue, stream);
}

template<> void SaveToStream(const inst_configure &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.cfgID, stream);
	StreamHelper::ToStream(entity.cfgValue, stream);
}

template<> void LoadFromText(inst_configure &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const inst_configure &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(inst_configure &entity, const rapidjson::Value &value)
{
	FromJson(entity.cfgID, value, "cfgID");
	FromJson(entity.cfgValue, value, "cfgValue");
}

template<> void JsonHelper::BlockToJson(const inst_configure &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.cfgID, value, "cfgID");
	ToJson(entity.cfgValue, value, "cfgValue");
}
