enum ScriptType
{
	None = 0,
	AIGraphml = 1,
	CharSpawn,
	CharDead,
	CharPlay,
	SObjSpawn = 11,
	SObjPlay,
	QuestReq = 21,
	QuestInit,
	QuestReward,
	QuestEvent,
	CanCastSpell = 31,
	UseItem = 41,
}

table Scriptable
{
	required uint32 scriptId;
	required uint32 scriptType;
	required string scriptFile;
	(key=scriptId, tblname=scriptable)
}