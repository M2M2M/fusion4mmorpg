enum PlayerCareer
{
	None,
	Terran,
	Protoss,
	Demons,
	Max
}

enum PlayerGender
{
	None,
	Male,
	Female,
	Max
}

enum CurrencyType
{
	None,
	Gold,				// 金币
	Diamond,			// 钻石
	Max
}

enum ChequeType
{
	None,
	Gold,				// 金币
	Diamond,			// 钻石
	Exp,				// 经验
	VipExp,				// VIP经验
	Max
}

enum ItemFlag
{
	Binding,
	Constant,
}

struct ChequeInfo
{
	uint8 type;
	uint64 value;
}

struct ItemInfo
{
	uint32 id;
	uint32 num;
}

struct FItemInfo
{
	uint32 id;
	uint32 num;
	uint32 flags;
}

table Configure
{
	required uint32 cfgIndex;
	required string cfgName;
	required string cfgVal;
	required string cfgDesc;
	(tblname=configure)
}