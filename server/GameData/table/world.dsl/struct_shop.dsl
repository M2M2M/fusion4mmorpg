table ShopPrototype
{
	required uint32 Id;
	required uint32 shopType;
	required uint32 itemTypeID;
	required uint32 itemCount;
	required uint32 itemFlags;
	required uint32 currencyType;
	required uint32 sellPrice;
	required uint32 discountPrice;
	required uint32 bundleNumMax;
	required uint32 genderLimit;
	required uint32 careerLimit;
	required uint32 dailyBuyLimit;
	required uint32 weeklyBuyLimit;
	(key=Id, tblname=shop_prototype)
}

struct SpecialShopPrototype : ShopPrototype
{
	uint64 itemUniqueKey;
	int64 itemBeginTime;
	int64 itemEndTime;
	uint32 serverBuyLimit;
	uint32 charBuyLimit;
	uint32 dailyServerBuyLimit;
	uint32 weeklyServerBuyLimit;
}