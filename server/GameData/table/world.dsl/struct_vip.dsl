#include "struct_base.h"

table PayShop
{
	required uint32 Id;
	required string strName;
	required uint32 platformId;
	required string strChannels;
	required uint32 itemType;
	required uint32 buyPrice;
	required uint32 buyGoldValue;
	required ChequeInfo[] buyCheques;
	required FItemInfo[] buyItems;
	required uint32 buyShowItemId;
	required string strRemark;
	(key=Id, tblname=pay_shop)
}