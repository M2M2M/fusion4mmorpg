#pragma once

#include "Singleton.h"
#include <vector>
#include <mutex>

typedef struct _Trie Trie;

class WordFilter : public Singleton<WordFilter>
{
public:
	WordFilter();
	virtual ~WordFilter();

	void Init();

	void FreeObsoleteInsts(time_t expireTime);

	void SetFilterChar(char ch);

	void AddWords(const std::vector<std::string_view>& words);
	void RemoveWords(const std::vector<std::string_view>& words);
	void ReplaceWords(const std::vector<std::string_view>& words);

	bool IsWordFilter(const std::string_view& str) const;
	std::string DoWordFilter(const std::string_view& str) const;

private:
	static Trie* NewTrieInst();
	static Trie* CloneTrieInst(Trie* trieInst);

	char m_fltChar;
	Trie* m_trieInst;

	std::vector<std::pair<Trie*, time_t>> m_obsoleteInsts;
	std::mutex m_mutex;
};

#define sWordFilter (*WordFilter::instance())
