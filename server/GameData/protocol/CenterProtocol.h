#pragma once

enum CenterProtocol
{
	CENTER_PROTOCOL_NONE,

	CCT_REGISTER,
	SCT_REGISTER_RESP,

	FLAG_CENTER_PROTOCOL_NEED_REGISTER_BEGIN,

	CCT_RPC_INVOKE_RESP,  // `ignore`
	SCT_RPC_INVOKE_RESP,  // `ignore`

	FLAG_CENTER_PROTOCOL_FOR_GATE_BEGIN,

	CCT_VERIFY_ACCOUNT_VALIDITY,  // `rpc`

	FLAG_CENTER_PROTOCOL_FOR_GAME_BEGIN,

	SCT_PUSH_WORD_FILTER,  // `ct2game`

	CCT_CHARACTER_CREATE,  // `rpc`
	CCT_CHARACTER_DELETE,  // `rpc`

	CCT_GUILD_CREATE,  // `rpc`
	CCT_GUILD_DELETE,  // `rpc`

	CENTER_PROTOCOL_COUNT,
};

enum LoginServer2CenterServer
{
	LOGIN2CENTER_MESSAGE_NONE,

	CLC_REGISTER,
	SLC_REGISTER_RESP,

	FLAG_LOGIN2CENTER_MSG_NEED_REGISTER_BEGIN,

	CLC_RPC_INVOKE_RESP,  // `ignore`
	SLC_RPC_INVOKE_RESP,  // `ignore`

	SLC_PUSH_GAME_SERVER_LIST,
	SLC_PUSH_GAME_SERVER_STATUS,

	LOGIN2CENTER_MESSAGE_COUNT,
};
