#include "jsontable/table_helper.h"
#include "teleport.h"

template<> void LoadFromStream(TeleportType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const TeleportType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(TeleportType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const TeleportType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(TeleportType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const TeleportType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(TeleportFlag &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const TeleportFlag &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(TeleportFlag &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const TeleportFlag &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(TeleportFlag &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const TeleportFlag &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(CharTeleportInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.playerGuid, stream);
	StreamHelper::FromStream(entity.ownerGuid, stream);
	StreamHelper::FromStream(entity.instGuid, stream);
	StreamHelper::FromStream(entity.x, stream);
	StreamHelper::FromStream(entity.y, stream);
	StreamHelper::FromStream(entity.z, stream);
	StreamHelper::FromStream(entity.o, stream);
	StreamHelper::FromStream(entity.type, stream);
	StreamHelper::FromStream(entity.flags, stream);
}

template<> void SaveToStream(const CharTeleportInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.playerGuid, stream);
	StreamHelper::ToStream(entity.ownerGuid, stream);
	StreamHelper::ToStream(entity.instGuid, stream);
	StreamHelper::ToStream(entity.x, stream);
	StreamHelper::ToStream(entity.y, stream);
	StreamHelper::ToStream(entity.z, stream);
	StreamHelper::ToStream(entity.o, stream);
	StreamHelper::ToStream(entity.type, stream);
	StreamHelper::ToStream(entity.flags, stream);
}

template<> void LoadFromText(CharTeleportInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CharTeleportInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CharTeleportInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.playerGuid, value, "playerGuid");
	FromJson(entity.ownerGuid, value, "ownerGuid");
	FromJson(entity.instGuid, value, "instGuid");
	FromJson(entity.x, value, "x");
	FromJson(entity.y, value, "y");
	FromJson(entity.z, value, "z");
	FromJson(entity.o, value, "o");
	FromJson(entity.type, value, "type");
	FromJson(entity.flags, value, "flags");
}

template<> void JsonHelper::BlockToJson(const CharTeleportInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.playerGuid, value, "playerGuid");
	ToJson(entity.ownerGuid, value, "ownerGuid");
	ToJson(entity.instGuid, value, "instGuid");
	ToJson(entity.x, value, "x");
	ToJson(entity.y, value, "y");
	ToJson(entity.z, value, "z");
	ToJson(entity.o, value, "o");
	ToJson(entity.type, value, "type");
	ToJson(entity.flags, value, "flags");
}

template<> void LoadFromStream(CharLoadInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.playerGuid, stream);
	StreamHelper::FromStream(entity.type, stream);
	StreamHelper::FromStream(entity.flags, stream);
}

template<> void SaveToStream(const CharLoadInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.playerGuid, stream);
	StreamHelper::ToStream(entity.type, stream);
	StreamHelper::ToStream(entity.flags, stream);
}

template<> void LoadFromText(CharLoadInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const CharLoadInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(CharLoadInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.playerGuid, value, "playerGuid");
	FromJson(entity.type, value, "type");
	FromJson(entity.flags, value, "flags");
}

template<> void JsonHelper::BlockToJson(const CharLoadInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.playerGuid, value, "playerGuid");
	ToJson(entity.type, value, "type");
	ToJson(entity.flags, value, "flags");
}

template<> void LoadFromStream(PlayerTeleportInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.playerGuid, stream);
	StreamHelper::FromStream(entity.x, stream);
	StreamHelper::FromStream(entity.y, stream);
	StreamHelper::FromStream(entity.z, stream);
	StreamHelper::FromStream(entity.o, stream);
	StreamHelper::FromStream(entity.guildId, stream);
	StreamHelper::FromStream(entity.guildTitle, stream);
	StreamHelper::FromStream(entity.guildName, stream);
	StreamHelper::FromStream(entity.teamId, stream);
	StreamHelper::FromStream(entity.type, stream);
	StreamHelper::FromStream(entity.flags, stream);
	StreamHelper::FromStream(entity.clientSN, stream);
	StreamHelper::FromStream(entity.gateSN, stream);
}

template<> void SaveToStream(const PlayerTeleportInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.playerGuid, stream);
	StreamHelper::ToStream(entity.x, stream);
	StreamHelper::ToStream(entity.y, stream);
	StreamHelper::ToStream(entity.z, stream);
	StreamHelper::ToStream(entity.o, stream);
	StreamHelper::ToStream(entity.guildId, stream);
	StreamHelper::ToStream(entity.guildTitle, stream);
	StreamHelper::ToStream(entity.guildName, stream);
	StreamHelper::ToStream(entity.teamId, stream);
	StreamHelper::ToStream(entity.type, stream);
	StreamHelper::ToStream(entity.flags, stream);
	StreamHelper::ToStream(entity.clientSN, stream);
	StreamHelper::ToStream(entity.gateSN, stream);
}

template<> void LoadFromText(PlayerTeleportInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const PlayerTeleportInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(PlayerTeleportInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.playerGuid, value, "playerGuid");
	FromJson(entity.x, value, "x");
	FromJson(entity.y, value, "y");
	FromJson(entity.z, value, "z");
	FromJson(entity.o, value, "o");
	FromJson(entity.guildId, value, "guildId");
	FromJson(entity.guildTitle, value, "guildTitle");
	FromJson(entity.guildName, value, "guildName");
	FromJson(entity.teamId, value, "teamId");
	FromJson(entity.type, value, "type");
	FromJson(entity.flags, value, "flags");
	FromJson(entity.clientSN, value, "clientSN");
	FromJson(entity.gateSN, value, "gateSN");
}

template<> void JsonHelper::BlockToJson(const PlayerTeleportInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.playerGuid, value, "playerGuid");
	ToJson(entity.x, value, "x");
	ToJson(entity.y, value, "y");
	ToJson(entity.z, value, "z");
	ToJson(entity.o, value, "o");
	ToJson(entity.guildId, value, "guildId");
	ToJson(entity.guildTitle, value, "guildTitle");
	ToJson(entity.guildName, value, "guildName");
	ToJson(entity.teamId, value, "teamId");
	ToJson(entity.type, value, "type");
	ToJson(entity.flags, value, "flags");
	ToJson(entity.clientSN, value, "clientSN");
	ToJson(entity.gateSN, value, "gateSN");
}
