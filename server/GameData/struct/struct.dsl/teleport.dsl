enum TeleportType
{
	Login,
	Reconnect,
	SwitchMap,
}

enum TeleportFlag
{
	Revive = 1 << 0,
}

struct CharTeleportInfo
{
	uint64 playerGuid;
	uint64 ownerGuid;
	uint64 instGuid;
	float x,y,z,o;
	uint32 type;
	uint32 flags;
}

struct CharLoadInfo
{
	uint64 playerGuid;
	uint32 type;
	uint32 flags;
}

struct PlayerTeleportInfo
{
	uint64 playerGuid;
	float x,y,z,o;

	uint32 guildId;
	uint32 guildTitle;
	string guildName;

	uint32 teamId;

	uint32 type;
	uint32 flags;

	uint32 clientSN;
	uint32 gateSN;
}