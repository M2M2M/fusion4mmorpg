xcopy ..\etc .\etc /E /I /Y
copy ..\..\..\thirdparty\lua\lua53.dll . /Y
copy ..\..\..\thirdparty\lz4\liblz4.dll . /Y
copy ..\..\..\thirdparty\mysql\libmysql.dll . /Y
copy ..\..\..\thirdparty\zlib\zlib1.dll . /Y
copy ..\..\..\thirdparty\openssl\ssleay32.dll . /Y
copy ..\..\..\thirdparty\openssl\libeay32.dll . /Y
set sed="D:\Program Files\Git\usr\bin\sed.exe"
%sed% -i 's/10.1.1.100/192.168.1.10/g' etc/*.conf %replace center server%
%sed% -i 's/10.1.1.198/192.168.1.3/g' etc/*.conf %replace char database%
%sed% -i 's/10.1.1.197/192.168.1.3/g' etc/*.conf %replace log database%
%sed% -i 's/10.1.1.196/192.168.1.3/g' etc/*.conf %replace world database%
%sed% -i 's/10.1.1.195/192.168.1.3/g' etc/*.conf %replace actvt database%
cd ../../../../global/bin
%sed% -i 's/10.1.1.100/192.168.1.10/g' *.json %replace center server%
%sed% -i 's/10.1.1.199/192.168.1.3/g' *.json %replace global database%
%sed% -i 's/10.1.1.196/192.168.1.3/g' *.json %replace world database%
pause