-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主机： db
-- 生成日期： 2020-07-08 12:03:06
-- 服务器版本： 8.0.20
-- PHP 版本： 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `mmorpg_actvt`
--

-- --------------------------------------------------------

--
-- 表的结构 `actvt_string_text_list`
--

CREATE TABLE `actvt_string_text_list` (
  `stringID` int UNSIGNED NOT NULL,
  `stringEN` text,
  `stringCN` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `operating_activities`
--

CREATE TABLE `operating_activities` (
  `nId` int UNSIGNED NOT NULL,
  `strName` varchar(32) NOT NULL,
  `strDesc` text NOT NULL,
  `nUIType` int UNSIGNED NOT NULL,
  `nType` int UNSIGNED NOT NULL,
  `nTimes` int UNSIGNED NOT NULL,
  `nPreType` int UNSIGNED DEFAULT NULL,
  `nPreTimes` int UNSIGNED DEFAULT NULL,
  `nShowReqVipLevel` int UNSIGNED DEFAULT NULL,
  `nShowReqLevel` int UNSIGNED DEFAULT NULL,
  `nPlayReqVipLevel` int UNSIGNED DEFAULT NULL,
  `nPlayReqLevel` int UNSIGNED DEFAULT NULL,
  `nActvtReferTime` datetime DEFAULT NULL,
  `nActvtStopTime` datetime NOT NULL,
  `nShowStartTime` datetime NOT NULL,
  `nShowEndTime` datetime NOT NULL,
  `nPlayStartTime` datetime NOT NULL,
  `nPlayEndTime` datetime NOT NULL,
  `nDailyStartTime` time DEFAULT NULL,
  `nDailyEndTime` time DEFAULT NULL,
  `strOpenServers` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `strNotOpenServers` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `strActivityParams` text NOT NULL,
  `strClientParams` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `operating_activity_uitypes`
--

CREATE TABLE `operating_activity_uitypes` (
  `nId` int UNSIGNED NOT NULL,
  `nUIType` int UNSIGNED NOT NULL,
  `strOpenServers` text,
  `strNotOpenServers` text,
  `strClientParams` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- 转储表的索引
--

--
-- 表的索引 `actvt_string_text_list`
--
ALTER TABLE `actvt_string_text_list`
  ADD PRIMARY KEY (`stringID`);

--
-- 表的索引 `operating_activities`
--
ALTER TABLE `operating_activities`
  ADD KEY `nId` (`nId`) USING BTREE;

--
-- 表的索引 `operating_activity_uitypes`
--
ALTER TABLE `operating_activity_uitypes`
  ADD KEY `nId` (`nId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
